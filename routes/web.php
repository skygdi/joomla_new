<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use App\Notifications\ForgotUsername;
//use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;


//use App\Models\Media;
//use App\Models\PostCard;

Auth::routes();
Route::post('socialauth/login/google-oauth2', 'Auth\SocialAuthController@redirectToProviderGoogle');
Route::post('socialauth/login/facebook-oauth2', 'Auth\SocialAuthController@redirectToProviderFacebook');
Route::get('socialauth/callback/google-oauth2', 'Auth\SocialAuthController@handleProviderCallbackGoogle');
Route::get('socialauth/callback/facebook-oauth2', 'Auth\SocialAuthController@handleProviderCallbackFacebook');
//https://jm.homestead.app/socialauth/login/facebook-oauth2


//Public
Route::middleware('locale')->group(function () {
    //Media
    Route::prefix('media')->group(function () {
        $c = "MediaController";
        Route::delete('/{id}', ['uses' =>$c.'@destroy'])->middleware('auth');
        Route::get( '/like/{id}/{value}', ['uses' =>$c.'@Like'])->middleware('auth');
        Route::post('/home_more', $c.'@HomeMore');
        Route::get( '/thumb/{filename}', ['uses' =>$c.'@Thumb']);
        Route::get( '/images/{filename}', ['uses' =>$c.'@Image']);
        Route::get( '/gallery/{count}', ['uses' =>$c.'@GalleryLoad']);
    });

    //user
    Route::prefix('user')->group(function () {
        $c = "UserController";
        Route::get('/keep_alive', ['uses' =>$c.'@KeepAlive']);
        Route::get('/language/{language}', ['uses' =>$c.'@Language']);
        Route::get('/avatar/{id?}', $c.'@Avatar')->middleware('auth.empty');
    });

    //Comment
    Route::resource('comment', 'CommentController')->middleware('auth');
    Route::prefix('comment')->middleware('auth')->group( function () {
        $c = "CommentController";
        Route::get( 'permission/{id}/{value}', ['uses' =>$c.'@Permission']);
    });

    //payment
    Route::prefix('payment')->group(function () {
        Route::prefix('paypal')->middleware('auth')->group(function () {
            //$c = "PaypalController";
            $c = "PayPalController";
            Route::post('/create', $c.'@Create');
            Route::post('/execute', $c.'@Execute');
        });
    });

    //Address
    //Route::resource('/address', 'AddressController',['only' => ['show','store', 'update', 'destroy'] ] )->middleware('auth');
    Route::resource('/address', 'AddressController',['except' => ['index','edit', 'create'] ] )->middleware('auth');
    Route::prefix('address')->group(function () {
        $c = "AddressController";
        Route::put( '/{id}/default_return_address', ['uses' =>$c.'@ChangeDefaultReturn']);
        Route::put( '/{id}/default_address', ['uses' =>$c.'@ChangeDefault']);
    });


    Route::get( '/slider/{filename}', ['uses' =>'MiscController@Slider']);
});

//Mobile
Route::namespace('Mobile')->middleware('locale')->group(function () {
    //['except' => ['Gallery','destroy']]
    Route::prefix('mobile')->group(function () {
        Route::get('/', 'FrontController@Home')->name('mobile.home');
    });

    Route::prefix('mobile')->middleware('auth.mobile')->group(function () {
        //Route::get('/', 'FrontController@Home')->name('mobile.home');

        //search
        Route::get('/search/{page}/{word}', ['uses' =>'FrontController@Search'])->name('mobile.search');

        //user
        Route::prefix('user')->group(function () {
            $c = "UserRController";
            Route::post('/profile', $c.'@profile');
            Route::get('/{id}/channel', ['uses' =>$c.'@Channel']);
        });

        

        //media
        Route::resource('/media', "MediaRController")->middleware('auth');
        Route::prefix('media')->group(function () {
            $c = "MediaRController";
            //Route::post('/profile', $c.'@profile');
        });

        //Post Card
        Route::resource('/postcard', 'PostCardRController')->middleware('auth');
        Route::prefix('/postcard')->group(function () {
            $c = "PostCardRController";
            Route::get( '/{id}/create', ['uses' =>$c.'@preselect_create']);
        });

        //Shopping Cart
        Route::resource('/shoppingcart', 'ShoppingCartRController')->middleware('auth');
        Route::prefix('shoppingcart')->group(function () {
            $c = "ShoppingCartRController";
            Route::post('/deletes', $c.'@deletes');
            Route::post('/updates', $c.'@updates');
            Route::get( '/reorder/{id}', ['uses' =>$c.'@reorder']);
            
        });

        //Order
        Route::resource('/order', 'OrderRController')->middleware('auth');

        //Address
        Route::resource('/address', 'AddressRController',['only' => ['index','show'] ])->middleware('auth');
        /*
        Route::prefix('address')->group(function () {
            $c = "AddressRController";
            Route::put( '/{id}/default_return_address', ['uses' =>$c.'@ChangeDefaultReturn']);
            Route::put( '/{id}/default_address', ['uses' =>$c.'@ChangeDefault']);
        });
        */
    });

});

Route::namespace('Desktop')->middleware('locale')->group(function () {

    Route::get('/', 'FrontController@Home')->name('home');
    Route::get('/home', 'FrontController@Home')->name('home');

    //search
    Route::match(['get', 'post'],'/search/{word?}', ['uses' =>'FrontController@SearchWord']);
    Route::get('/search_page', ['uses' =>'FrontController@SearchPage'])->name('desktop.search');
    

    

    //Order
    Route::resource('/order', 'OrderRController')->middleware('auth');

    //Shopping Cart
    Route::resource('/shoppingcart', 'ShoppingCartController')->middleware('auth');
    Route::prefix('shoppingcart')->group(function () {
        $c = "ShoppingCartController";
        Route::post('/deletes', $c.'@deletes');
        Route::post('/updates', $c.'@updates');
        Route::get( '/reorder/{id}', ['uses' =>$c.'@reorder']);
        
    });
    
    //Post Card
    Route::resource('postcard', 'PostCardController')->middleware('auth');
    Route::prefix('postcard')->group(function () {
        $c = "PostCardController";
        Route::get( '/{id}/create', ['uses' =>$c.'@preselect_create']);
    });

    //User
    Route::prefix('user')->group(function () {
        $c = "UserRController";
        
        Route::get('/{id}/channel', ['uses' =>$c.'@Channel']);

        Route::group(['middleware' => 'auth'], function(){
            $c = "UserRController";
            //Route::match(array('GET', 'POST'),'/profile', $c.'@Profile')->name('user.profile');
            Route::match(array('GET', 'POST'), '/profile', ['uses' =>$c.'@Profile'])->name('user.profile');

            Route::get('/logout_show', $c.'@showLogoutForm')->middleware('auth');
        });
    });

    

    
    //Media
    Route::get( 'media/gallery', ['uses' =>'MediaRController@Gallery']);
    Route::resource('media', 'MediaRController',['except' => ['Gallery','destroy']]);
    
    //Address
    Route::resource('/address', 'AddressRController',['only' => ['index'] ])->middleware('auth');
    /*
    Route::prefix('address')->group(function () {
        $c = "AddressRController";
        Route::put( '/{id}/default_return_address', ['uses' =>$c.'@ChangeDefaultReturn']);
        Route::put( '/{id}/default_address', ['uses' =>$c.'@ChangeDefault']);
    });
    */
    
    //About
    Route::prefix('about')->group(function () {
        $c = "AboutController";
        Route::get('/', $c.'@about');
        Route::get('privacy', $c.'@privacy');
        Route::get('terms', $c.'@terms');
        Route::get('tos', $c.'@tos');
        Route::get('contact', $c.'@contact')->name("contact");
        Route::post('contact', $c.'@contact_save');
    });
    

});

Route::namespace('Admin')->prefix('admin')->group(function () {
    $c = "MenuController";
    Route::get('/', $c.'@Login');

    Route::middleware('admin')->group( function () {
        $c = "MenuController";
        Route::get('dashboard', $c.'@Dashboard');
        //Route::get('order', $c.'@Order');
        Route::resource('order', 'OrderRController');
        Route::post('order/change_filter', 'OrderRController@ChangeFilter');

        Route::resource('order_item', 'OrderItemRController',['only' => ['destroy'] ]);
        Route::resource('expenditure', 'ExpenditureRController',['only' => ['index','store','update','destroy'] ]);
        Route::resource('slider', 'SliderRController');

        Route::get('slider/{id}/suspend/{value}', 'SliderRController@Suspend');
        
    });
    /*
    Route::get('/', function () {
        return view('admin.login');
        //return view('welcome');
    });

    Route::get('login', function () {
        return view('admin.login');
        //return view('auth.login');
    });

    
    Route::get('dashboard', function () {
        return view('admin.dashboard');
        //return view('auth.login');
    });
    */
});


Route::get('/u', 'Upgrade@All');

/*
Route::get('/test', function () {
    //dd(1);
    $postcard = PostCard::find(384);
    //$media->ImageCacheGet();
    dd($postcard->Media()->first()->toArray());
});
*/


<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google'  =>    [
        "client_id"=> "394556994572-7b4m9cp8iift2910jggak3nthh1ck8t6.apps.googleusercontent.com",
        "client_secret"    =>  "Xxt6VSkyMTH8n6nSd1tXA9je",
        'redirect' => 'https://jm.homestead.app/socialauth/callback/google-oauth2',
    ],

    'facebook'  =>    [
        "client_id"=> "143971566301882",
        "client_secret"    =>  "bd0b36a3238f2506fc7f0b0d7c6df1a7",
        'redirect' => 'https://jm.homestead.app/socialauth/callback/facebook-oauth2',
    ],

    


];

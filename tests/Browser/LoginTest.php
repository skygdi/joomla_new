<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            //$email = "skygdi87@gmail.com";
            $browser->visit('http://jm.homestead.app/laravel/login')
                    ->type('username', "Steven")
                    ->type('password', '2')
                    ->press('.btn-primary')
                    ->assertPathIs('/laravel/home1');
                    ///home
        });
    }
}

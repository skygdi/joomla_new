<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserRegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        /*
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Laravel');
        });
        ->waitForText('skygdi84')
        */
        /*
        $this->browse(function (Browser $browser) {
            $email = "skygdi84@gmail.com";
            $browser->visit('http://jm.homestead.app/laravel/register')
                    ->type('name', "skygdi83")
                    ->type('password', '123456')
                    ->type('password_confirmation', '123456')
                    ->type('email',         $email)
                    ->press('Register')
                    
                    ->assertSee('skygdi83');
                    //->assertPathIs('/index.php/home');
                    ///home
        });
        */

        return;
        ///*
        $this->browse(function (Browser $browser) {
            $email = "skygdi87@gmail.com";
            $browser->visit('http://jm.homestead.app/laravel/register')
                    ->type('username', $email)
                    ->type('password', '350185')
                    ->type('password_confirmation', '123456')
                    ->type('name', 'dd')
                    ->type('email',         $email)
                    ->type('email_confirmation', $email)
                    ->type('paypal', $email)
                    ->press('SUBMIT')
                    //->waitForText('skygdi83')
                    //->assertSee('skygdi83');
                    ->assertPathIs('/laravel/home');
                    ///home
        });
        //*/
    }
}

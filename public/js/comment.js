function CommentSubmit(id,obj){
    var content = $(obj).parent().find("textarea").val();
    if( content.length<2 || content.length>250 ){
        //alert("Content length should be in between 2~255 character!");
        ///*
        swal({
            title: "",
            text: comment_string_min_tip,
            timer: 4000,
            type: 'error'
        });
        //*/
        return;
    }
    //return false;
    
    //console.log(id);

    $.ajax({
        type: "POST",
        url: system_url+"/comment",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
            "content":content,
            "id":id,
        },
        success: function(dataObj){
            if( dataObj=="denied" ) return return_to_home();

            //console.log(1);
            //id
            //dataObj = eval("("+msg+")");

            swal({
                title: "",
                text: dataObj["result_string"],
                timer: 4000,
                type: 'success'
            });

            //console.log(1);
            var media_id = dataObj["id"];
            //console.log(item_id);

            var data = $("#div_comment").html();
            data = data.replace("<!--","");
            data = data.replace("-->","");


            data = data.replace("{id}",dataObj["comment"]["id"]);
            data = data.replace("{created_at}",dataObj["comment"]["created_at"]);
            data = data.replace("{content}",dataObj["comment"]["content"]);

            //console.log(data);

            //console.log(data);
            //console.log(1);
            $("div[id='collapse"+media_id+"']:lt(1) > ul ").prepend(data);
            //$("div[id='collapse"+media_id+"']:lt(1) > ul ").append(data);
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });

}

function CommentDelete(id,obj){
    var answer = confirm(item_confirm_string);
    if (!answer) return;

    $.ajax({
        type: "DELETE",
        url: system_url+"/comment/"+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg.state=="success" ){
                $(obj).parent().parent().parent().remove();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function CommentPermission(id,obj){
    var v = 0;
    if( $(obj).prop('checked')==true ) v = 1;

    $.ajax({
        type: "GET",
        url: system_url+"/comment/permission/"+id+"/"+v,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: "",
        success: function(msg){
            if( msg.state=="denied" ) return return_to_home();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
        }
    });
}
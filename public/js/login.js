
$(document).ready(function(){
    var validation_public_option = {
        rules: {
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 1
            },
        },
        messages: {
            username: msg_validation_name,
            password: msg_validation_password,
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                //error.insertAfter( element );
                error.appendTo( $( element ).parents( ".form-group" ).find("label") );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
            
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    };

    //var validation_option_new = validation_public_option;
    $.extend(validation_public_option,{
        submitHandler: function(form) {
            login();
            //new_address('billing','formNewBillingAddress');
        }
    });
    $( "#formLogin").validate(validation_public_option);

    /*
    //test value
    $("[name='name']").val("1111");
    $("[name='username']").val("1111");
    $("[name='email']").val("11@gmail.com");
    $("[name='confirm_email']").val("11@gmail.com");
    $("[name='password']").val("123456");
    $("[name='confirm_password']").val("123456");
    $("[name='paypal']").val("11@gmail.com");
    */
});



function login(){
    //var obj = $("#formCreateAccount");
    $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: system_url+"/login",
        dataType: "json",
        data:{
            "username":$("input[name='username']").val(),
            "password":$("input[name='password']").val(),
        },
        success: function(data){
            if( data["flag"]==false ){
                if( data["result"]=="username and or password error" ){
                    swal({
                        title: "",
                        text: msg_username_or_password_error,
                        timer: 5000,
                        type: 'error'
                    });
                }
                else{
                    swal({
                        title: "",
                        text: data["result"],
                        timer: 5000,
                        type: 'error'
                    });
                }
            }
            else{
                /*
                swal({
                    title: "",
                    text: msg_success,
                    timer: 5000,
                    type: 'success'
                });
                */
                return_to_home();
            }
            //console.log(data);
            //
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}
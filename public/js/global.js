
$(document).ready(function(){
    $("[title]").tooltip();
});

$( function(){
    //$( document ).tooltip();
});

function logout(){
    $.ajax({
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: system_url+"/logout",
        success: function(data){
            return_to_home();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function switch_language(lang){
	$.ajax({
		type: "GET",
		url:system_url+"/user/language/"+lang,
		success: function(data) {
			if( data=="success" ){
				window.location = window.location.href;
				//window.location.reload();
			} 
			//alert(data);
			
		}
	});
}

function keep_alive(lang){
	$.ajax({
		type: "GET",
		url:system_url+"/keep_alive?random="+(new Date()).getTime(),
		success: function(data) {
			if( data=="denied" ) return return_to_home();
			//if( data=="success" ) window.location.reload();
			
			//alert(data);
		}
	});
}

function return_to_home(){
	window.location = root_url+"/";

}



function init_carousel(){
    $('.owl-carousel').owlCarousel({
        autoplay:false,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        lazyLoad: true,
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:6
            }
        }
    });
}

function select_image(obj,id){
    current_item_id = id;
    $("#big_image").attr("src",root_url+"/"+$(obj).attr("original_src") );
}
//var current_item_id = {{postcard.original_media_id}};
var current_item_id;

function edit_post_card_submit(){
    if( $("input[name='name']").val()=="" ){
        swal({
            title: "",
            text: "Please Input Post Card Name",
            timer: 6000,
            type: 'error'
        });
        return;
    }

    if( current_item_id==null ){
        swal({
            title: "",
            text: "Please Select An Image First",
            timer: 6000,
            type: 'error'
        });
        return;
    }

    

    $.ajax({
        type: "POST",
        url:root_url+"/index.php/edit-postcard?random="+(new Date()).getTime(),
        data:{
            "name":$("input[name='name']").val(),
            "content":$("#backside_text_box").val(),
            "id":current_item_id,
            "postcard_id":current_id,
        },
        success: function(msg){
            //alert(msg);
            if( msg=="denied" ) return return_to_home();
            swal({
                title: "",
                text: "success",
                timer: 6000,
                type: 'success'
            });
            //return;

        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}
//var current_id = {{postcard.id}};
var current_id;
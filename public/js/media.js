
function toggle_detail( obj ){
    $(obj).next().toggle( "fast" );
}
function change_like(id,obj){
    var cr = $(obj).attr("cr");
    var crl = parseInt($(obj).next().children().text());

    if( cr=="n" ){
        $(obj).attr("cr","p");
        $(obj).attr("class",$(obj).attr("cp") );
        crl++;
    }
    else{
        $(obj).attr("cr","n");
        $(obj).attr("class",$(obj).attr("cn") );
        crl--;
    }
    $(obj).next().children().text(crl);

    $.ajax({
        type: "GET",
        url: system_url+"/media/like/"+id+"/"+cr,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: "",
        success: function(msg){
            if( msg.state=="denied" ) return return_to_home();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}






function MediaRemove(id,obj){
    var answer = confirm(item_confirm_string);
    if (!answer) return;

    $.ajax({
        type: "DELETE",
        url: system_url+"/media/"+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg.state=="success" ){
                $(obj).parent().next().remove();;
                $(obj).parent().remove();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });

    
}
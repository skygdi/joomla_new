


function toggle_detail( obj ){
    $(obj).next().toggle( "fast" );
}

function init_carousel(){
    $('.owl-carousel').owlCarousel({
        autoplay:false,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        lazyLoad: true,
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:6
            }
        }
    });
}

function select_image(obj,id){
    current_item_id = id;
    $("#big_image").attr("src",root_url+"/"+$(obj).attr("original_src") );
}
var current_item_id = null;

function NewPostCard(){
    if( $("input[name='name']").val()=="" ){
        swal({
            title: "",
            text: msg_input_name,
            timer: 6000,
            type: 'error'
        });
        return;
    }

    if( current_item_id==null ){
        swal({
            title: "",
            text: msg_select_image,
            timer: 6000,
            type: 'error'
        });
        return;
    }

    

    $.ajax({
        type: "POST",
        //url:system_url+"/postcard/store?random="+(new Date()).getTime(),
        url:system_url+"/postcard",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
            "name":$("input[name='name']").val(),
            "content":$("#backside_text_box").val(),
            "id":current_item_id,
        },
        success: function(msg){
            //alert(msg);
            if( msg=="denied" ) return return_to_home();
            swal({
                title: "",
                text: msg_success,
                timer: 6000,
                type: 'success'
            });

            
            //return;

        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function UpdatePostCard(){
    if( $("input[name='name']").val()=="" ){
        swal({
            title: "",
            text: msg_input_name,
            timer: 6000,
            type: 'error'
        });
        return;
    }

    if( current_item_id==null ){
        swal({
            title: "",
            text: msg_select_image,
            timer: 6000,
            type: 'error'
        });
        return;
    }

    

    $.ajax({
        type: "patch",
        url:root_url+"/postcard/"+current_id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
            "name":$("input[name='name']").val(),
            "content":$("#backside_text_box").val(),
            "id":current_item_id,
            "postcard_id":current_id,
        },
        success: function(msg){
            //alert(msg);
            if( msg=="denied" ) return return_to_home();
            swal({
                title: "",
                text: "success",
                timer: 6000,
                type: 'success'
            });
            //return;

        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}


function PostCardRemove(id,obj){
    var answer = confirm(confirm_string);
    if (!answer) return;

    $.ajax({
        type: "DELETE",
        url: system_url+"/postcard/"+id,
        dataType: "json",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            //$(obj).parent().next().remove();;
            //$(obj).parent().remove();

            if( data.state=="success" ){
                $(obj).parent().next().remove();;
                $(obj).parent().remove();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });

    
}


var obj_IM = new ItemMore();

$(document).ready(function () {
	obj_IM.load_each = load_each;
	obj_IM.Init();
});



function ItemMore(){
	this.load_each = 0;
	this.flag_refresh = false;
}

ItemMore.prototype.Init=function(){
	this.loaded_click = this.load_each;
	this.loaded_newest = this.load_each;
	
	this.model = $("#model_item").html();
	this.model = this.model.replace("<!--","");
	this.model = this.model.replace("-->","");
	this.model = this.model.replaceAll("{item_width}",item_width);
}

ItemMore.prototype.Refresh=function(){
	if( this.flag_refresh ) return;
	this.flag_refresh = true;
	//url:system_url+"/api/index.php/home_items_more",
	$.ajax({
		type: "POST",
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:system_url+"/media/home_more",
		data:{
			"loaded_click":obj_IM.loaded_click,
			"loaded_newest":obj_IM.loaded_newest,
			"load_each":obj_IM.load_each,
		},
		success: function(msg){
			//alert(msg);
			obj_IM.loaded_click += obj_IM.load_each;
			

			dataObj = eval("("+msg+")");
			obj_IM.Render($("#item_list_click"),dataObj["click"]);
			
			obj_IM.Render($("#item_list_newest"),dataObj["newest"]);

			
			obj_IM.flag_refresh = false;
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
			obj_IM.flag_refresh = false;
		}
	});
}

ItemMore.prototype.Render=function(obj,dataObj){
	
	for( var key in dataObj ){
		var tmp = this.model;
		tmp = tmp.replaceAll("{id}",dataObj[key].id);
		tmp = tmp.replaceAll("{image_cache_url}",dataObj[key].image_cache_url);
		tmp = tmp.replaceAll("{alias}",dataObj[key].alias);
		tmp = tmp.replaceAll("{user_id}",dataObj[key].user_id);
		tmp = tmp.replaceAll("{name}",dataObj[key].user_name);

		
		$(tmp).appendTo(obj);
	}
}

String.prototype.replaceAll  = function(s1,s2){   
  return this.replace(new RegExp(s1,"gm"),s2);   
}

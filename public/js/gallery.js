$(document).ready(function(){
    data_load();
});

function data_load(){
    
    var current_data_count = $("#item_list_click").children().length;
    

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:system_url+"/media/gallery/"+current_data_count,
        success: function(dataObj){

            
            for( var key in dataObj ){
                var tmp = $("#model_item").html();
                tmp = tmp.replaceAll("<!--","");
                tmp = tmp.replaceAll("-->","");
                tmp = tmp.replaceAll("{id}",dataObj[key].id);
                tmp = tmp.replaceAll("{image_cache_url}",dataObj[key].image_cache_url);
                tmp = tmp.replaceAll("{alias}",dataObj[key].alias);
                tmp = tmp.replaceAll("{user_id}",dataObj[key].user_id);
                tmp = tmp.replaceAll("{name}",dataObj[key].name);

                
                $(tmp).appendTo($("#item_list_click"));
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
            //obj_IM.flag_refresh = false;
        }
    });
}

String.prototype.replaceAll  = function(s1,s2){   
  return this.replace(new RegExp(s1,"gm"),s2);   
}
$(document).ready(function(){
	$('.form-horizontal:lt(1)').on('submit',function(){
		if( $(this).find("input[name='password']").val()!="" || $(this).find("input[name='password_confirmation']").val()!="" ){
			if( $(this).find("input[name='password']").val()!=$(this).find("input[name='password_confirmation']").val() ){
				swal({
					title: "",
					text: "Password doesn't match the confirm's password",
					timer: 3000
				});

				$(this).find("#password").focus();
				return false;
			}
		}
		return true;
	});
});

function remove_address(obj){
    var id = $(obj).parents(".modal-content").find("input[name='id']").val();
    //console.log(id);
    var answer = confirm("Are you sure?");
    if (!answer) return;

    $.ajax({
        type: "DELETE",
        url: system_url+"/address/"+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg=="denied" ) return return_to_home();
            else{
                window.location.reload();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
        }
    });
}

function remove_address2(obj,id){
    var answer = confirm(msg_confirm);
    if (!answer) return;

    $.ajax({
        type: "DELETE",
        url: system_url+"/address/"+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg=="denied" ) return return_to_home();
            else{
                window.location.reload();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
        }
    });
}

function new_address(form_id){
    var obj = $("#"+form_id);
    $.ajax({
        type: "POST",
        url: system_url+"/address" ,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $(obj).serialize(),
        success: function(msg){
            if( msg.state!="success" ){
                swal({
                    title: "",
                    text: msg.text,
                    timer: 5000,
                    type: 'error'
                });
            }
            else{
                window.location.reload();
            }
            /*
            if( msg=="denied" ) return return_to_home();
            else{
                window.location.reload();
            }
            */
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function edit_address(form_id){
    var obj = $("#"+form_id);
    var id = $(obj).find("input[name='id']").val();
    $.ajax({
        type: "PUT",
        url: system_url+"/address/"+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $(obj).serialize(),
        success: function(msg){
            //console.log(msg);
            if( msg.state!="success" ){
                swal({
                    title: "",
                    text: msg.text,
                    timer: 5000,
                    type: 'error'
                });
            }
            else{
                window.location.reload();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function edit_address_show(id,obj,type,modal_id){

    $.ajax({
        type: "GET",
        dataType: "json",
        url: system_url+"/address/"+id,
        success: function(msg){
            //if( msg=="denied" ) return return_to_home();
            //console.log(msg);
            var edit_modal = $('#'+modal_id);
            //rendering
            $(edit_modal).find("input[name='id']").val(msg.id);
            $(edit_modal).find("input[name='name']").val(msg.name);
            $(edit_modal).find("input[name='address1']").val(msg.address1);
            $(edit_modal).find("input[name='address2']").val(msg.address2);
            $(edit_modal).find("input[name='city']").val(msg.city);
            $(edit_modal).find("input[name='phone']").val(msg.phone);
            $(edit_modal).find("input[name='email']").val(msg.email);
            $(edit_modal).find("input[name='state']").val(msg.state);
            $(edit_modal).find("input[name='zipcode']").val(msg.zipcode);
            $(edit_modal).find("[name='country']").val(msg.country);
            $(edit_modal).find('.selectpicker').selectpicker('refresh');

            //show
            $('#'+modal_id).modal('show');
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
            if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
        }
    });
}


function ChangeReturnAddress(id,obj,type){
    
    var previous_value = 0;
    if( $(obj).find("i").attr("class")=="uk-icon-dot-circle-o" ) previous_value = 1;
    else previous_value = 0;

    //mark all as unmarked
    $(obj).parents("ul").find("button[class~='change_return']").find("i").attr("class","uk-icon-circle-o");

    if( previous_value==1 ) $(obj).find("i").attr("class","uk-icon-circle-o");
    else $(obj).find("i").attr("class","uk-icon-dot-circle-o");



    $.ajax({
        type: "PUT",
        url: system_url+"/address/"+id+"/default_return_address",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg=="denied" ) return return_to_home();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function change_default_address(id,obj,type){
    $(obj).parents("ul").find("button[class~='change_default']").prop('disabled', false);
    $(obj).parents("ul").find("button[class~='change_default']").find("i").attr("class","uk-icon-circle-o");


    //$(obj).prop('disabled', true);
    $(obj).find("i").attr("class","uk-icon-dot-circle-o");

    $.ajax({
        type: "PUT",
        url: system_url+"/address/"+id+"/default_address",
        data:{
            "id":id,
            "type":type,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg=="denied" ) return return_to_home();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function select_state_change(obj,value){
    $(obj).parents("form").find("input[name='state']").val(value);
}

function button_change_address(obj,id){
    $(obj).find("span:eq(0)").toggle();
    $(obj).find("span:eq(1)").toggle();

    $("#"+id).slideToggle();
    $(obj).parent().prev().toggleClass("address_current_weak");
}
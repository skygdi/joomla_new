

function ShoppingCartMini(){
	this.Modal = $('#ModalShoppingCartMini');
	this.Table = $('#shopping_cart_mini_table');

	$(this.Modal)
	.data('that', this)
	.on('shown.bs.modal', function (e) {
		var obj = $(this).data('that');
		if( obj.initialized ) return;
		obj.Render();
		obj.initialized = true;
	});

	this.loading = false;
	this.initialized = false;
	this.test = "mao";
}

ShoppingCartMini.prototype.AddToCart=function(obj,id){
	//uk-icon-spinner uk-icon-spin
	$(obj).find("i:lt(1)").attr("class","uk-icon-spinner uk-icon-spin");
	var that = obj;
	
	//url:system_url+"/api/index.php/shopping-cart-mini-item-add?id="+id+"&random="+(new Date()).getTime(),
	$.ajax({
		type: "POST",
		url:system_url+"/shoppingcart",
		data:{
			"id":id,
			"quantity":1,
		},
		headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		dataType:'json',
		context : this,
		success: function(data) {
			JSON_ShoppingCartMini = data.minicart.data;
			//this.RenderSumarry(data.sumarry);
			this.RenderSumarry(data.minicart);
			this.Render();

			$(that).find("i:lt(1)").attr("class","uk-icon-shopping-cart");

			//alert( $(that).html() );
			//if( data=="success" ) 
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//console.log(XmlHttpRequest.responseText);
			if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
		}
	});
}

ShoppingCartMini.prototype.ChangeQuantity=function(obj){
	var id = $(obj).parent().parent().attr("data-id");
	var quantity = $(obj).val();
	//console.log(id);
	//console.log(quantity);
	if( this.loading ) return;
	this.Lock();
	//return;

	//url:system_url+"/shoppingcart/"+id,
	//url:system_url+"/api/index.php/shopping-cart-mini-item-quantity-update?random="+(new Date()).getTime(),
	$.ajax({
		type: "PUT",
		url:system_url+"/shoppingcart/"+id,
		data:{
			"quantity":quantity,
		},
		headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		dataType:'json',
		context : this,
		success: function(data) {
			this.UnLock();
			JSON_ShoppingCartMini = data.minicart.data;
			this.RenderSumarry(data.minicart);
			this.Render();
			
			//alert(data.user_info.role);
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
			this.UnLock();
			//console.log(this.test);
		}
	});
}

ShoppingCartMini.prototype.Remove=function(obj){
	var id = $(obj).parent().parent().attr("data-id");
	if( this.loading ) return;
	//console.log(this.loading);

	this.Lock();


	$.ajax({
		type: "DELETE",
		url:system_url+"/shoppingcart/"+id,
		headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		dataType:'json',
		context : this,
		success: function(data) {
			this.UnLock();
			JSON_ShoppingCartMini = data.minicart.data;
			this.Render();
			this.RenderSumarry(data.minicart);
			//alert(data.user_info.role);
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
			this.UnLock();
			//console.log(this.test);
		}
	});
}

ShoppingCartMini.prototype.Lock=function(obj){
	this.loading = true;
	$(this.Modal).find(".loading:lt(1)").show();
}

ShoppingCartMini.prototype.UnLock=function(obj){
	this.loading = false;
	$(this.Modal).find(".loading:lt(1)").hide();
}

ShoppingCartMini.prototype.RenderSumarry=function(data){
	//$("#shopping_cart_sumarry_count").html( data[0] );
	//$("#shopping_cart_sumarry_total").html( "$"+data[1] );
	//console.log(data);
	$("#shopping_cart_sumarry_count").html( data["count"] );
	$("#shopping_cart_sumarry_total").html( "$"+data["total"] );
	
}

ShoppingCartMini.prototype.Render=function(){
	
	$(this.Table).find("tbody > tr").remove();
	var model = $("#model_shopping_cart_mini > tbody > tr").clone();

	if( JSON_ShoppingCartMini.length==0 ){
		$(this.Modal).find(".shopping_cart_empty:lt(1)").show();
		//$(this.Modal).find("#shopping_cart_mini_table").hide();
		$(this.Modal).find("#shopping_cart_mini_content").hide();
	}
	else{
		$(this.Modal).find(".shopping_cart_empty:lt(1)").hide();
		//$(this.Modal).find("#shopping_cart_mini_table").show();
		$(this.Modal).find("#shopping_cart_mini_content").show();
		
	}
	
	for( var key in JSON_ShoppingCartMini  ){
		var data = JSON_ShoppingCartMini[key];
		var current_tr = model;
		$(current_tr).attr("data-id",data.id);
		var tds = $(current_tr).find("td");
		$($(tds)[1]).html(data.name);
		$($(tds)[3]).html("$"+data.price);
		$($(tds)[4]).html("$"+data.total);


		$(this.Table).find("tbody").append($(current_tr)[0].outerHTML );

		$(this.Table).find("tbody > tr:last").find("input:lt(1)").val( data.quantity );
	}

	$(this.Modal).find(".total:lt(1)").html( $("#shopping_cart_sumarry_total").html() );
	
}
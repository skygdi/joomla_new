

$(document).ready(function(){
    Obj_ShoppingCart = new ShoppingCart();
});

function agreeing(obj){
    $(obj).find("i").toggleClass("uk-icon-check-square-o uk-icon-square-o");
}

function checkout(obj){
	//if hadn't check the box
	if( $("#checking_button").find("i:lt(1)").attr("class")!="uk-icon-check-square-o" ){
		//shaking
		var properties = {
	        color: "#aa00aa",
	        fontSize: "120%"
	    };
	    var els = $('#checkout_text');
	    els.pulse(properties, {duration : 650, pulses : 1});
	    $( obj ).effect( "shake" );

	    return;
	}

	var item_info_array = new Array();
	var anArray = $("#cart_item").find("tbody").find("tr");
    for(var i=0;i<anArray.length;i++){
        var $v = $(anArray[i]);

        var item_info = new Object();

        item_info.id = $(anArray[i]).attr("item_id");
        item_info.quantity = parseInt($v.find("[class~='zx-zoocart-tableitems-row-quantity']").find("input").val());

        item_info_array.push(item_info);
    }

    if( item_info_array.length==0 ){
    	swal({
            title: "",
            text: msg_order_empty,
            timer: 5000,
            type: 'error'
        });
    	return;
    }

    var addresses = new Object();
    addresses.billing = $("#billing_address_current").find("input[name='id']").val();
    addresses.shipping = $("#shipping_address_current").find("input[name='id']").val();

    //console.log(addresses.billing);return;
    if( addresses.billing=="" ){
    	swal({
            title: "",
            text: msg_address_billing_empty,
            timer: 5000,
            type: 'error'
        });
    	return;
    }

    if( addresses.shipping=="" ){
    	swal({
            title: "",
            text: msg_address_shipping_empty,
            timer: 5000,
            type: 'error'
        });
    	return;
    }


    var that = obj;

	$.ajax({
		type: "POST",
		url:system_url+"/order",
		data: {item_info: JSON.stringify(item_info_array),addresses:JSON.stringify(addresses)},
		headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		dataType:'json',
		context : this,
		success: function(data) {
			//console.log(data.order_id);
			//window.location = root_url+"/index.php/orders?task=pay&id="+data.order_id;
			//window.location = root_url+"/index.php/payment/checkout/paypal?id="+data.order_id;
			//$(this).hide();
			$(that).hide();
			$("#paypal-button").show();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
		}
	});

    
}

function try_click(){
	$("#paypal-button").show();
}

function ShoppingCart(){

	this.current_tr = null;

	this.shopping_cart_changing_flag = false;
	this.timer_refresh;

	this.shopping_cart_removing_flag = false;
	this.timer_removing;

	this.item_remove_array = new Array();

	//initial input number
	$(function () {
		$( "input[type='number']" ).change(function() {
			var max = parseInt($(this).attr('max'));
			var min = parseInt($(this).attr('min'));
			var item_id = $(this).attr('item_id');
			if ($(this).val() > max){
				$(this).val(max);
				Obj_ShoppingCart.ChangeQuantity(item_id,$(this));
			}
			else if ($(this).val() < min){
				$(this).val(min);
				Obj_ShoppingCart.ChangeQuantity(item_id,$(this));
			}       
		}); 
	});
}

ShoppingCart.prototype.InvokeSubmitRemove=function(id,obj){
	this.shopping_cart_removing_flag = true;
	clearInterval(this.timer_removing);

	var that = this;
	this.timer_removing = setTimeout(function(){that.SubmitRemove()},2000);
}

ShoppingCart.prototype.SubmitRemove=function(){

	//url:system_url+"/api/index.php/shopping-cart-item-remove?random="+(new Date()).getTime(),
	//data: {item_remove: JSON.stringify(this.item_remove_array)},
	//dataType:'json',
	//console.log("sub del");
	
	$.ajax({
		type: "POST",
		url:system_url+"/shoppingcart/deletes",
		data: {item_remove: this.item_remove_array},
		headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		context : this,
		success: function(data) {
			this.shopping_cart_removing_flag = false;
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
		}
	});

	this.item_remove_array = new Array();
}

ShoppingCart.prototype.InvokeSubmitQuantity=function(id,obj){
	this.shopping_cart_changing_flag = true;
	clearInterval(this.timer_refresh);

	var that = this;
	this.timer_refresh = setTimeout(function(){that.SubmitQuantity()},2000);
}

ShoppingCart.prototype.SubmitQuantity=function(){

	var item_info_array = new Array();
	var anArray = $("#cart_item").find("tbody").find("tr");
    for(var i=0;i<anArray.length;i++){
        var $v = $(anArray[i]);

        var item_info = new Object();

        item_info.id = $(anArray[i]).attr("item_id");
        item_info.quantity = parseInt($v.find("[class~='zx-zoocart-tableitems-row-quantity']").find("input").val());

        item_info_array.push(item_info);
    }

    //url:system_url+"/api/index.php/shopping-cart-item-quantity-update?random="+(new Date()).getTime(),
    //url:system_url+"/shoppingcart/"+id,

	$.ajax({
		type: "POST",
		url:system_url+"/shoppingcart/updates",
		data: {item_info: JSON.stringify(item_info_array)},
		headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		dataType:'json',
		context : this,
		success: function(data) {
			this.shopping_cart_changing_flag = false;
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
		}
	});
}

ShoppingCart.prototype.Change=function(id,obj){
	var quantity = $(obj).val();
	this.current_tr = $(obj).parent().parent().parent();

	var price = $(this.current_tr).find("[class~='price']").find("span").html();
	$(this.current_tr).find("[class~='total']").find("span").html( $.number((quantity*price),2) );

	calculate_shipping_fee();
	this.InvokeSubmitQuantity();
}

ShoppingCart.prototype.Remove=function(id,obj){

	this.current_tr = $(obj).parent().parent().parent();
	this.item_remove_array.push(id);

	$(this.current_tr).remove();

	calculate_shipping_fee();
	this.InvokeSubmitRemove();
}

function re_order(id){
    var ret = confirm(re_order_tips);
    if( !ret ) return;

    $.ajax({
        type: "GET",
        url:system_url+"/shoppingcart/reorder/"+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType:'json',
        context : this,
        success: function(data) {
            //console.log(data);
            if( data.state=="success" ) window.location = root_url+"/shoppingcart";
            else if( data.state=="denied" ) return return_to_home();
            
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
        }
    });
}
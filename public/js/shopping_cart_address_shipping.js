
$(document).ready(function(){
	/*
	var validation_public_option = {
		rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            state: "required",
            address1: {
                required: true,
                minlength: 5
            },
            zipcode: {
                required: true,
                minlength: 4,
                digits: true
            },
            country: "required",
            
        },
        messages: {
            name: msg_validation_name,
            email: msg_validation_email,
            state: msg_validation_state,
            address1: msg_validation_address1,
            zipcode: msg_validation_zipcode,
            country: msg_validation_country,
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            //error.addClass( "help-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                //error.insertAfter( element );
                error.appendTo( $( element ).parents( ".form-group" ).find("label") );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
            
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    };


    var validation_option_new = validation_public_option;
    $.extend(validation_option_new,{
	    submitHandler: function(form) {
            new_address('shipping','formNewShippingAddress');
        }
	});
	$( "#formNewShippingAddress").validate(validation_option_new);

	var validation_option_edit = validation_public_option;
    $.extend(validation_option_edit,{
	    submitHandler: function(form) {
            edit_address('formEditShippingAddress');
        }
	});
	$( "#formEditShippingAddress").validate(validation_option_edit);
    */

    new_country = $("#shipping_address_current").find("input[name='country']").val();
    calculate_shipping_fee();

});

var new_country = null;
function choose_shipping_address(obj){
    var previous_country = $("#shipping_address_current").find("input[name='country']").val();
    $("#shipping_address_current").html( $(obj).parent().prev().html() );
    $("#shipping_address_current").next().children().trigger("click");
    new_country = $("#shipping_address_current").find("input[name='country']").val();
    if( new_country!=previous_country ){
        calculate_shipping_fee();
    }
}

function calculate_shipping_fee(){
    var q = 0;
    var subtotal = 0;
    var anArray = $("#cart_item").find("tbody").find("tr");
    for(var i=0;i<anArray.length;i++){
        var $v = $(anArray[i]);
        q += parseInt($v.find("[class~='zx-zoocart-tableitems-row-quantity']").find("input").val());
        subtotal += parseFloat($v.find("[class~='total']").find("span").html());
    }


    var shipping_fee = 0;
    if( new_country!="US" ){
        shipping_fee = q*0.7;
        $("span.shipping_fee").html( $.number( shipping_fee, 2 ) );
        subtotal += shipping_fee;
    }

    $("span.sub_total").html( $.number(subtotal,2) );
    $("span.total").html( $.number(subtotal,2) );
}

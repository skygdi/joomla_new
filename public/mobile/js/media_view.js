
function media_view_comment_submit(id,obj){
    var content = $(obj).parent().find("textarea").val();
    if( content.length<2 || content.length>250 ){
        //JMAlert2(msg_content_length_not_fit);
        $(".div_tip_alert").find("p").html(msg_content_length_not_fit);
        $(".div_tip_alert").popup( "open" );
        return;
    }
    //console.log(content);return;

    $.ajax({
        type: "POST",
        url: root_url+"/comment",
        data: "content="+content+"&id="+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(dataObj){
            //dataObj = eval("("+msg+")");
            //console.log(dataObj);
            //console.log(msg);
            //var dataObj = JSON.parse(msg);
            //console.log(dataObj);
            if( dataObj.state=="success" ){
                MediaViewRefreshData();
            }
            else{
                JMAlert(dataObj["message"]);
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });

}

var comment_remove_id = null;
var comment_remove_li = null;
function CommentRemoveConfirming(id,obj){
	comment_remove_id = id;
	comment_remove_li = obj;

	$(".div_tip_confirm").find("p").html(msg_delete_confirm);
	$(".div_tip_confirm").popup( "open" );
	return;

}

function CommentRemove(){
	$(".div_tip_confirm").popup( "close" );

	$.ajax({
		type: "DELETE",
		url: root_url+"/comment/"+comment_remove_id,
		headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
		success: function(msg){
		  console.log(msg);

		  //alert("Success");
		  if( msg=="success" ){
		  	$(comment_remove_li).parent().next().remove();
		  	$(comment_remove_li).parent().remove();

		  }
		  //$(obj).parent().parent().parent().remove();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
		  //alert("fail:"+XmlHttpRequest.responseText);
		}
	});
}

function comment_permission_set(id,obj){
	//alert(value);
	//console.log( $(obj).prop('checked') );
	var v = 0;
	if( $(obj).prop('checked')==true ) v = 1;

	$.ajax({
		type: "GET",
		url: root_url+"/comment/permission/"+id+"/"+v,
		data: "",
		success: function(msg){
		  //alert(msg);
		  //dataObj = eval("("+msg+")");
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
		  //alert("fail:"+XmlHttpRequest.responseText);
		}
	});
}
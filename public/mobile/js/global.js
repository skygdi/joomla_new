
//Panel menu
$(document).on("panelbeforeopen","#sideMenu",function(){ // When entering pagetwo
	$(".c-hamburger").addClass("is-active");
});
$(document).on("panelbeforeclose","#sideMenu",function(){ // When entering pagetwo
	$(".c-hamburger").removeClass("is-active");
});

var current_item_id = null;
var current_user = null;
function switching_data(alias,item_id,created_by,name){
    var a_array = $("#item_dialog").find("a");

    current_item_id = item_id;
    current_user = created_by;
    
    $("#item_dialog").find("font:lt(1)").html(name);
}

function Step2PostCard(){
    current_item_id = "";
    $.mobile.changePage('#div_page_new_postcard');
}

function JMAlert(message){

    $("#div_tip").find("p").html(message);

    $.mobile.changePage('#div_tip', {transition: 'flip'});
    //$.mobile.changePage('#div_tip', {transition: 'popup'});
}

function SearchByTag( value ){
    current_search = value;
    //$.mobile.navigate( "#div_page_search" );
    if( $(current_page_id).attr("id")=="div_page_search" ){
        //current page is search
        SearchRefreshData(0);
        //$("#sideMenu").panel("close");
    }
    else{
        $.mobile.navigate( "#div_page_search" );
    }
    return;

}

function Search( obj ){
    //console.log(current_page_id);
    //console.log($(current_page_id).attr("id"));
    current_search = $(obj).find("input:lt(1)").val();

    //console.log( $(current_page_id).attr("id") );

    if( $(current_page_id).attr("id")=="div_page_search" ){
        //current page is search
        SearchRefreshData(0);
        $("#sideMenu").panel("close");
    }
    else{
        
        $.mobile.navigate( "#div_page_search" );
    }
    
    return false;
}


function init_carousel(){
    $('.owl-carousel').owlCarousel({
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        loop:true,
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:5
            }
        }
    });
}

function Home(){
    $.mobile.changePage('#div_page_home');
}

function RespondCheck(status,force_to_login){
    if( force_to_login && status==401){
        user = null;
        ReRenderHeader(current_page_id);
        Home();
        return false;
    }
    else{
        ReRenderHeader(current_page_id);
        return true;
    }
}

function ReRenderHeader( current_page ){
    if( user!=null ){
        $(current_page).find("a[tag='avatar']").find("img").attr("src",root_url+"/user/avatar/"+user.id);
        $(current_page).find("a[tag='avatar']").show();
        $(current_page).find("a[tag='login']").hide();

        //menu
        $("#sideMenu").find("a.user").show();
    }
    else{
        $(current_page).find("a[tag='login']").show();
        $(current_page).find("a[tag='avatar']").hide();

        $("#sideMenu").find("a.user").hide();
    }
}



/*
var flag_for_ajaxComplete = "";
$( document ).ajaxComplete(function( event, xhr, settings ) {
    //console.log(flag_for_ajaxComplete);
    if( flag_for_ajaxComplete=="" ) return;

    //procedure of after ajax complete by custom page
    //console.log("ajaxComplete ");
    if( flag_for_ajaxComplete=="new address" ) window.location = "#div_page_addresses_management";
    else if( flag_for_ajaxComplete=="edit address" ) window.location = "#div_page_addresses_management";

    //clear flag
    flag_for_ajaxComplete = "";
});
*/
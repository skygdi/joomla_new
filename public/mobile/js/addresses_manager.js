
$(document).on("pagebeforeshow","#div_page_addresses_management",function(){

    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }

    if( flag_refresh_addresses_management ){
        RefreshAddresses();
        flag_refresh_addresses_management = false;
    }
    
    
});





var new_address_type = "";
function GoPageAddAddress(type){
    new_address_type = type;
    $.mobile.changePage('#div_page_new_address');
}

function RemoveAddress(id){
    var answer = confirm(msg_delete_confirmation)
    if (!answer){
        return;
    }

    $.ajax({
        type: "DELETE",
        url: system_url+"/address/"+id,
        context : this,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            //console.log(data);
            if( data.state=="success" ){
                RefreshAddresses();
            }
            
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
        }
    });
}

function ChangeDisplayAsReturnAddress(obj,id){

    if( !obj.checked ) return;

    //if( obj.checked ){
        $("#div_page_addresses_management").find(".billing").find("input[type='checkbox'][name='display_as_return_address']").not(obj).prop("checked",false);
        $("#div_page_addresses_management").find(".billing").find("input[type='checkbox'][name='display_as_return_address']").not(obj).flipswitch("refresh");
        //console.log("true");
    //}
    //else{
    //}

    $.ajax({
        type: "PUT",
        url: system_url+"/address/"+id+"/default_return_address",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            //if( msg=="denied" ) return RefreshAddresses();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function ChangeDefaultAddress(obj,type,id){
    //console.log(obj);
    //obj = obj || window.event;
    //
    if( !obj.checked ) return;
    
    //if( obj.checked ){
    $("#div_page_addresses_management").find("."+type).find("input[type='checkbox'][name='is_default']").not(obj).prop("checked",false);
    $("#div_page_addresses_management").find("."+type).find("input[type='checkbox'][name='is_default']").not(obj).flipswitch("refresh");
    //}
    //else{
    //}

    //if( isTriggered ) return;

    //console.log(isTriggered);
    //return;

    $.ajax({
        type: "PUT",
        url: system_url+"/address/"+id+"/default_address",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            //if( msg.state=="success" ) return RefreshAddresses();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function GoPageEditAddress(type,id){
    new_address_type = type;
    current_address_id = id;
    $.mobile.navigate( "#div_page_edit_address" );

}

var recaptcha_response_password = "";
var recaptcha_response_username = "";

var recaptchaVerifyCallback_username = function(response) {
    recaptcha_response_username = response;
};
var recaptchaVerifyCallback_password = function(response) {
    recaptcha_response_password = response;
};


//initial recaptcha
var CaptchaCallback = function() {
    grecaptcha.render('RecaptchaField_forgot_password', {'sitekey' : google_recaptcha_sitekey,'callback' : recaptchaVerifyCallback_password});
    grecaptcha.render('RecaptchaField_forgot_username', {'sitekey' : google_recaptcha_sitekey,'callback' : recaptchaVerifyCallback_username});
};

function ForgotUsername(){
    var email = $("#div_page_forgot_your_username").find("input[name='email']").val();
    if( recaptcha_response_username.length==0 ){
        $("#div_page_forgot_your_username").find(".div_tip_alert").find("p").html(msg_interact_the_recaptcha);
        $("#div_page_forgot_your_username").find(".div_tip_alert").popup( "open" );
        return;
    }

    $.ajax({
        type: "POST",
        data:{
            "g-recaptcha-response":recaptcha_response_username,
            "email":email,
        },
        url:system_url+"/api/index.php/forgot-username?email="+email+"&random="+(new Date()).getTime(),
        success: function(data) {
            if( data=="email not exist" ){
                $("#div_page_forgot_your_username").find(".div_tip_alert").find("p").html(msg_email_not_exist);
                $("#div_page_forgot_your_username").find(".div_tip_alert").popup( "open" );
            }
            else if( data=="Recaptcha denied" ){
                $("#div_page_forgot_your_username").find(".div_tip_alert").find("p").html(msg_recaptcha_error);
                $("#div_page_forgot_your_username").find(".div_tip_alert").popup( "open" );
            }
            else if( data=="success" ){
                $("#div_page_forgot_your_username").find(".div_tip_alert").find("p").html(msg_success);
                $("#div_page_forgot_your_username").find(".div_tip_alert").popup( "open" );
            }
            else{
                $("#div_page_forgot_your_username").find(".div_tip_alert").find("p").html("unknow error");
                $("#div_page_forgot_your_username").find(".div_tip_alert").popup( "open" );
            }
        }
    });
}

function ForgotPassword(){
    var email = $("#div_page_forgot_your_password").find("input[name='email']").val();
    //var v = recaptcha_password.getResponse();
    if( recaptchaVerifyCallback_password.length==0 ){
        $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_interact_the_recaptcha);
        $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
        return;
    }

    // Regular expression to validate email input value
    var re = /^(([^()[\]\\.,;:\s@\"]+(\.[^()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var test = re.test(email);
    if(!test) {
        $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_a_valid_email_address);
        $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
        return;
    }

    $.ajax({
        type: "POST",
        data:{
            "g-recaptcha-response":recaptchaVerifyCallback_password,
            "email":email,
        },
        url:system_url+"/api/index.php/forgot-password?email="+email+"&random="+(new Date()).getTime(),
        success: function(data) {
            if( data=="email not exist" ){
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_email_not_exist);
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
            else if( data=="Recaptcha denied" ){
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_recaptcha_error);
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
            else if( data=="success" ){
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_success);
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
            else{
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html("unknow error");
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
        }
    });

}

function ForgotPassword(){
    var email = $("#div_page_forgot_your_password").find("input[name='email']").val();
    var v = grecaptcha.getResponse();
    if( v.length==0 ){
        $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_interact_the_recaptcha);
        $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
        return;
    }

    // Regular expression to validate email input value
    var re = /^(([^()[\]\\.,;:\s@\"]+(\.[^()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var test = re.test(email);
    if(!test) {
        $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_a_valid_email_address);
        $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
        return;
    }

    $.ajax({
        type: "POST",
        data:{
            "g-recaptcha-response":v,
            "email":email,
        },
        url:system_url+"/api/index.php/forgot-password?email="+email+"&random="+(new Date()).getTime(),
        success: function(data) {
            if( data=="email not exist" ){
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_email_not_exist);
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
            else if( data=="Recaptcha denied" ){
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_recaptcha_error);
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
            else if( data=="success" ){
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html(msg_success);
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
            else{
                $("#div_page_forgot_your_password").find(".div_tip_alert").find("p").html("unknow error");
                $("#div_page_forgot_your_password").find(".div_tip_alert").popup( "open" );
            }
        }
    });

}
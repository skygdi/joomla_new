
/*
function page_new_media_add_tag(){
    var new_tag = $("input[name='tag']").val();
    if( new_tag=="" || new_tag.length<3 ){
        $("input[name='tag']").focus();
        return;
    }
    $("input[name='tag']").val("");

    $("ul[class='tags']").append('<li class="tag label label-success tagB"><input type="hidden" name="tags[]" value="'+new_tag+'">'+new_tag+'<a class="close" href="#" onclick="page_new_media_remove_tag(this)">×</a></li>');

    $("input[name='tag']").focus();
}
function page_new_media_remove_tag(obj){
    $(obj).parent().remove();
}
*/

function AddTag(obj){
    var new_tag = $(obj).parent().find("input[name='tag']").val();
    if( new_tag=="" || new_tag.length<3 ){
        $(obj).parent().find("input[name='tag']").focus();
        return;
    }
    $(obj).parent().find("input[name='tag']").val("");

    $(obj).parent().find("ul[class='tags']").append('<li class="tag label label-success tagB"><input type="hidden" name="tags[]" value="'+new_tag+'">'+new_tag+'<a class="close" href="#" onclick="RemoveTag(this)">×</a></li>');

    $(obj).parent().find("input[name='tag']").focus();
}

function RemoveTag(obj){
    $(obj).parent().remove();
}
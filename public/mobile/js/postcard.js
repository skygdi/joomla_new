
function AddToCart(id){
    
    $.ajax({
        type: "POST",
        url:system_url+"/mobile/shoppingcart",
        data:{
            "id":id,
        },
        context : this,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            //console.log(data);
            if( data.state=="success" ){
                $("#div_page_postcard_management").find(".div_tip_alert").popup( "open" );
            }
            //JSON_ShoppingCartMini = data.data;
            //this.RenderSumarry(data.sumarry);
            //this.Render();

            //$(that).find("i:lt(1)").attr("class","uk-icon-shopping-cart");
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
        }
    });
}

function new_post_card_submit(){
    if( $("#div_page_new_postcard").find("input[name='id']").val()=="" ){
        $("#div_page_new_postcard").find(".div_tip_alert").find("p").html(msg_select_image);
        $("#div_page_new_postcard").find(".div_tip_alert").popup( "open" );
        //JMAlert(msg_select_image);
        return false;
    }

    if( $("#div_page_new_postcard").find("input[name='name']").val()=="" ){
        //JMAlert(msg_input_name);
        $("#div_page_new_postcard").find(".div_tip_alert").find("p").html(msg_input_name);
        $("#div_page_new_postcard").find(".div_tip_alert").popup( "open" );
        return false;
    }

    var tl = $("#div_page_new_postcard").find("textarea[name='content']").val().length;
    if( tl<2 || tl>250 ){
        //JMAlert(msg_content_length_not_fit);
        $("#div_page_new_postcard").find(".div_tip_alert").find("p").html(msg_content_length_not_fit);
        $("#div_page_new_postcard").find(".div_tip_alert").popup( "open" );
        return false;
    }

    //return;

    $.ajax({
        type: "POST",
        url:root_url + "/mobile/postcard",
        data:{
            "id":$("#div_page_new_postcard").find("input[name='id']").val(),
            "name":$("#div_page_new_postcard").find("input[name='name']").val(),
            "content":$("#div_page_new_postcard").find("textarea[name='content']").val(),
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg.state=="success" ){
                //$.mobile.navigate( "#div_page_home" );
                $.mobile.navigate( "#div_page_postcard_management" );
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });

    //return true;
}

//var xhr = new XMLHttpRequest();
function NewMedia(){
    if( $("#div_page_new_media").find("input[type='file']").val()=="" ){
        $("#div_page_new_media").find(".div_tip_alert").find("p").html(msg_select_image);
        $("#div_page_new_media").find(".div_tip_alert").popup( "open" );
    }
    else if( $("#div_page_new_media").find("input[name='name']").val()=="" ){
        $("#div_page_new_media").find(".div_tip_alert").find("p").html(msg_input_name);
        $("#div_page_new_media").find(".div_tip_alert").popup( "open" );
    }
    var formData = new FormData();
    var fileSelect = document.getElementById('file_image');
    var files = fileSelect.files;
    var file = files[0];
    //formData.append('file_image', fileSelect, fileSelect.name);
    //console.log(file.size);
    //
    // Add the file to the request.
    formData.append('file_image', file, file.name);
    //Seachable
    if( $("form[name='media_new']").find("input[name='searchable']").val()=="on" ){
        formData.append('searchable', "1");
    }
    else{
        formData.append('searchable', "0");
    }

    //Tag
    //var tag_array = $("form[name='media_new']").find("li[class^='tag label']");
    var tag_element_array = $("form[name='media_new']").find("input[name^='tags']");
    var tag_array = new Array();
    for(var i=0;i<tag_element_array.length;i++){
        var $v = $(tag_element_array[i]);
        tag_array.push( $v.val() );
    }
    formData.append('keywords', JSON.stringify(tag_array));
    //tag label label-success
    
    formData.append('name', $("form[name='media_new']").find("input[name='name']:lt(1)").val() );
    //formData.append('file_image', file, file.name);

    // Set up the AJAX request.
    
    //console.log( $('meta[name="csrf-token"]').attr('content') );return false;
    
    // Open the connection.
    var xhr = new XMLHttpRequest();
    xhr.open('POST', $("form[name='media_new']").attr('action'), false);
    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
    //xhr.setRequestHeader('X-CSRF-TOKEN',$('meta[name="csrf-token"]').attr('content') );
    
    // Set up a handler for when the request finishes.
    xhr.onload = function () {
        //console.log(xhr.status);
        if (xhr.status === 200) {
            var jsonResponse = JSON.parse(xhr.responseText);
            if( jsonResponse.state=="success" ){
                MediaManagerRefreshData(page);
                window.location = "#div_page_media_management";
            }
            else{
                JMAlert(jsonResponse.message);
            }

            //console.log(jsonResponse);
            //statusDiv.innerHTML = 'The file uploaded successfully.......';
        } else {
            JMAlert("Server error");
            //statusDiv.innerHTML = 'An error occurred while uploading the file. Try again';
        }
    };

    // Send the Data.
    xhr.send(formData);

    return false;
}

function init_carousel_new_postcard(){
    $("#div_page_new_postcard").find('.owl-carousel').owlCarousel({
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:5
            }
        }
    });
}
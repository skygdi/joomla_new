$(document).on("pagebeforeshow","#div_page_shopping_cart",function(){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }

    ShoppingCartRefresh();

    //return;
    
});

function ShoppingCartCheckout(obj){
    //console.log("checkout");
    var addresses = new Object();
    addresses.billing = $("#billing_address_current").val();
    addresses.shipping = $("#shipping_address_current").val();

    var obj_li_array = $("#div_page_shopping_cart").find("[data-role='main']").find("li[item_id]");

    var item_info_array = new Array();
    for(var i=0;i<obj_li_array.length;i++){
        var $v = $(obj_li_array[i]);
        var item = new Object();
        item.id = $v.attr("item_id");
        item.quantity = $v.find("input[name='quantity']").val();
        item_info_array.push(item);
    }



    $.ajax({
        type: "POST",
        url:system_url+"/mobile/order",
        data: {item_info: JSON.stringify(item_info_array),addresses:JSON.stringify(addresses)},
        dataType:'json',
        context : obj,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            $(obj).hide();
            $("#paypal-button").show();
            //console.log(data.order_id);
            //window.location = root_url+"/index.php/orders?task=pay&id="+data.order_id;
            //window.location = root_url+"/index.php/payment/checkout/paypal?id="+data.order_id;
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
        }
    });
}

function ShoppingCartQuantityChange(){
    //var obj_li_array = $(obj).prev().find("li");
    var obj_li_array = $("#div_page_shopping_cart").find("[data-role='main']").find("li[item_id]");
    if( obj_li_array.length==0 ){
        $("#div_page_shopping_cart").find(".div_tip_alert").popup( "open" );
        return;
    }

    var item_info_array = new Array();
    for(var i=0;i<obj_li_array.length;i++){
        var $v = $(obj_li_array[i]);
        var item = new Object();
    
        item.id = $v.attr("item_id");
        item.quantity = $v.find("input[name='quantity']").val();

        item_info_array.push(item);
    }

    $.ajax({
        type: "POST",
        url:root_url+"/mobile/shoppingcart/updates",
        data: {item_info: JSON.stringify(item_info_array)},
        dataType:'json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        context : this,
        success: function(data) {
            //console.log(data);
            if( data[0]=="success" ){
                $("#div_page_shopping_cart").find(".div_tip_ok").find("p").html("success");
                $("#div_page_shopping_cart").find(".div_tip_ok").popup( "open" );
            }
            //ShoppingCartCheckout();

        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //console.log("bad");
            if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
        }
    });

}

function ShoppingCartRefresh(){
    var current_page = $("#div_page_shopping_cart");
    ReRenderHeader(current_page);

    //load data
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/shoppingcart", function () {
        $(this).trigger('create');
    });
}


var shoppingcart_remove_id = null;
function ShoppingCartRemoveConfirmation(id){
    shoppingcart_remove_id = id;
    //$("#div_page_shopping_cart").find(".div_tip_confirm").find("p").html(msg_delete_confirm);
    $("#div_page_shopping_cart").find(".div_tip_confirm").popup( "open" );
}


function ShoppingCartRemove(){
    $("#div_page_shopping_cart").find(".div_tip_confirm").popup( "close" );
    
    $.ajax({
        type: "DELETE",
        url:root_url+"/mobile/shoppingcart/"+shoppingcart_remove_id,
        context : this,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            //console.log(data);
            if( data.state=="success" ){
                //$("#div_page_postcard_management").find(".div_tip_alert").popup( "open" );
                ShoppingCartRefresh();
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            if( XmlHttpRequest.responseText=="denied" ) return return_to_home();
        }
    });
}
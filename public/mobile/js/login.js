
function login(obj){
    $.ajax({
        type: "POST",
        url:system_url + "/login",
        data:{
            "username":$(obj).parent().find("input[name='username']").val(),
            "password":$(obj).parent().find("input[name='password']").val(),
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(dataObj){
            if( dataObj["auth"]==true )
            {
                user = dataObj["user"];
                window.location = "#div_page_home";
            }
            /*
            else
            {
                if( dataObj["result"]=="username and or password error" ) JMAlert(msg_username_or_password_error);
                else JMAlert(dataObj["result"]);
            }
            */
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //console.log(XmlHttpRequest.responseText);
            //console.log(XmlHttpRequest.responseJSON);
            JMAlert(XmlHttpRequest.responseJSON.message);
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });

}

function logout(){
    $.ajax({
        type: "POST",
        url:system_url + "/logout",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            //alert(msg);
            //window.location = root_url+"/index.php/home";
            user = null;
            $("#sideMenu").panel("close");
            window.location = "#div_page_home";

            //Refresh 
            var current_page = $("#div_page_home");
            ReRenderHeader(current_page);
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
            //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}
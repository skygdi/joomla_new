
//This page for user only, it will redirect automatically
$(document).on("pagebeforeshow","#div_page_profile",function(){ // When entering pagetwo
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }
    
    profile.onsubmit = function(event) {
        event.preventDefault();
        profile_submit(this);
    };
    //init user
    $("input[name='name']").val(user.name);
    $("input[name='email']").val(user.email);
    $("input[name='paypal']").val(user.paypal);
    $("textarea[name='description']").val(user.description);

});

function profile_submit(obj){
    if( $(obj).find("input[name='password']").val().length!=0 ){
        if( $(obj).find("input[name='password']").val()!=$(obj).find("input[name='password2']").val() ){
            //password check
            $("#div_tip").find("p:lt(1)").html("{{MLP.S('profile','Password and confirm password not matched')}}");
            $.mobile.changePage('#div_tip', {transition: 'pop'});
            return;
        }
    }

    $("input[name='submit_button']").val(msg_submitting);

    // Create a new FormData object.
    var formData = new FormData();

    //image file
    var files = $("input[name='avatar_file']").prop('files');
    // Loop through each of the selected files.
    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // Check the file type.
        if (!file.type.match('image.*')) {
            continue;
        }

        // Add the file to the request.
        formData.append('avatar_file', file, file.name);
        break;
    }

    formData.append("name",         $(obj).find("input[name='name']").val() );
    formData.append("password",     $(obj).find("input[name='password']").val() );
    formData.append("email",        $(obj).find("input[name='email']").val() );
    formData.append("paypal",       $(obj).find("input[name='paypal']").val() );
    formData.append("description",  $(obj).find("textarea[name='description']").val() );

    //console.log($(obj).find("textarea[name='description']").val());

    // Set up the request.
    var xhr = new XMLHttpRequest();

    // Open the connection.
    xhr.open('POST', $(obj).attr("action") , true);
    xhr.setRequestHeader('X-CSRF-TOKEN',$('meta[name="csrf-token"]').attr('content') );
    // Set up a handler for when the request finishes.
    xhr.onload = function () {
        if (xhr.status === 200) {
            // File(s) uploaded.
            //uploadButton.innerHTML = 'Upload';
            $("input[name='submit_button']").val( $("input[name='submit_button']").attr("value_original") );
            //console.log( $("a[tag='avatar']").length );
            var d = new Date();
            $("a[tag='avatar']").find("img").attr("src",root_url+"/user/avatar/"+user.id+"?=t"+d.getTime());
        } else {
            $("input[name='submit_button']").val( $("input[name='submit_button']").attr("value_original") );
            //console.log(xhr);
            //alert('An error occurred!');
            var data = JSON.parse(xhr.responseText);
            for(var key in data){
                alert(data[key]);
                break;
            }
            //console.log(data);

        }
    };
    xhr.send(formData);
    //console.log(formData);
}

function EditMedia(){
    if( $("#div_page_edit_media").find("input[type='file']").val()=="" ){
        $("#div_page_edit_media").find(".div_tip_alert").find("p").html(msg_select_image);
        $("#div_page_edit_media").find(".div_tip_alert").popup( "open" );
    }
    else if( $("#div_page_edit_media").find("input[name='name']").val()=="" ){
        $("#div_page_edit_media").find(".div_tip_alert").find("p").html(msg_input_name);
        $("#div_page_edit_media").find(".div_tip_alert").popup( "open" );
    }
    var formData = new FormData();
    var fileSelect = document.getElementById('file_image');
    var files = fileSelect.files;
    var file = files[0];
    //formData.append('file_image', fileSelect, fileSelect.name);
    //console.log(file.size);
    //
    // Add the file to the request.
    if( file!=null ) formData.append('file_image', file, file.name);
    //Seachable
    if( $("form[name='media_edit']").find("input[name='searchable']").val()=="on" ){
        formData.append('searchable', "1");
    }
    else{
        formData.append('searchable', "0");
    }

    //Tag
    var tag_element_array = $("form[name='media_edit']").find("input[name^='tags']");
    var tag_array = new Array();
    for(var i=0;i<tag_element_array.length;i++){
        var $v = $(tag_element_array[i]);
        tag_array.push( $v.val() );
    }
    formData.append('keywords', JSON.stringify(tag_array));
    
    formData.append('name', $("form[name='media_edit']").find("input[name='name']:lt(1)").val() );

    formData.append('_method', 'PUT' );

    

    // Set up the AJAX request.
    // Open the connection.
    //console.log("edit");
    //return false;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', $("form[name='media_edit']").attr('action'), false);
    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
    
    
    // Set up a handler for when the request finishes.
    xhr.onload = function () {
        //console.log(xhr.status);
        if (xhr.status === 200) {
            var jsonResponse = JSON.parse(xhr.responseText);
            if( jsonResponse.state=="success" ){
                MediaManagerRefreshData(page);
                window.location = "#div_page_media_management";
            }
            else{
                JMAlert(jsonResponse.message);
            }

            //console.log(jsonResponse);
            //statusDiv.innerHTML = 'The file uploaded successfully.......';
        } else {
            JMAlert("Server error");
            //statusDiv.innerHTML = 'An error occurred while uploading the file. Try again';
        }
    };

    // Send the Data.
    xhr.send(formData);

    return false;
}

function media_management_like(id,obj){
    var total_likes = parseInt($(obj).next().text());
    var cr = "p";

    if( $(obj).hasClass("had_plus") ){
        //Previous liked
        $(obj).removeClass("had_plus");
        total_likes--;
        cr = "p";
    }
    else{
        //Previous disliked
        $(obj).addClass("had_plus");
        total_likes++;
        cr = "n";
    }
    $(obj).next().text(total_likes);

    $.ajax({
        type: "GET",
        url: system_url+"/media/like/"+id+"/"+cr,
        data: "",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(msg){
            if( msg=="denied" ) return Home();
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function media_manager_comment_submit(id,obj){
    var content = $(obj).parent().find("textarea").val();
    if( content.length<2 || content.length>250 ){
        //JMAlert2(msg_content_length_not_fit);
        $(".div_tip_alert").find("p").html(msg_content_length_not_fit);
        $(".div_tip_alert").popup( "open" );
        return;
    }

    $.ajax({
        type: "POST",
        url: root_url+"/comment",
        data: "content="+content+"&id="+id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(dataObj){
            //dataObj = eval("("+msg+")");
            if( dataObj.state=="success" ){
                MediaManagerRefreshData(page);
            }
            else{
                JMAlert(dataObj["result_string"]);
            }
        },
        error:function(XmlHttpRequest,textStatus, errorThrown){
          //alert("fail:"+XmlHttpRequest.responseText);
        }
    });
}

function MediaManagerRefreshData(page){
    initialized_media_management = true;
    var current_page = $("#div_page_media_management");
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/media?page="+page, function ( response, status, xhr ) {
        current_page_id = $(this).parent();
        RespondCheck(xhr.status,true);
        $(this).trigger('create');
    });
}

function user_channel_select_image(id){
    current_item_id = id;
    $.mobile.navigate( "#div_page_new_postcard" );
}



function MediaEdit(id){
    current_item_id = id;
    $.mobile.changePage('#div_page_edit_media');
}

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:5
            }
        }
    });
});
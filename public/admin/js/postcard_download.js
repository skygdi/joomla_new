
$(document).ready(function () {
    //CaptureProcess();
});

var ifc = 0;
function CaptureProcess(){
	var anArray = $(".container_backside");
	var $v = $(anArray[ifc]);
	if( ifc>=anArray.length ) return;
	ifc++;

	//copy the content to capture container
	$(".div_container_backside").html( $v.html() );
	//show for image capture
	$(".div_container_backside").show();

	//Rendering
	html2canvas( $(".div_container_backside"), {
		onrendered: function(canvas) {
			var item_id = $($v).attr("iid");
			//Store the image
			$("[tag='"+item_id+"'][class='new_image_container']").append(canvas);

			//Clean the capture container
			$(".div_container_backside").html("");
			$(".div_container_backside").hide();

			//Show the download button
			$("a[tag='"+item_id+"']").show();

			//Next
			CaptureProcess();
		}
	});

}

function DownloadCanvas(link, id, filename) {
	var image_obj = $("[tag='"+id+"'][class='new_image_container']");
	var obj_canvas = $(image_obj).find("canvas:lt(1)");
    link.href = obj_canvas[0].toDataURL();
    link.download = id+"_"+filename;
}

/*
var item_id = null;
function GenerateDownload(id){
	item_id = id;

	//copy current picture and text to shooter DIV
	$(".div_container_backside").html( $("[class='container_backside'][iid='"+item_id+"']").html() );
	//show for image capture
	$(".div_container_backside").show();
	//return;
	//capture
	html2canvas( $(".div_container_backside") , {
		onrendered: function(canvas) {
			//capture completed 
		
			//copy the whole capture to DIV
			$("[tag='"+item_id+"'][class='new_image_container']").append(canvas);
			//$("[tag='"+item_id+"'][class='new_image_container']").show();
			//hide the shooter DIV
			$(".div_container_backside").hide();

			//show download button
			$("a[class*='download_canvas_button'][tag='"+item_id+"']").show();
			$("a[class*='download_front_button'][tag='"+item_id+"']").show();
			

			//$("[tag='download-canvas'][tag='"+item_id+"'][class='download-canvas']").append(canvas);
			
		}
	});
}

*/
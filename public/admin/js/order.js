$(document).ready(function(){
   
    $('#daterange-btn').daterangepicker({
	    ranges   : {
	      'Today'       : [moment(), moment()],
	      'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	      'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
	      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	      'This Month'  : [moment().startOf('month'), moment().endOf('month')],
	      'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	    },
	    startDate: moment().subtract(29, 'days'),
	    endDate  : moment()
    },
    function (start, end) {
        //$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY');
        $('#daterange-btn span').html(start.format('MM-DD-YYYY') + ' ~ ' + end.format('MM-DD-YYYY'))
        $("input[name='date_from']").val( start.format('MM-DD-YYYY') );
        $("input[name='date_to']").val( end.format('MM-DD-YYYY') );
    });

    //Init 
    if( $("input[name='date_from']").val()!="" ) $('#daterange-btn').data('daterangepicker').setStartDate( $("input[name='date_from']").val() );
    if( $("input[name='date_to']").val()!="" ) $('#daterange-btn').data('daterangepicker').setEndDate( $("input[name='date_to']").val() );
    if( $("input[name='date_from']").val()!="" && $("input[name='date_to']").val()!="" ){
    	$('#daterange-btn span').html( $("input[name='date_from']").val() + ' ~ ' + $("input[name='date_to']").val() );
	}

    $("#form_filter").on('reset', function(e)
	{
		$("input[name='date_from']").val("");
		$("input[name='date_to']").val("");
	    change_filter();
	});

	$("select[name='pay_state']").val( $("select[name='pay_state']").attr("value") );
});
function change_filter(){
	window.location.href = admin_path+"/order?"+$("#form_filter").serialize();
}
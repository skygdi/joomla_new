function RemoveOrderItem(id){
	if( !confirm('Are you sure?') ) return;

	$.ajax({
		type: "DELETE",
		url:admin_path+"/order_item/"+id,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		success: function(msg){
			//alert(msg);
			//console.log(msg);
			window.location.reload();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
		}
	});

}
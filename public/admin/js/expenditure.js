
$(document).ready(function(){
	//$('select[name=user_id]').selectpicker();
});

function RemovePayment(id){
	if( !confirm('Are you sure?') ) return;
	
	$.ajax({
		type: "DELETE",
		url:admin_path+"/expenditure/"+id,
		headers: {
      		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		success: function(msg){
			window.location.reload();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
		}
	});
}

function PaymentAdd(){
	$.ajax({
		type: "POST",
		url:admin_path+"/expenditure",
		data:$("#form_add").serialize(),
		headers: {
      		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		success: function(msg){
			window.location.reload();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
		}
	});

}
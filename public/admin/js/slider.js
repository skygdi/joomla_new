function SliderSuspend(obj,id){
	//console.log( $(obj).is(':checked') );
	//return;
	var value = 0;
	if( $(obj).is(':checked') ) value = 1;
	else valuce =  0;

	$.ajax({
		type: "GET",
		url:admin_path+"/slider/"+id+"/suspend/"+value,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		success: function(msg){
			//window.location.reload();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
		}
	});

}

function RemoveSlider(id){
	if( !confirm('Are you sure?') ) return;
	
	$.ajax({
		type: "DELETE",
		url:admin_path+"/slider/"+id,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
		success: function(msg){
			window.location.reload();
		},
		error:function(XmlHttpRequest,textStatus, errorThrown){
			//alert("fail:"+XmlHttpRequest.responseText);
		}
	});

}

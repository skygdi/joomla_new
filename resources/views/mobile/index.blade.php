<!DOCTYPE HTML>
<html lang="en-gb" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('vendor/jquery.mobile-1.4.5/jquery.mobile-1.4.5.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fortawesome/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('mobile/css/mobile.css') }}">
    <link rel="stylesheet" href="{{ asset('mobile/css/postcard-management.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owl.carousel/owl.carousel.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('mobile/css/keyword.css') }}">

    <script type="text/javascript" src="{{ asset('js/jquery.min.1.12.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/html2canvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/postcard-download.js') }}"></script>

    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>

    <script type="text/javascript" src="{{ asset('mobile/js/global.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/global_mobile.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('mobile/js/media_new.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/login.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/profile.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/postcard_keyword.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/postcard.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/media_management.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/media_view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/shopping_cart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/forgot.js') }}"></script>
    <script type="text/javascript" src="{{ asset('mobile/js/addresses_manager.js') }}"></script>

    <script type="text/javascript">
        var timer_keep_alive;
        $( document ).delegate("#div_page_home", "pageinit", function() {
            init_carousel();

            clearInterval(timer_keep_alive);
            timer_keep_alive = setInterval('keep_alive()',60*5*1000);
        });


        </script>
        
        <script type="text/javascript">
        var system_url = "{{URL('/')}}";
        var root_url = system_url;
        var google_recaptcha_sitekey = "{{$google_recaptcha_sitekey}}";
        var page = 0;
        @if( Auth::check() )
        var user = {!! json_encode(Auth::user()->toArray()) !!};
        user.paypal = '{{Auth::user()->Profile()->first()->paypal}}';
        user.description = '{{Auth::user()->Profile()->first()->description}}';
        @else
        var user = null;
        @endif

        var current_page_id = null;
        $(document).on("pagebeforeshow","#div_page_home",function(){
            //Re render the header
            var current_page = $("#div_page_home");
            //ReRenderHeader(current_page);

            current_page_id = $("#div_page_home");
            RespondCheck(200,false);
            
        });
    </script>
</head>

<body>

<div data-role="page" id="div_page_home">
    <div data-role="header" class="content_header">
        <div style="padding-top:2px;padding-bottom: 3px;">
            <img src="{{URL('/')}}/images/logo.png" alt="UR Works" style="max-height:30px;">
        </div>
        <div style="position: absolute;top:0px;left:0px;">
            <a href="#sideMenu" class="ui-btn ui-btn-inline" style="margin: 0px;padding: 0px;border: none;">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div style="position: absolute;top:7px;right:10px;">
            <a href="#div_page_profile" style="font-size: 25px;color: black;display: none;" tag="avatar">
                <img src="" style="max-width:28px;">
            </a>
            <a href="#div_page_login" style="font-size: 25px;color: black;" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    
    <div data-role="main" class="ui-content" style="padding-top:5px;">
        <h2>@lang('Top Photos'):</h2>
        <div class="owl-carousel owl-theme">
            @foreach ($vars["result"]["click"] as $r)
            <div class="item" style="width:100%;height:180px;position: relative;">
                <a data-transition="flip" href="#item_dialog" onclick="switching_data('{{ $r["alias"] }}','{{ $r["id"] }}','{{ $r["user_id"] }}','{{ $r["name"] }}')">
                    <img src="{{ $r->image_cache_url }}" alt="{{ $r["alias"] }}" width="180" height="180"/>
                </a>
            </div>
            @endforeach
        </div>
        
        <h2>@lang('Newest'):</h2>
        <div class="owl-carousel owl-theme">
            @foreach ($vars["result"]["newest"] as $r)
            <div class="item" style="width:100%;height:180px;position: relative;">
                <a data-transition="flip" href="#item_dialog" onclick="switching_data('{{ $r["alias"] }}','{{ $r["id"] }}','{{ $r["user_id"] }}','{{ $r["name"] }}')">
                    <img src="{{ $r->image_cache_url }}" alt="{{ $r["alias"] }}" width="180" height="180"/>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div data-role="page" data-dialog="true" id="item_dialog" >
    <div role="main" class="ui-content">
        <ul data-role="listview">
            <li><a href="#div_page_media_view"><i class="fa fa-eye" aria-hidden="true"></i>@lang('View')</a></li>
            <li><a href="#div_page_new_postcard"><i class="fa fa-plus"></i>@lang('Postcard')</a></li>
            <li><a href="#div_page_user_channel"><i class="fa fa-user" aria-hidden="true"></i><font></font></a></li>
        </ul>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a" style="margin-top:30px;">@lang('Cancel')</a>
    </div>
</div>

@include('mobile.partial.side_menu')
@include('mobile.page.login')
@include('mobile.page.forgot_your_password')
@include('mobile.page.forgot_your_username')
@include('mobile.page.profile')
@include('mobile.page.gallery')
@include('mobile.page.new_media')
@include('mobile.page.new_address')
@include('mobile.page.media_view.main')
@include('mobile.page.postcard_new.main')
@include('mobile.page.media_management.main')
@include('mobile.page.media_management.edit')
@include('mobile.page.user_channel.main')
@include('mobile.page.postcard_management.main')
@include('mobile.page.shopping_cart.main')
@include('mobile.page.orders.main')
@include('mobile.page.orders_detail.main')
@include('mobile.page.search.main')
@include('mobile.page.addresses_management.main')
@include('mobile.page.addresses_management.edit')





<div class="content_header" style="display:none;">
    
</div>

<div data-role="page" data-dialog="true" id="div_tip" >
    <div role="main" class="ui-content">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a" style="margin-top:30px;">OK</a>
    </div>
</div>


<!-- the postcard-management shooter DIV -->
<div class="div_container_backside"></div>

</body>

</html>
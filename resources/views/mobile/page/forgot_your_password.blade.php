
<script type="text/javascript">
var msg_success = "@lang('Email have been sent, please check your mail')";
var msg_email_not_exist = "@lang('email not exist')";
var msg_interact_the_recaptcha = "@lang('Pleaase interact the recaptcha')";
var msg_recaptcha_error = "@lang('Recaptcha denied')";
var msg_a_valid_email_address = "@lang('Please enter a valid email address')";


$(document).on("pagebeforeshow","#div_page_forgot_your_password",function(){
    var current_page = $("#div_page_forgot_your_password");
    current_page_id = $(current_page).attr("id");
    ReRenderHeader(current_page);
});
</script>

<div data-role="page" id="div_page_forgot_your_password">
    
    <div data-role="header" class="content_header">
        <div class="title">
            @lang('Forgot your password?')
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        <label for="username">@lang('Email'):</label>
        <input type="email" name="email" data-clear-btn="true">
        <div id="RecaptchaField_forgot_password"></div>
        <input type="button" onclick="ForgotPassword()" data-inline="true" value="@lang('SUBMIT')" data-role="none" class="ui-btn ui-input-btn ui-corner-all ui-shadow ui-btn-inline uk-btn">
    </div>

    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

</div>
<ul data-role="listview">
    <li style="margin-top:15px;">
        <a class="ui-btn" style="text-align: center;">
            <span>0011{{$data["id"]}}</span>
        </a>
    </li>

    @foreach( $data->OrderItem()->get() as $item )
    <li class="shopping-cart-data" item_id="{{$item['id']}}">
        <div>
            <a class="ui-btn">
                <i class="fa fa-picture-o" aria-hidden="true" style=""></i>
                <span style="">{{$item["name"]}}</span>
            </a>
        
        </div>
        <div>
            <a class="ui-btn">
                <i class="fa fa-balance-scale" aria-hidden="true"></i>
                <span style="">{{$item["quantity"]}}</span>
            </a>
        </div>
        <div>
            <a class="ui-btn">
                <i class="fa fa-usd" aria-hidden="true" style=""></i>
                <span style="">{{$item["price"]}}</span>
            </a>
        </div>
        <div>
            <a class="ui-btn">
                <i class="fa fa-shopping-bag" aria-hidden="true" style=""></i>
                <i class="fa fa-usd" aria-hidden="true" style=""></i>
                <span style="">{{ number_format($item["quantity"]*$item["price"],2, '.', ',') }}</span>
            </a>
        </div>
    </li>
    @endforeach

    <li style="margin-top:15px;">
        <a class="ui-btn">
            <span style="float:left;">@lang('Payment Fee')</span>
            <span style="float:right;">$0.00</span>
        </a>
        <a class="ui-btn">
            <span style="float:left;">@lang('Subtotal')</span>
            <span style="float:right;">${{ number_format($data->Subtotal(), 2, '.', ',') }}</span>
        </a>
        <a class="ui-btn">
            <span style="float:left;">@lang('Shipping Fee')</span>
            <span style="float:right;">${{ number_format($data->ShippingFee(), 2, '.', ',') }}</span>
        </a>
        <a class="ui-btn">
            <span style="float:left;">@lang('TAXES')</span>
            <span style="float:right;">$0.00</span>
        </a>
        <a class="ui-btn">
            <span style="float:left;">@lang('Total')</span>
            <span style="float:right;"><b>${{ number_format($data->Total(), 2, '.', ',') }}</b></span>
        </a>
    </li>

    <li style="margin-top:15px;">
        <a class="ui-btn">
            <span>@lang('Billing Address'):</span>
        </a>
        <a class="ui-btn">
            {{ $data["BA_name"] }},{{ $data["BA_email"] }}
        </a>
    </li>

    <li style="margin-top:15px;">
        <a class="ui-btn">
            <span>@lang('Shipping Address'):</span>
        </a>
        <a class="ui-btn">
            {{ $data["SA_name"] }},{{ $data["SA_email"] }}
        </a>
    </li>


</ul>




<script type="text/javascript">
$(document).on("pagebeforeshow","#div_page_orders_detail",function(){
    var current_page = $("#div_page_orders_detail");
    current_page_id = current_page;
    //page = 0;
    ReRenderHeader(current_page);
    //load data
    OrdersDetailRefreshData();
});

var current_order_id = null;
function OrdersDetailRefreshData(){
    if( current_order_id==null ) return;
    var current_page = $("#div_page_orders_detail");
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/order/"+current_order_id, function ( response, status, xhr ) {
        RespondCheck(xhr.status,true);
        $(this).trigger('create');
    });
}
</script>

<div data-role="page" id="div_page_orders_detail">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('Order')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
    </div>
    
    
</div>

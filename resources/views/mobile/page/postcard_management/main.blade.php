
<script type="text/javascript">
$(document).on("pagebeforeshow","#div_page_postcard_management",function(){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }
    
    var current_page = $("#div_page_postcard_management");
    current_page_id = current_page;
    ReRenderHeader(current_page);

    //load data
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/postcard", function ( response, status, xhr ) {
        $(this).trigger('create');
        RespondCheck(xhr.status,true);

        
    });


});

</script>

<div data-role="page" id="div_page_postcard_management">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('MY POSTCARDS')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
    </div>

    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('Add Success')</a>
    </div>
    
    
</div>

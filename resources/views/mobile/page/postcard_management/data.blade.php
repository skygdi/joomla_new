
@foreach ($items as $item)
<div data-role="collapsible" class="media-management-data">
    <h1>{{ $item['name'] }} (@lang('POSTCARD'))</h1>

    <div class="ui-field-contain">

        <fieldset data-role="fieldcontain">
            <div class="uk-margin-remove uk-margin-bottom" style="color:#919191;">
              @php
                  echo date( "l, j F Y" ,  strtotime($item['updated_at']) );
              @endphp
            </div>
            <div><h3 class="uk-text-success uk-margin-small">$ {{ $item->Price( Auth::id() ) }}</h3></div>
        </fieldset>

        @if ( $item->Media()->first()->file!="" )
        <fieldset data-role="fieldcontain" style="overflow:hidden;">
            <img class="media" src="{{ URL('/') }}/media/images/{{ $item->Media()->first()->file }}" alt="{{ $item['name'] }}" style="max-width: 720px;max-height: 266px;">
            <div class="container_backside" iid="{{$item['id']}}">
                <img src="{{ URL('/') }}/images/postcard02.PNG" width="100%" for="backside_text_box">
                <div class="user_content container_backside_text" data-role="none">{{ $item['content'] }}</div>
                <div class="user_address">
                  {{ $item->UserDefaultReturnAddress()->name }}<BR>
                  {{ $item->UserDefaultReturnAddress()->address1 }}<BR>
                  {{ $item->UserDefaultReturnAddress()->address2 }}<BR>
                  {{ $item->UserDefaultReturnAddress()->city }}<BR>
                  {{ $item->UserDefaultReturnAddress()->state }}&nbsp;{{ $item->UserDefaultReturnAddress()->zipcode }}<BR>
                  {{ $item->UserDefaultReturnAddress()->country }}<BR>
                </div>
            </div>
            <!-- for capture only -->
            <div class="new_image_container" tag="{{$item['id']}}"></div>
        </fieldset>
        @endif

        
        <button class="uk-button uk-button-primary" type="button" onclick="AddToCart('{{ $item['id'] }}')">@lang('Add to Cart')</button>
        

        @if ( $item->created_by==$item->Media()->first()->created_by )
        <button type="button"
           onclick="GenerateDownload('{{$item['id']}}')">
            @lang('Generate Download')
        </button>
        <a type="button" 
           class="ui-btn download_canvas_button" 
           onclick="DownloadCanvas(this,'{{$item['id']}}','back.png')"
           style="display:none;"
           tag="{{$item['id']}}">
            @lang('The Back')
        </a>
        <a type="button" 
           class="ui-btn download_front_button" 
           style="display:none;"
           tag="{{$item['id']}}"
           target="_blank" 
           href="{{ URL('/') }}/media/images/{{ $item->Media()->first()->file }}" 
           download="{{$item['id']}}_front">
            @lang('The Front')
       </a>
       @else
       <p class="download_front_button"
          >@lang('Postcard cannot be downloaded because you are not the owner of the image.')</p>
       @endif
    </div>
    


</div>
@endforeach


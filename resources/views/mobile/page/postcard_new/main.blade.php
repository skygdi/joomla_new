
<script type="text/javascript">
var msg_content_length_not_fit = "@lang('Content length should be in between 2~255 character')";
var msg_input_name = "@lang('Please Input Post Card Name')";
var msg_select_image = "@lang('Please Select An Image First')";
var msg_success = "@lang('Success')";
$(document).on("pagebeforeshow","#div_page_new_postcard",function(){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }
    
    var current_page = $("#div_page_new_postcard");
    current_page_id = current_page;
    ReRenderHeader(current_page);

    var url;
    if( current_item_id!=null && current_item_id!="" ) url = root_url+"/mobile/postcard/"+current_item_id+"/create";
    else url = root_url+"/mobile/postcard/create";
    //load data
    $(current_page).find("[data-role='main']").load(url, function ( response, status, xhr ) {
        $(this).trigger('create');
        RespondCheck(xhr.status,true);
        init_carousel_new_postcard();
    });
});



function new_post_card_select_image(id){
    $("#div_page_new_postcard").find("input[name='id']").val(id);
}


</script>

<div data-role="page" id="div_page_new_postcard">

    <div data-role="header" class="content_header">
        <div class="title">
            @lang('CREATE POSTCARD')
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
    </div>

    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>
    
    
</div>

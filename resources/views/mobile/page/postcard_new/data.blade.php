<div class="ui-grid-solo" style="margin-top:10px;height: 300px;">
    <div class="owl-carousel owl-theme">
        @if( isset($medias) )
        @foreach ($medias as $media)
            @php
                $media->ImageCacheGet();
            @endphp
            <div class="item" style="width:100%;position: relative;">
                <img onclick="new_post_card_select_image('{{ $media['id'] }}')" src="{{ $media->image_cache_url }}" alt="{{ $media['alias'] }}"/>
            </div>
        @endforeach
        @else
            <div class="item" style="width:100%;position: relative;">
                <img onclick="new_post_card_select_image('{{ $preselect_media['id'] }}')" src="{{ $preselect_media->image_cache_url }}" alt="{{ $preselect_media['alias'] }}"/>
            </div>
        @endif
    </div>
</div>


<input type="hidden" name="id" value="">
<div class="ui-grid-solo">
    <input type="text" name="name" placeholder="@lang('Name')" autocomplete="off">
</div>

<div class="ui-grid-solo">
    <div class="container_backside">
        <img src="{{ URL('/') }}/images/postcard02.PNG" width="100%" for="backside_text_box">
        <textarea name="content" class="user_content container_backside_text" data-role="none"></textarea>
    </div>
</div>


<div class="ui-grid-solo">
    <button class="uk-button uk-button-primary" type="button" onclick="new_post_card_submit()">@lang('Submit')</button>
</div>


<script type="text/javascript">
var msg_new_billing_address = "@lang('New Billing Address')";
var msg_new_shipping_address = "@lang('New Shipping Address')";
var msg_validation_name = "@lang('Please enter the recipient name')";
var msg_validation_email = "@lang('Please enter a valid email address')";
var msg_validation_state = "@lang('Please type or select a state')";
var msg_validation_address1 = "@lang('Please enter a valid address')";
var msg_validation_zipcode = "@lang('Please enter a valid zipcode, digits only')";
var msg_validation_country = "@lang('Please select a country')";

var flag_refresh_addresses_management = true;
$(document).on("pagebeforeshow","#div_page_new_address",function(event, data){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }

    var current_page = $("#div_page_new_address");
    current_page_id = $(current_page).attr("id");
    ReRenderHeader(current_page);

    if( new_address_type=="billing" ){
        $(current_page).find(".content_header").find(".title").html(msg_new_billing_address);
    }
    else{
        $(current_page).find(".content_header").find(".title").html(msg_new_shipping_address);
    }

    $("#div_page_new_address").find("form[name='address_new']").submit(function(event){
        //var form = this;
        //console.log("test1");
        var obj_form = $("#div_page_new_address");
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_name);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
        //...
    });

});



function NewAddressCheck(){
    var obj_form = $("#div_page_new_address");

    if( $(obj_form).find("input[name='name']").val()=="" ){
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_name);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
    }
    if( $(obj_form).find("input[name='email']").val()=="" ){
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_email);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
    }
    if( $(obj_form).find("input[name='address1']").val()=="" ){
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_address1);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
    }

    flag_for_ajaxComplete = "new address";
    flag_refresh_addresses_management = true;
}

</script>

<div data-role="page" id="div_page_new_address">

    <div data-role="header" class="content_header">
        <div class="title">
            <div></div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    
    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
        <form name="address_new" onsubmit="return NewAddressCheck()" method="post" action="{{URL('/')}}/mobile/new-address">

            <div class="ui-field-contain">
                <input type="hidden" name="from" value="mobile-new-media">
                <fieldset data-role="fieldcontain">
                    <input type="text" name="name" value="" placeholder="Enter Name">
                    <input type="text" name="phone" value="" placeholder="Enter Phone">
                    <input type="email" name="email" value="" placeholder="Enter Email">
                    <input type="text" name="address1" value="" placeholder="Enter Address1">
                    <input type="text" name="address2" value="" placeholder="Enter Address2">
                    <input type="text" name="city" value="" placeholder="Enter City">
                    <label for="text-basic">@lang('State'):</label>
                    <select name="state">
                        <option value="">Please select</option>
                        {% include 'partial/select_option_US_state.html' %}
                    </select>
                    <input type="text" name="zipcode" value="" placeholder="Enter Zip Code">
                    <label for="text-basic">@lang('Country'):</label>
                    <select name="country">
                        <option value="">Please select</option>
                        {% include 'partial/select_option_country.html' %}
                    </select>
                </fieldset>
                <fieldset data-role="fieldcontain">
                    <button type="submit" class="uk-button uk-button-primary uk-btn">@lang('Add')</button>
                </fieldset>
                
            </div>
        </form>
	    
    </div>

    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

</div>
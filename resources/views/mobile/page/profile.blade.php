
<script type="text/javascript">
var msg_submitting = "Submitting...";
</script>

<div data-role="page" id="div_page_profile">

    <div data-role="header" class="content_header">
        <div class="title">
            @lang('Profile')
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="javascript://void();" onclick="history.go(-1)">
                <i class="glyphicon glyphicon-chevron-left" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">

        <form name="profile" method="post" action="{{URL('/')}}/mobile/user/profile" style="margin-top: 5px;">
        
        <fieldset data-role="fieldcontain">
            <label for="name">@lang('Name'):</label>
            <input type="text" name="name" value="" data-clear-btn="true" required>
        </fieldset>
        <fieldset data-role="fieldcontain">
            <label for="password">@lang('Password'):</label>
            <input type="password" name="password" value="" data-clear-btn="true">
        </fieldset>
        <fieldset data-role="fieldcontain">
            <label for="password2">@lang('Confirm Password'):</label>
            <input type="password" name="password2" value="" data-clear-btn="true">
        </fieldset>
        <fieldset data-role="fieldcontain">
            <label for="email">@lang('Email'):</label>
            <input type="email" name="email" value="" data-clear-btn="true">
        </fieldset>
        <fieldset data-role="fieldcontain">
            <label for="paypal">@lang('Paypal account'):</label>
            <input type="email" name="paypal" value="" data-clear-btn="true">
        </fieldset>
        <fieldset data-role="fieldcontain">
            <label for="description">@lang('Description'):</label>
            <textarea name="description" data-clear-btn="true"></textarea>
        </fieldset>

        <fieldset data-role="fieldcontain" class="ui-field-contain">
            <label for="avatar_file">@lang('Avatar'):</label>
            <span class="ui-btn ui-icon-plus ui-btn-icon-left ui-corner-all fileinput-button"><span>@lang('CHOOSE IMAGE')</span>
            <input type="file" name="avatar_file" multipledata-role="none"/></span>
        </fieldset>

        <input type="submit" data-inline="true" value="@lang('SUBMIT')" value_original="@lang('SUBMIT')" data-role="none" class="ui-btn ui-input-btn ui-corner-all ui-shadow ui-btn-inline uk-btn" name="submit_button">
        
        </form>
    </div>

</div>

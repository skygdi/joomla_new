
<script type="text/javascript">

var msg_content_length_not_fit = "@lang('Content length should be in between 2~255 character')";
var msg_delete_confirm =  "@lang('Are you sure want to delete')";
var initialized_media_management = false;
$(document).on("pagebeforeshow","#div_page_media_management",function(){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }
    
    var current_page = $("#div_page_media_management");
    current_page_id = current_page;
    //page = 0;
    ReRenderHeader(current_page);

    if( initialized_media_management ) return;

    MediaManagerRefreshData(0);
    //return;
    //load data
});





</script>

<div data-role="page" id="div_page_media_management">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('MEDIA MANAGEMENT')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
    </div>
    
    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

    <div data-role="popup" class="ui-content div_tip_confirm" data-overlay-theme="a">
        <p></p>
        <a onclick="CommentRemove()" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('CANCEL')</a>
    </div>


    
</div>

<div class="ui-grid-solo" name="media_show_container" style="margin-top:10px;height: 300px;">
    <div class="owl-carousel owl-theme">
        @if($media["file"]!=null)
            <img class="media" src="{{URL('/')}}/media/images/{{ $media['file'] }}" alt="{{ $media['alias'] }}">
        @endif
    </div>
</div>




<form name="media_edit" method="post" action="{{URL('/')}}/mobile/media/{{$media['id']}}" enctype="multipart/form-data" data-ajax="false">
    <input type="hidden" name="id" value="{{$media['id']}}">
    <fieldset data-role="fieldcontain">
        <div class="ui-grid-solo">
            <input type="file" name="file_image" class="filestyle" data-buttonName="uk-btn uk-btn-file" data-buttonText="@lang('CHOOSE IMAGE')" accept="image/*">
        </div>
    </fieldset>

    <fieldset data-role="fieldcontain">
        <input type="text" name="name" data-clear-btn="true" autocomplete="off" placeholder="@lang('Name')" data-wrapper-class="controlgroup-textinput ui-btn" value="{{$media['name']}}">
    </fieldset>

    <fieldset data-role="fieldcontain">
        <input type="text" name="tag" data-clear-btn="true" data-wrapper-class="controlgroup-textinput ui-btn" placeholder="Enter tag" autocomplete="off">
        <button type="button" onclick="AddTag(this)">ADD Tag</button>
        <ul style="margin:0px;padding:0px;float:left;" class="tags">
            @foreach ($media->KeyWord()->get() as $keyword)
            <li class="tag label label-success tagB">
                <input type="hidden" name="tags[]" value="{{$keyword['name']}}">{{$keyword['name']}}
                <a class="close" href="#" onclick="RemoveTag(this)">×</a>
            </li>
            @endforeach
        </ul>
    </fieldset>

    <fieldset data-role="fieldcontain">
        <label for="switch">Public:</label>
        <input type="checkbox" data-role="flipswitch" name="is_public" data-on-text="Yes" data-off-text="No"
            @if( $media->searchable=="1" )
                checked
            @endif
        >
    </fieldset>
    

    <div class="ui-grid-solo">
        <button type="button" class="uk-button uk-button-primary uk-btn" onclick="EditMedia()">@lang('Submit')</button>
    </div>

    <div class="ui-grid-solo">
        <a href="#" class="ui-btn" data-rel="back">@lang('Cancel')</a>
    </div>

</form>


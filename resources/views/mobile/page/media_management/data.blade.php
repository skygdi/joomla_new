@foreach( $medias as $media  )
<div data-role="collapsible" class="media-management-data">
    <h1>{{$media["name"]}} (@lang('Media'))</h1>

    <div class="ui-field-contain">
        <fieldset data-role="fieldcontain">
        @if($media["file"]!=null)
            <img class="media" src="{{ URL('/') }}/media/images/{{ $media['file'] }}" alt="{{ $media['alias'] }}">
        @endif
        </fieldset>

        <fieldset data-role="fieldcontain">
            <ul data-role="listview" >
                <li>
                    <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                    <span class="ui-li-count plus
                        @if( $media->Liked() )
                         had_plus
                        @endif
                        " 
                        onclick="media_management_like('{{$media["id"]}}',this)">+1</span>
                    <span class="ui-li-count">{{ $media->Likes() }}</span>
                </li>
                <li>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    <span class="ui-li-count">{{ $media->shows }}</span></a>
                </li>
                <li>
                    
                    <span>@lang('Total Sold')</span>
                    <span class="ui-li-count">{{ $media->Sold() }}</span></a>
                </li>
                <li>
                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                    <span class="ui-li-count">{{ $media->Comment()->count() }}</span></a>
                </li>
               
            </ul>
        </fieldset>

        <fieldset>
            <div class="tags">
                @foreach ($media->KeyWord()->get() as $tag)
                <a href="#" onclick="SearchByTag('{{$tag['name']}}')">
                    <div class="tag label label-success">{{$tag['name']}}</div>
                </a>
                @endforeach
            </div>
        </fieldset>

        @if(  $media['user_id']==Auth::user()->id  )
        <fieldset>
            <a onclick="MediaEdit('{{$media["id"]}}')" class="ui-btn ui-icon-info ui-btn-icon-left">@lang('Edit Media')</a>
        </fieldset>
        @endif

        <fieldset data-role="fieldcontain">
            <div data-role="collapsible" class="media-management-data">
                <h1>@lang('Comments')</h1>
                <ul data-role="listview">
                    <li>
                        <input id="someSwitchOptionWarning{{$media["id"]}}" type="checkbox"
                            @if(  $media->disable_comment==1  )
                            checked="true"
                            @endif
                            onchange="comment_permission_set('{{$media["id"]}}',this)" />
                        <label for="someSwitchOptionWarning{{$media["id"]}}">@lang('Disable comment'):</label>
                    </li>
                    
                    <li>
                        <div class="form-group">
                            <label>@lang('Comment'):</label>
                            <textarea class="form-control comment_input" rows="5" style="margin-bottom: 5px;"></textarea>
                            <button class="uk-button uk-button-primary" onclick="media_manager_comment_submit('{{$media["id"]}}',this)">@lang('Submit')</button>
                        </div>
                    </li>
                    @foreach( $media->Comment()->OrderBy("created_at","DESC")->get() as $comment )
                    @if(  $comment['user_id']==Auth::user()->id  )
                    <li><a onclick="CommentRemoveConfirming('{{$comment["id"]}}',this)" class="ui-btn ui-icon-delete ui-btn-icon-left">Delete</a></li>
                    @endif
                    <li>
                        <div>
                            <div class="user">{{$comment->User()->first()->name}}</div>
                            <div class="issue_datetime">
                                @php
                                    echo date("F j, Y H:i:s",strtotime($comment["created_at"]));
                                @endphp
                            </div>
                        </div>
                        <div class="content">{{$comment['content']}}</div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </fieldset>
    </div>
</div>
@endforeach




<div class="pagination-bottom">

<a class="ui-btn ui-icon-info ui-btn-icon-left">@lang('Current'): ( {{$current_page}} / {{$max_page}} )</a>
@if( $previous_page!=null )
<button class="ui-btn ui-icon-back ui-btn-icon-left" onclick="MediaManagerRefreshData(1)">@lang('First')</button>
@endif

@if( $previous_page!=null )
<a class="ui-btn ui-icon-arrow-l ui-btn-icon-left" onclick="MediaManagerRefreshData({{$previous_page}})">@lang('Previous')</a>
@endif

@if( $next_page!=null )
<a class="ui-btn ui-icon-arrow-r ui-btn-icon-left" onclick="MediaManagerRefreshData({{$next_page}})">@lang('Next')</a>
@endif

</div>

<div style="margin-top:30px;">
    <ul data-role="listview" >
        <li>
            <span class="">@lang('Sold on this page'):</span>
            <span class="ui-li-count"></span>
        </li>
        <li>
            <span class="">@lang('Total Sold'):</span>
            <span class="ui-li-count">$ {{$total_sold}}</span>
        </li>
        <li>
            <span class="">@lang('Total Sold price'):</span>
            <span class="ui-li-count">$ {{$total_balance}}</span>
        </li>
        <li>
            <span class="">@lang('Unpaid balance'): </span>
            <span class="ui-li-count">$ {{$unpaied_balance}}</span>
        </li>
    </ul>
</div>
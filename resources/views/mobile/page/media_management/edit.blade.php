
<script type="text/javascript">

var msg_content_length_not_fit = "@lang('Content length should be in between 2~255 character')";
var msg_input_name = "@lang('Please Input Post Card Name')";
var msg_select_image = "@lang('Please Select An Image First')";
var msg_success = "@lang('Success')";

$(document).on("pagebeforeshow","#div_page_edit_media",function(){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }
    
    var current_page = $("#div_page_edit_media");
    ReRenderHeader(current_page);

    if( current_item_id==null ) return;

    //load data
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/media/"+current_item_id+"/edit", function () {
        $(this).trigger('create');
        init_carousel_edit_postcard();

        $("#div_page_edit_media").find("input[name='file_image']").change(function() {
            //console.log("changed");
            //$("#div_page_edit_media").find("div[class~='owl-carousel']").hide();
            $("#div_page_edit_media").find("[name='media_show_container']").hide();
        });
    });
});

function EditMediaCheck(){
    
    if( $("#div_page_edit_media").find("input[name='name']").val()=="" ){
        $("#div_page_edit_media").find(".div_tip_alert").find("p").html(msg_input_name);
        $("#div_page_edit_media").find(".div_tip_alert").popup( "open" );
        return false;
    }

    initialized_media_management = false;
    return true;
}

function init_carousel_edit_postcard(){
    $("#div_page_edit_media").find('.owl-carousel').owlCarousel({
        autoHeight:true,
        loop:true,
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:5
            }
        }
    });
}



</script>

<div data-role="page" id="div_page_edit_media">

    <div data-role="header" class="content_header">
        <div class="title">
            @lang('Edit Media')
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
    </div>
    
    
</div>



<style type="text/css">

</style>

<ul data-role="listview">
@foreach ($result as $item)
    <li class="shopping-cart-data" item_id="{{ $item["id"] }}">
        <div>
            <a class="ui-btn ui-icon-delete ui-btn-icon-right" onclick="ShoppingCartRemoveConfirmation('{{ $item["id"] }}')">&nbsp;</a>
        </div>
        <div>
            <a class="ui-btn">
                <i class="fa fa-picture-o" aria-hidden="true" style=""></i>
                <span style="">{{$item->PostCard()->first()->name}}</span>
            </a>
        
        </div>
        <div>
            <input type="text" name="quantity" value="{{$item->quantity}}" item_id='{{ $item["id"] }}' style="">
        </div>
        <div>
            <a class="ui-btn">
                <i class="fa fa-usd" aria-hidden="true" style=""></i>
                <span style="">{{$item->PostCard()->first()->Price()}}</span>
            </a>
        </div>
        <div>
            <a class="ui-btn">
                <i class="fa fa-shopping-bag" aria-hidden="true" style=""></i>
                <i class="fa fa-usd" aria-hidden="true" style=""></i>
                <span style="">{{$item->Total()}}</span>
            </a>
        </div>
    </li>
@endforeach
</ul>

<ul data-role="listview" data-inset="true" style="margin-top:40px;">
    <li style="background-color: #eee;">
        <div>@lang('Billing Address')</div>
    </li>
    <li>
        <select name="billing_address_current" id="billing_address_current">
            @foreach( $BA as $address )
                @if( $address->is_default==1 )
                <option value="{{$address->id}}" selected>
                @else
                <option value="{{$address->id}}">
                @endif
                {{$address->name}}
                @if( !empty($address->phone) )
                {{$address->phone}},
                @endif
                </option>
            @endforeach
        </select>
    </li>
    <li style="background-color: #eee;">
        <div>@lang('Shipping Address')</div>
    </li>
    <li>
        <select name="shipping_address_current" id="shipping_address_current">
            @foreach( $SA as $address )
                @if( $address->is_default==1 )
                <option value="{{$address->id}}" selected>
                @else
                <option value="{{$address->id}}">
                @endif
                {{$address->name}}
                @if( !empty($address->phone) )
                {{$address->phone}},
                @endif
                </option>
            @endforeach
        </select>
    </li>
</ul>


<a class="ui-btn" onclick="ShoppingCartQuantityChange()">@lang('Update')</a>

<a class="ui-btn" onclick="ShoppingCartCheckout(this)">@lang('Checkout')</a>

<div style="text-align: center;">
    <div id="paypal-button" style="display:none;"></div>
</div>

@include('partial.paypal')
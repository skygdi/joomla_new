
<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<script type="text/javascript">


</script>

<div data-role="page" id="div_page_shopping_cart">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('Your Cart')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
    </div>


    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('Please add item first')</a>
    </div>

    <div data-role="popup" class="ui-content div_tip_confirm" data-overlay-theme="a">
        <p>@lang('Are you sure?')</p>
        <a onclick="ShoppingCartRemove()" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('CANCEL')</a>
    </div>

    <div data-role="popup" class="ui-content div_tip_ok" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>
    
    
</div>

<ul data-role="listview">
@foreach( $result as $order )
    <li class="shopping-cart-data" item_id="{{$order["id"]}}">
        <div>
            <a class="ui-btn ui-icon-search ui-btn-icon-right" onclick="SwtichToOrderDetail('{{$order["id"]}}')">0011{{$order["id"]}}</a>
        </div>
        
        <div>
            <a class="ui-btn">
                <span style="float:left">${{$order->Total()}}</span>
                <span style="float:right">{{$order["pay_type"]}}</span>
            </a>
        </div>
        <div>
            <a class="ui-btn">
                <span style="float:left">
                    @if( $order["cancel"]==0 && $order["paid"]==0 && $order["delivered"]==0 && $order["payment_validating"]==0 )
                    <span class="uk-badge uk-badge-default" title="This is the status of an order that has yet to be payed, and has just been saved from your cart" data-uk-tooltip="">
                        @lang('Pending')
                    </span>
                    @elseif( $order["cancel"]==0 && $order["paid"]==1 && $order["delivered"]==0 )
                    <span class="uk-badge uk-badge-success" title="" data-uk-tooltip="" data-cached-title="Status for order, when payment is received">
                        @lang('Payment received')
                    </span>
                    @elseif( $order["cancel"]==0 && $order["paid"]==1 && $order["delivered"]==1 )
                    <span class="uk-badge uk-badge-default" title="This is the status of an order that has yet to be payed, and has just been saved from your cart" data-uk-tooltip="">
                        @lang('SHIPPED')
                    </span>
                    @elseif( $order["cancel"]==1 )
                    <span class="uk-badge uk-badge-danger" title="" data-uk-tooltip="" data-cached-title="Status for order that was cancelled by user or administrator">
                        @lang('Canceled')
                    </span>
                    @elseif( $order["cancel"]==0 && $order["paid"]==0 && $order["payment_validating"]==1 )
                    <span class="uk-badge uk-badge-default" title="" data-uk-tooltip="" data-cached-title="Order being validating">
                        @lang('Validating Payment')
                    </span>
                    @endif
                </span>
                <span style="float:right">
                    {{ Carbon\Carbon::parse($order["created_on"])->format('j F Y') }}
                </span>
            </a>
        </div>
    </li>
@endforeach
</ul>


<div class="pagination-bottom">
<a class="ui-btn ui-icon-info ui-btn-icon-left">@lang('Current'): ( {{$current_page}} / {{$max_page}} )</a>
@if( $previous_page!=null )
<button class="ui-btn ui-icon-back ui-btn-icon-left" onclick="OrdersRefreshData(1)">@lang('First')</button>
@endif

@if( $previous_page!=null )
<a class="ui-btn ui-icon-arrow-l ui-btn-icon-left" onclick="OrdersRefreshData({{$previous_page}})">@lang('Previous')</a>
@endif

@if( $next_page!=null )
<a class="ui-btn ui-icon-arrow-r ui-btn-icon-left" onclick="OrdersRefreshData({{$next_page}})">@lang('Next')</a>
@endif
</div>

<script type="text/javascript">

$(document).on("pagebeforeshow","#div_page_orders",function(){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }
    
    var current_page = $("#div_page_orders");
    current_page_id = current_page;
    ReRenderHeader(current_page);

    //load data
    OrdersRefreshData(0);
});

function OrdersRefreshData(page){
    var current_page = $("#div_page_orders");
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/order?page="+page, function ( response, status, xhr ) {
        current_page_id = $(this).parent();
        RespondCheck(xhr.status,true);
        $(this).trigger('create');
    });
}

function SwtichToOrderDetail(id){
    //console.log(current_order_id);
    current_order_id = id;
    $.mobile.changePage('#div_page_orders_detail');
}
</script>

<div data-role="page" id="div_page_orders">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('Orders')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
    </div>
    
    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

    <div data-role="popup" class="ui-content div_tip_confirm" data-overlay-theme="a">
        <p></p>
        <a onclick="CommentRemove()" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('CANCEL')</a>
    </div>


    
</div>

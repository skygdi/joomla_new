
<script type="text/javascript">

var msg_username_or_password_error = "@lang('Username and or password error')";
$(document).on("pagebeforeshow","#div_page_login",function(){
    
});
</script>

<div data-role="page" id="div_page_login">
    
    <div data-role="header" class="content_header">
        <div class="title">
            @lang('LOG IN')
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_home">
                <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        <form method="post" action="{{ route('login') }}" style="margin-top: 5px;">
            {{ csrf_field() }}
            <label for="username">@lang('Username'):</label>
            <input type="text" name="username" id="username" data-clear-btn="true">
            <label for="password">@lang('Password'):</label>
            <input type="password" name="password" id="password" data-clear-btn="true">
            <button type="button" onclick="login(this)" class="ui-btn">@lang('Sign in')</button>
            <!--<button type="submit" class="ui-btn">@lang('Sign in')</button>-->
            

            <a type="button" class="ui-btn" href="#div_page_forgot_your_password" style="margin-top:40px;">@lang('Forgot your password?')</a>
            <a type="button" class="ui-btn" href="#div_page_forgot_your_username">@lang('Forgot your username?')</a>
        </form>
    </div>

</div>
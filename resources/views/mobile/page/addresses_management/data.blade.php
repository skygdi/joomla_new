<div data-role="collapsible" class="billing">
    <h1>@lang('Billing Address')</h1>


    <a onclick="GoPageAddAddress('billing')" class="ui-btn ui-icon-plus ui-btn-icon-left">
    @lang('New Billing Address')
    </a>

    @foreach ($BA as $address)
    <div data-role="collapsible">
        <h1>{{ $address->name }}</h1>
        <div class="ui-field-contain">
            <fieldset data-role="fieldcontain">
                <span class="">
                    {{ $address->name }}<BR>
                    @if( $address->phone!="" )
                    {{ $address->phone }},
                    @endif
                    {{ $address->email }}
                </span>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <span style="line-height: 49px;font-size:13px;">@lang('Set this Address as default'):</span>
                <span style="float:right;">
                    <input type="checkbox" data-role="flipswitch" name="is_default" data-on-text="Yes" data-off-text="No" 
                    onchange='ChangeDefaultAddress(this, "billing","{{ $address->id }}");'
                    @if( $address->is_default==1 )
                    checked
                    @endif
                    >
                </span>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <span style="line-height: 49px;font-size:13px;">@lang('Display as return address'):</span>
                <span style="float:right;">
                    <input type="checkbox" data-role="flipswitch" name="display_as_return_address" data-on-text="Yes" data-off-text="No" 
                    onchange='ChangeDisplayAsReturnAddress(this,"{{ $address->id }}");'
                    @if( $address->display_as_return==1 )
                    checked
                    @endif
                    >
                </span>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <a onclick="GoPageEditAddress('billing','{{ $address->id }}')" class="ui-btn ui-icon-edit ui-btn-icon-left">@lang('Edit')</a>
                <a onclick="RemoveAddress('{{ $address->id }}',this)" class="ui-btn ui-icon-delete ui-btn-icon-left">@lang('Delete')</a>
            </fieldset>
        </div>
    </div>
    @endforeach
</div>


<div data-role="collapsible" class="shipping">
    <h1>@lang('Shipping Address')</h1>

    <a onclick="GoPageAddAddress('shipping')" class="ui-btn ui-icon-plus ui-btn-icon-left">
    @lang('New Shipping Address')
    </a>

    @foreach ($SA as $address)
    <div data-role="collapsible">
        <h1>{{ $address->name }}</h1>
        <div class="ui-field-contain">
            <fieldset data-role="fieldcontain">
                <span class="">
                    {{ $address->name }}<BR>
                    @if( $address->phone!="" )
                    {{ $address->phone }},
                    @endif
                    {{ $address->email }}
                </span>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <span style="line-height: 49px;font-size:13px;">@lang('Set this Address as default'):</span>
                <span style="float:right;">
                    <input type="checkbox" data-role="flipswitch" name="is_default" data-on-text="Yes" data-off-text="No" 
                    onchange='ChangeDefaultAddress(this,"shipping","{{ $address->id }}");'
                    @if( $address->is_default==1 )
                    checked
                    @endif
                    >
                </span>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <a onclick="GoPageEditAddress('shipping','{{ $address->id }}')" class="ui-btn ui-icon-edit ui-btn-icon-left">@lang('Edit')</a>
                <a onclick="RemoveAddress('{{ $address->id }}',this)" class="ui-btn ui-icon-delete ui-btn-icon-left">@lang('Delete')</a>
            </fieldset>
        </div>
    </div>
    @endforeach
</div>
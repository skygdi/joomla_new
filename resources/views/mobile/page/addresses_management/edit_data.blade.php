<form name="address_edit" id="address_edit" method="post" onsubmit="return EditAddressCheck()" action="{{URL('/address')}}/{{$address["id"]}}" >
    <input type="hidden" name="id" value="{{$address["id"]}}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    
    <div class="ui-field-contain">
        
        <fieldset data-role="fieldcontain">
            <input type="text" name="name" value="{{$address["name"]}}" placeholder="Enter Name">
            <input type="text" name="phone" value="{{$address["phone"]}}" placeholder="Enter Phone">
            <input type="email" name="email" value="{{$address["email"]}}" placeholder="Enter Email">
            <input type="text" name="address1" value="{{$address["address1"]}}" placeholder="Enter Address1">
            <input type="text" name="address2" value="{{$address["address2"]}}" placeholder="Enter Address2">
            <input type="text" name="city" value="{{$address["city"]}}" placeholder="Enter City">
            <label for="text-basic">@lang('State'):</label>
            <input type="hidden" name="original_state" value="{{$address["state"]}}">
            <input type="hidden" name="original_country" value="{{$address["country"]}}">
            <select name="state">
                <option value="">Please select</option>
                @include('partial.select_option_US_state')
            </select>
            <input type="text" name="zipcode" value="{{$address["zipcode"]}}" placeholder="Enter Zip Code">
            <label for="text-basic">@lang('Country'):</label>
            <select name="country">
                <option value="">Please select</option>
                @include('partial.select_option_country')
            </select>
        </fieldset>
        <fieldset data-role="fieldcontain">
            <button type="button" onclick="SubmitAddressEdit()" class="uk-button uk-button-primary uk-btn">@lang('Update')</button>
        </fieldset>
    </div>
</form>
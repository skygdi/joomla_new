
<script type="text/javascript">

var msg_edit_billing_address = "@lang('Edit Billing Address')";
var msg_edit_shipping_address = "@lang('Edit Shipping Address')";
$(document).on("pagebeforeshow","#div_page_edit_address",function(event, data){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }

    var current_page = $("#div_page_edit_address");
    current_page_id = $(current_page).attr("id");
    ReRenderHeader(current_page);

    if( new_address_type=="billing" ){
        $(current_page).find(".content_header").find(".title").html(msg_edit_billing_address);
    }
    else{
        $(current_page).find(".content_header").find(".title").html(msg_edit_shipping_address);
    }

    RefreshAddress();
});


function RefreshAddress(){
    if( current_address_id==null ) return;
    var current_page = $("#div_page_edit_address");
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/address/"+current_address_id, function ( response, status, xhr ) {
        RespondCheck(xhr.status,true);
        $(this).trigger('create');

        $("select[name='state']").val($("input[name='original_state']").val());
        $("select[name='state']").selectmenu();
        $("select[name='state']").selectmenu("refresh",true);

        $("select[name='country']").val($("input[name='original_country']").val());
        $("select[name='country']").selectmenu();
        $("select[name='country']").selectmenu("refresh",true);

    });
    ReRenderHeader(current_page);
}


function EditAddressCheck(){
    var obj_form = $("#div_page_edit_address");
    if( $(obj_form).find("input[name='name']").val()=="" ){
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_name);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
    }
    if( $(obj_form).find("input[name='email']").val()=="" ){
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_email);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
    }
    if( $(obj_form).find("input[name='address1']").val()=="" ){
        $(obj_form).find(".div_tip_alert").find("p").html(msg_validation_address1);
        $(obj_form).find(".div_tip_alert").popup( "open" );
        return false;
    }

    flag_for_ajaxComplete = "edit address";
    //flag_refresh_addresses_management = true;
    //flag_ajaxComplete_edit_address = true;
    
    return true;
}

function SubmitAddressEdit(){
    var data = $("#address_edit").serialize(); // returns all the data in your form
    var url = $("#address_edit").attr("action");
    //console.log(data);
    //
    $.ajax({
        url: url,
        type: 'PUT',
        data: data,
        datatype: 'json',
        success: function (data) {
            //console.log(data);
            if( data.state=="success" ){
                $.mobile.changePage('#div_page_addresses_management');
                //RefreshAddress();
                RefreshAddresses();
                //window.location = system_url+"/mobile#div_page_addresses_management";
            }
            else{
                $("#div_page_edit_address").find(".div_tip_alert").find("p").html(data.text);
                $("#div_page_edit_address").find(".div_tip_alert").popup( "open" );
            }
            
            //someSuccessFunction(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}



</script>

<div data-role="page" id="div_page_edit_address">

    <div data-role="header" class="content_header">
        <div class="title">
            <div></div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    
    <div data-role="main" class="ui-content" style="padding-top:5px;">
    </div>

    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

</div>
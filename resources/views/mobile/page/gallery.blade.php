
<script type="text/javascript">
$( document ).delegate("#div_page_gallery", "pageinit", function() {
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }

    var current_page = $("#div_page_gallery");
    ReRenderHeader(current_page);
    
    init_carousel();


});
</script>

<div data-role="page" id="div_page_gallery">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('Gallery')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        <div class="owl-carousel owl-theme">
            @foreach ($vars["result"]["click"] as $r)
            <div class="item" style="width:100%;height:180px;position: relative;">
                <a data-transition="flip" href="#item_dialog" onclick="switching_data('{{ $r["alias"] }}','{{ $r["id"] }}','{{ $r["user_id"] }}','{{ $r["name"] }}')">
                    <img src="{{ $r->image_cache_url }}" alt="{{ $r["alias"] }}" width="180" height="180"/>
                </a>
            </div>
            @endforeach
        </div>
        
    </div>
    
    
</div>

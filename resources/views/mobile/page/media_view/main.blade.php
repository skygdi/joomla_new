
<script type="text/javascript">
var msg_content_length_not_fit = "@lang('Content length should be in between 2~255 character')";
var msg_delete_confirm =  "@lang('Are you sure want to delete')";
$(document).on("pagebeforeshow","#div_page_media_view",function(){
    var current_page = $("#div_page_media_view");
    current_page_id = current_page;
    ReRenderHeader(current_page);
    MediaViewRefreshData()
});

function MediaViewRefreshData(){
    var current_page = $("#div_page_media_view");
    //load data
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/media/"+current_item_id, function () {
        $(this).trigger('create');
    });
}

</script>

<div data-role="page" id="div_page_media_view">

    <div data-role="header" class="content_header">
        <div class="title">
            @lang('Media')
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
    </div>


    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

    <div data-role="popup" class="ui-content div_tip_confirm" data-overlay-theme="a">
        <p></p>
        <a onclick="CommentRemove()" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('CANCEL')</a>
    </div>
    
    
</div>

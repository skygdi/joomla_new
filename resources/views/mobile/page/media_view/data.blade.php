<div class="ui-grid-solo" style="margin-top:10px;">
    @if($media["file"]!=null)
        <img class="media" src="{{ URL('/') }}/media/images/{{ $media['file'] }}" alt="{{ $media['alias'] }}">
    @endif
</div>

<div class="ui-field-contain">
    <fieldset data-role="fieldcontain">

        <ul data-role="listview" >
            <li>
                <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                <span class="ui-li-count plus
                    @if( $media->Liked() )
                     had_plus
                    @endif
                    "

                    @if( Auth::check() )
                        onclick="media_management_like('{{ $media["id"] }}',this)"
                    @endif
                    >+1</span>
                <span class="ui-li-count">{{ $media->Likes() }}</span>
            </li>
            <li>
                <i class="fa fa-eye" aria-hidden="true"></i>
                <span class="ui-li-count">{{ $media->shows }}</span></a>
            </li>
            <li>
                <i class="fa fa-usd" aria-hidden="true"></i>
                <span class="ui-li-count">{{ $media->Sold() }}</span></a>
            </li>
            <li>
                <i class="fa fa-commenting-o" aria-hidden="true"></i>
                <span class="ui-li-count">{{ $media->Comment()->count() }}</span></a>
            </li>
           
        </ul>
    </fieldset>

    <fieldset style="margin-top: 20px;">
        <div class="tags">
            @foreach ($media->KeyWord()->get() as $tag)
            <a href="{{ URL('/') }}/search/{{$tag['name']}}">
                <div class="tag label label-success">{{$tag['name']}}</div>
            </a>
            @endforeach
        </div>
    </fieldset>

    <fieldset data-role="fieldcontain">
        <div data-role="collapsible" class="media-management-data">
            <h1>@lang('Comments')</h1>
            <ul data-role="listview">
                <li>
                    <input id="someSwitchOptionWarning{{$media["id"]}}" type="checkbox"
                        @if(  $media->disable_comment==1  )
                        checked="true"
                        @endif
                        onchange="comment_permission_set('{{$media["id"]}}',this)" />
                    <label for="someSwitchOptionWarning{{$media["id"]}}">@lang('Disable comment'):</label>
                </li>
                
                <li>
                    <div class="form-group">
                        <label>@lang('Comment'):</label>
                        <textarea class="form-control comment_input" rows="5" style="margin-bottom: 5px;"></textarea>
                        <button class="uk-button uk-button-primary" onclick="media_view_comment_submit('{{$media["id"]}}',this)">@lang('Submit')</button>
                    </div>
                </li>
                @foreach( $media->Comment()->OrderBy("created_at","DESC")->get() as $comment )
                @if(  $comment['user_id']==Auth::user()->id  )
                <li><a onclick="CommentRemoveConfirming('{{$comment["id"]}}',this)" class="ui-btn ui-icon-delete ui-btn-icon-left">Delete</a></li>
                @endif
                <li>
                    <div>
                        <div class="user">{{$comment->User()->first()->name}}</div>
                        <div class="issue_datetime">
                            @php
                                echo date("F j, Y H:i:s",strtotime($comment["created_at"]));
                            @endphp
                        </div>
                    </div>
                    <div class="content">{{$comment['content']}}</div>
                </li>
                @endforeach
            </ul>
        </div>
    </fieldset>

</div>





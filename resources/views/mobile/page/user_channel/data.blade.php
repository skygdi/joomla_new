<div class="ui-grid-solo" style="margin-top:10px;height: 300px;">
    <div class="owl-carousel owl-theme">
        @foreach( $medias as $media )
        	@if($media["file"]!=null)
	            <img onclick="user_channel_select_image('{{$media['user_id']}}')" src="{{ URL('/') }}/media/images/{{$media['file']}}" alt="{{$media['alias']}}">
	        @endif
        @endforeach
    </div>
</div>

<input type="hidden" name="name" = value="{{$user['name']}}">


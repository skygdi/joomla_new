
<script type="text/javascript">
$(document).on("pagebeforeshow","#div_page_user_channel",function(){
    var current_page = $("#div_page_user_channel");
    current_page_id = current_page;
    ReRenderHeader(current_page);

    //load data
    $(current_page).find("[data-role='main']").load(root_url+"/mobile/user/"+current_user+"/channel", function ( response, status, xhr ) {
        $(this).trigger('create');
        init_carousel_user_channel();
        //console.log(xhr.status);
    });
});

function user_channel_select_image(id){
    current_item_id = id;
    $.mobile.navigate( "#div_page_new_postcard" );
}

function init_carousel_user_channel(){
    $("#div_page_user_channel").find("div[class='name']:lt(1)").html($("#div_page_user_channel").find("input[name='name']").val());

    $("#div_page_user_channel").find('.owl-carousel').owlCarousel({
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        margin:10,
        responsive:{
            320:{
                items:1
            },
            412:{
                items:2
            },
            667:{
                items:3
            },
            1024:{
                items:4
            },
            1366:{
                items:5
            }
        }
    });
}
</script>

<div data-role="page" id="div_page_user_channel">

    <div data-role="header" class="content_header">
        <div class="title" style="line-height: 16px;">
            <div>@lang('user channel')</div>
            <div class="name"></div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div data-role="main" class="ui-content" style="padding-top:5px;">
        
    </div>
    
    
</div>

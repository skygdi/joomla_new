
<script type="text/javascript">
$(document).on("pagebeforeshow","#div_page_new_media",function(event, data){
    if( user==null ){
        $.mobile.changePage('#div_page_login');
        return;
    }

    var current_page = $("#div_page_new_media");
    ReRenderHeader(current_page);
});
</script>

<div data-role="page" id="div_page_new_media">

    <div data-role="header" class="content_header">
        <div class="title">
            <div>@lang('Upload Media')</div>
        </div>
        <div class="menu">
            <a href="#sideMenu" class="ui-btn ui-btn-inline">
                <button class="c-hamburger c-hamburger--htx" data-role="none">
                    <span>toggle menu</span>
                </button>
            </a>
        </div>
        <div class="icon">
            <a href="#div_page_profile" style="display:none" tag="avatar">
                <img src="">
            </a>
            <a href="#div_page_login" tag="login">
                <i class="fa fa-user-o" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    
    <div data-role="main" class="ui-content" style="padding-top:5px;">

        <form name="media_new" method="post" action="{{URL('/')}}/mobile/media" enctype="multipart/form-data" data-ajax="false">
            
            <input type="hidden" name="from" value="mobile-new-media">
            <fieldset data-role="fieldcontain">
                <input type="text" name="name" data-clear-btn="true" autocomplete="off" placeholder="@lang('Name')" data-wrapper-class="controlgroup-textinput ui-btn">
            </fieldset>

            <fieldset data-role="fieldcontain">
                <input type="text" name="tag" data-clear-btn="true" data-wrapper-class="controlgroup-textinput ui-btn" placeholder="Enter Keyword" autocomplete="off">
                <button type="button" class="uk-button uk-btn" onclick="AddTag(this)">ADD Keyword</button>
                <ul style="margin:0px;padding:0px;float:left;" class="tags"></ul>
            </fieldset>

            <fieldset data-role="fieldcontain">
                <div class="ui-grid-solo">
                    <input type="file" name="file_image" id="file_image" class="filestyle" data-buttonName="uk-btn uk-btn-file" data-buttonText="@lang('CHOOSE IMAGE')" accept="image/*" required>
                </div>
            </fieldset>

            <fieldset data-role="fieldcontain">
            
            <label for="switch">Public:</label>
            <input type="checkbox" data-role="flipswitch" name="searchable" data-on-text="Yes" data-off-text="No" checked>
            

            </fieldset>



            <div class="ui-grid-solo">
                <button type="button" onclick="NewMedia()" class="uk-button uk-button-primary uk-btn">@lang('Submit')</button>
            </div>
        </form>
	    
    </div>

    <div data-role="popup" class="ui-content div_tip_alert" data-overlay-theme="a">
        <p></p>
        <a data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-a">@lang('OK')</a>
    </div>

</div>
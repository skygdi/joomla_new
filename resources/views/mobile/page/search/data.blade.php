@foreach( $result as $media )
<div data-role="collapsible" class="media-management-data">

    <h1>{{$media['name']}} (@lang('Media'))</h1>

    <div class="ui-field-contain">
        <fieldset data-role="fieldcontain">
            @if($media["file"]!=null)
            <img src="{{ URL('/') }}/media/images/{{ $media['file'] }}" alt="{{$media["alias"]}}">
            @endif
        </fieldset>

        <fieldset data-role="fieldcontain">
            <ul data-role="listview" >
                <li>
                    <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                    <span class="ui-li-count plus
                        @if($media->Liked())
                         had_plus
                        @endif
                        " 
                        onclick="media_management_like('{{$media["id"]}}',this)">+1</span>
                    <span class="ui-li-count">{{$media->Likes()}}</span>
                </li>
                <li>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    <span class="ui-li-count">{{$media->shows}}</span></a>
                </li>
                <li>
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <span class="ui-li-count">{{$media->Sold()}}</span></a>
                </li>
                <li>
                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                    <span class="ui-li-count">{{$media->Comment()->count()}}</span></a>
                </li>

               
            </ul>
        </fieldset>

        <fieldset>
            <div class="tags">
                @foreach ($media->KeyWord()->get() as $word)
                <a href="#" onclick="SearchByTag('{{$word['name']}}')">
                    <div class="tag label label-success">{{$word['name']}}</div>
                </a>
                @endforeach
            </div>
        </fieldset>

        <fieldset data-role="fieldcontain">
            <a class="ui-btn ui-icon-eye ui-btn-icon-left" onclick="SearchMediaView('{{$media["id"]}}')">@lang('View')</a>
            <a class="ui-btn ui-icon-plus ui-btn-icon-left" onclick="SearchNewPostcard('{{$media["id"]}}')">@lang('Postcard')</a>
            <a class="ui-btn ui-icon-user ui-btn-icon-left" onclick="SearchUserChannel('{{$media["user_id"]}}')">{{$media->User()->first()["name"]}}</a>
        </fieldset>
    </div>
</div>
@endforeach

@if( count($result)!=0 )
<div class="ui-btn ui-icon-info ui-btn-icon-left">@lang('Current'): ( {{$current_page}} / {{$max_page}} )</a>
    @if( $previous_page!=null )
    <button class="ui-btn ui-icon-back ui-btn-icon-left" onclick="SearchRefreshData(1)">@lang('First')</button>
    @endif

    @if( $previous_page!=null )
    <a class="ui-btn ui-icon-arrow-l ui-btn-icon-left" onclick="SearchRefreshData({{$previous_page}})">@lang('Previous')</a>
    @endif

    @if( $next_page!=null )
    <a class="ui-btn ui-icon-arrow-r ui-btn-icon-left" onclick="SearchRefreshData({{$next_page}})">@lang('Next')</a>
    @endif
</div>

@else
<button class="ui-btn ui-icon-back ui-btn-icon-left" onclick='$("#sideMenu").panel("open")'>@lang('No item found')</button>
@endif

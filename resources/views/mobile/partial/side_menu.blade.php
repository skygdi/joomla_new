
<script type="text/javascript">
$(document).ready(function () {
    $("#sideMenu").enhanceWithin().panel();
    $("#sideMenu").find("li").click(function() {
    	$("#sideMenu").find("li").removeClass("uk-active");
    	$(this).addClass("uk-active");
	});
});
</script>



<div data-role="panel" id="sideMenu" data-position="left" data-display="overlay" data-theme="a" data-dismissible="true" data-swipe-close="true">
	<div style="padding: 15px;">
		<form id="search" onsubmit="return Search(this)" method="post">
			<input type="search" name="searchword" placeholder="@lang('search')...">
		</form>
	</div>
	<ul>
		<li><a href="#div_page_home">@lang('Home')</a></li>
		<li><a href="#div_page_gallery">@lang('Gallery')</a></li>
		
		<li><a href="#div_page_profile">@lang('Profile')</a></li>
		<li><a href="javascript://void()" onclick="logout()" class="user">@lang('Logout')</a></li>
		
		
	</ul>
	<fieldset class="ui-field-contain" style="margin: 0px;">
		<select onchange="switch_language(this.value)">
			@if( $vars["locale"]=="en" )
				<option value="en" SELECTED>English</option>
			@else
				<option value="en">English</option>
			@endif
			@if( $vars["locale"]=="cn" )
				<option value="cn" SELECTED>中文</option>
			@else
				<option value="cn">中文</option>
			@endif
		</select>
	</fieldset>
	<div><h4><i class="fa fa-shopping-cart" aria-hidden="true" style=""></i>@lang('menu_bottom.Orders')</h4></div>
	<ul>
		<li><a href="#div_page_shopping_cart">@lang('Shopping Cart')</a></li>
		<li><a href="#div_page_orders">@lang('Orders')</a></li>
		<li><a href="#div_page_addresses_management">@lang('Addresses')</a></li>
	</ul>
	<div><h4><i class="fa fa-pencil-square-o" aria-hidden="true" ></i>@lang('My Works')</h4></div>
	<ul>
		<li><a href="#div_page_new_media">@lang('Step 1 Upload')</a></li>
		<li><a href="#div_page_media_management">@lang('MEDIA MANAGEMENT')</a></li>
		<li><a onclick="Step2PostCard()">@lang('Step 2 Postcard')</a></li>
		<li><a href="#div_page_postcard_management" class="user">@lang('Step 3 Order')</a></li>
	</ul>
</div>
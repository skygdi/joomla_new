<script>
    

    var CREATE_PAYMENT_URL  = '{{ URL('/') }}/payment/paypal/create';
    var EXECUTE_PAYMENT_URL = '{{ URL('/') }}/payment/paypal/execute';

    paypal.Button.render({

        env: '{{$PAYPAL_ENV}}', // 'production' Or 'sandbox'

        commit: true, // Show a 'Pay Now' button

        payment: function() {
            return paypal.request({
                method: 'post',
                url: CREATE_PAYMENT_URL,
                headers: {
                    'x-csrf-token': '{{ csrf_token() }}'
                }
            }).then(function(data) {
                return data.id;
            });
            /*
            return paypal.request.post(CREATE_PAYMENT_URL).then(function(data) {
                return data.id;
            });
            */
        },

        onAuthorize: function(data) {
            /*
            return paypal.request.post(EXECUTE_PAYMENT_URL, {
                paymentID: data.paymentID,
                payerID:   data.payerID
            }).then(function(data) {
             */
            return paypal.request({
                method: 'post',
                url: EXECUTE_PAYMENT_URL,
                headers: {
                    'x-csrf-token': '{{ csrf_token() }}'
                },
                data:{
                    'paymentID': data.paymentID,
                    'payerID':   data.payerID
                }
            }).then(function(data) {
                if( data.error!=null ){
                    alert(data.error.msg+"! Please contact our administrator for help");
                    return;
                }
                else{
                    alert("Thank you!");
                    window.location = "{{$after_paid_url}}";
                }
                
                // The payment is complete!
                // You can now show a confirmation message to the customer
            });
        }

    }, '#paypal-button');

    
</script>
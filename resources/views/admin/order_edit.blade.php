@extends('adminlte::page')

@section('title', 'Dashboard')

@section('adminlte_css')
@parent
    <link rel="stylesheet" href="{{ asset('css/item_manage.css') }}">

@stop

@section('adminlte_js')
    @parent
    <script src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/html2canvas.js') }}"></script>
    <script src="{{ asset('admin/js/order_edit.js') }}"></script>
    <script src="{{ asset('admin/js/postcard_download.js') }}"></script>

    <script type="text/javascript">
        @if( $data["cancel"]==0 && $data["paid"]==0 && $data["delivered"]==0 && $data["payment_validating"]==0 )
            var pay_state = "pending";
        @elseif( $data["cancel"]==0 && $data["paid"]==1 && $data["delivered"]==0 )
            var pay_state = "payment_received";
        @elseif( $data["cancel"]==0 && $data["paid"]==1 && $data["delivered"]==1 )
            var pay_state = "shipped";
        @elseif( $data["cancel"]==1 )
            var pay_state = "canceled";
        @elseif( $data["cancel"]==0 && $data["paid"]==0 && $data["payment_validating"]==1 )
            var pay_state = "validating_payment";
        @endif

        $(document).ready(function(){
            $("select[name='pay_state']").val( pay_state );
        });

        
    </script>
@stop



@section('content')
<form id="form_filter" method="POST" action="{{ URL('/') }}/admin/order/{{ $data['id'] }}">
<input type="hidden" name="_method" value="PUT">
{!! csrf_field() !!}
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-th-list"></i> Order Edit.
                <small class="pull-right">{{ Carbon\Carbon::parse($data->created_at)->format('m-d-Y') }}</small>
            </h2>
        </div>
    <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Billing Address:
            <address>
                <strong>{{ $data->BA_name }}</strong><br>
                {{ $data->BA_address1 }}, {{ $data->BA_address2 }}<br>
                {{ $data->BA_city }}, {{ $data->BA_state }} {{ $data->BA_zipcode }}<br>
                Phone: {{ $data->BA_phone }}<br>
                Email: {{ $data->BA_email }}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            Shipping Address:
            <address>
                <strong>{{ $data->SA_name }}</strong><br>
                {{ $data->SA_address1 }}, {{ $data->SA_address2 }}<br>
                {{ $data->SA_city }}, {{ $data->SA_state }} {{ $data->SA_zipcode }}<br>
                Phone: {{ $data->SA_phone }}<br>
                Email: {{ $data->SA_email }}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Invoice {{ $data->ID_show() }}</b>
            <br>
            <br>
            <b>status:</b>
            <select class="" name="pay_state">
                <option value="pending">Pending</option>
                <option value="payment_received">Payment received</option>
                <option value="shipped">Shipped</option>
                <option value="canceled">Canceled</option>
                <option value="validating_payment">Validating Payment</option>
            </select>
            <br>
            <b>Username:</b> {{ $data->User()->first()->name }}
            <br>
            <b>Payment Methods:</b> {{ $data->pay_type }}
            
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <p class="lead">Post Card list:</p>
            <table class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Image
                            <button type="button"
                                class="btn btn-default btn-sm" 
                                href="#"
                                onclick="ifc=0;CaptureProcess();">
                                @lang('Generate Download')
                            </button>
                        </th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $data->OrderItem()->get() as $item )
                    <tr>
                        <td>{{ $item["name"] }}</td>
                        <td>
                            <div class="col-xs-3 form-group-sm">
                                <input type="number" min=1 class="form-control" name="qty[{{ $item['id'] }}]"  value="{{ $item["quantity"] }}">
                            </div>
                        </td>
                        <td><span class="label label-success">${{ $item["price"] }}</span></td>
                        <td tid="{{ $item["id"] }}">
                            <a type="button" 
                                class="btn btn-default btn-xs" 
                                href="{{ URL('/') }}/media/images/{{ $item["file"] }}"
                                target="_blank">Front</a>
                            
                            <a type="button" 
                                class="btn btn-default btn-xs" 
                                onclick="DownloadCanvas(this,'{{ $item['id'] }}','back.png')"
                                style="display:none;"
                                tag="{{ $item['id'] }}">
                                @lang('The Back')
                            </a>

                            <div class="container_backside" iid="{{ $item['id'] }}" style="display: none;">
                                <img src="{{ URL('/') }}/images/postcard02.PNG" width="100%" for="backside_text_box"/>
                                <div class="user_content container_backside_text">{{ $item['content'] }}</div>
                                <div class="user_address" style="top:53px;">
                                    <div>{{ $data->BA_name }}</div>
                                    <div>{{ $data->BA_address1 }}</div>
                                    <div>{{ $data->BA_address2 }}</div>
                                    <div>{{ $data->BA_city }}</div>
                                    <div>{{ $data->BA_state }} {{ $data->BA_zipcode }}</div>
                                    <div>{{ $data->BA_country }}</div>
                                </div>

                                <div class="shipping_address">
                                    <div class="username">{{ $data->SA_name }}</div>
                                    <div class="address1">{{ $data->SA_address1 }}</div>
                                    <div class="address2">{{ $data->SA_address2 }}</div>
                                    <div class="city">{{ $data->SA_city }}</div>
                                    <div class="state">{{ $data->SA_state }} {{ $data->SA_zipcode }}</div>
                                    <div class="country">{{ $data->SA_country }}</div>
                                </div>
                            </div>
                            <div class="new_image_container" tag="{{ $item['id'] }}"></div>
                        </td>
                        <td><span class="label label-success">${{ number_format($item["quantity"]*$item["price"],2, '.', ',') }}</span></td>
                        <td>
                            <button type="button" class="btn btn-default btn-sm" onclick="RemoveOrderItem('{{ $item['id'] }}')"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
            <p class="lead">Balance:</p>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>${{ $data->Subtotal() }}</td>
                        </tr>
                        <tr>
                            <th>Tax (0%)</th>
                            <td>$0</td>
                        </tr>
                        <tr>
                            <th>Shipping:</th>
                            <td>${{ $data->ShippingFee() }}</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>${{ $data->Total() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Update </button>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
        </div>
    </div>
</section>
</form>

@stop

@section('adminlte_skygdi_footer')
    <!-- shooter DIV -->
    <div class="div_container_backside"></div>
@stop


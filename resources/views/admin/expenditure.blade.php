@extends('adminlte::page')

@section('title', 'Dashboard')

@section('adminlte_css')
	@parent
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<style type="text/css">
.bootstrap-select{
	/*width:100%;*/
}
</style>
	
@stop

@section('adminlte_js')
    @parent
	<script src="{{ asset('vendor/bootstrap-select/js/bootstrap-select.js') }}"></script>
	<script src="{{ asset('admin/js/expenditure.js') }}"></script>
    <script type="text/javascript">
	</script>
@stop

@section('content')
<section class="invoice">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">
				<i class="fa fa-dollar"></i>
				Expenditure List
				<small class="pull-right" style="margin-top: -10px;">
					<!--Date: 2/10/2014-->
					<form id="form_filter" method="POST" action="POST">
	            		<!-- form-horizontal -->
	                	<fieldset class="form-inline">
	                		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_add">
								<span class="glyphicon glyphicon-plus"></span>
							</button>
	              		</fieldset>
              		</form>	
				</small>
			</h2>
		</div>
	<!-- /.col -->
	</div>

    <!-- Table row -->
	<div class="row">
		<div class="col-xs-12 table-responsive">
			<table class="table table-striped">
			    <thead>
			        <tr>
			            <th scope="col">ID#</th>
			            <th scope="col">User</th>
			            <th scope="col">@lang('Created on')</th>
			            <th scope="col">@lang('Expenditure')</th>
			            <th>Note</th>
			            <th></th>
			        </tr>
			    </thead>
			    <tbody>
			        @foreach( $result as $payment )
			        <tr>
			            <td scope="row">{{$payment["id"]}}</td>
			            <td>{{ $payment->User()->first()->name }}</td>
			            <td>{{ Carbon\Carbon::parse($payment["created_at"])->format('j F Y') }}</td>
			            <td><span class="label label-success">$ {{ $payment["money"] }}</span></td>
			            <td>{{ $payment["note"] }}</td>
			            <td>
		            		<button type="button" class="btn btn-default btn-sm" onclick="RemovePayment('{{ $payment['id'] }}')"><i class="fa fa-trash-o"></i></button>
			            </td>
			        </tr>
			        @endforeach
			    </tbody>
			</table>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->

	<div class="row">
		<div class="col-md-8 pull-right">
		    <div id="pagination" class="pull-right">
		        {!! $result->render() !!}
		    </div>
		</div>
	</div>
  
</section>


<!-- Modal -->
<div class="modal fade" id="Modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-plus"></span></h4>
			</div>
			<div class="modal-body">
				<form id="form_add" method="POST">
				<div class="input-group col-xs-4">
				    <span class="input-group-addon">
				        <span class="glyphicon glyphicon-user"></span>
				    </span>
				    <select class="selectpicker" name="user_id" data-live-search="true">
				    	@foreach( $user_result as $user )
						<option value="{{ $user["id"] }}">{{ $user["name"] }}</option>
						@endforeach
					</select>
				</div>
				<br>
                <div class="input-group col-xs-4">
					<span class="input-group-addon" style="width:39px;">$</span>
					<input type="text" name="money" class="form-control" value="0" placeholder="amount">
				</div>
				<br>
				<div class="input-group col-xs-7">
					<textarea class="form-control" rows="3" name="note" placeholder="Note"></textarea>
				</div>
				

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="PaymentAdd()">Add</button>
			</div>
		</div>
	</div>
</div>
    
@stop
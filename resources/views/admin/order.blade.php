@extends('adminlte::page')

@section('title', 'Dashboard')

@section('adminlte_css')
	@parent
	<!--
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	-->
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-daterangepicker/css/daterangepicker.min.css') }}">
	
@stop

@section('adminlte_js')
    @parent
    <!--
	<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	-->
	<script src="{{ asset('admin/js/moment.min.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-daterangepicker/js/daterangepicker.min.js') }}"></script>
    <script src="{{ asset('admin/js/order.js') }}"></script>
    <script type="text/javascript">
	</script>
@stop

@section('content')
<section class="invoice">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">
				<i class="fa fa-list"></i>
				Order List
				<small class="pull-right" style="margin-top: -10px;">
					<!--Date: 2/10/2014-->
					<form id="form_filter" method="POST" action="POST">
	            		<!-- form-horizontal -->
	                	<fieldset class="form-inline">
		                	<select class="form-control" name="pay_state" value="{{$pay_state}}">
		                		<option value="">All</option>
								<option value="pending">Pending</option>
								<option value="payment_received">Payment received</option>
								<option value="shipped">Shipped</option>
								<option value="canceled">Canceled</option>
								<option value="validating_payment">Validating Payment</option>
							</select>
							<!--
							<input type="email" class="form-control datepicker" name="date_from" value="{{ $date_from }}" placeholder="From">
							<input type="email" class="form-control datepicker" name="date_to" value="{{ $date_to }}" placeholder="To">
							-->
							<input type="hidden" name="date_from" value="{{ $date_from }}">
							<input type="hidden" name="date_to" value="{{ $date_to }}">

							<div class="form-group">
								<div class="input-group">
									<button type="button" class="btn btn-default pull-right" id="daterange-btn">
										<span>
										<i class="fa fa-calendar"></i> Date range picker
										</span>
										<i class="fa fa-caret-down"></i>
									</button>
								</div>
							</div>
							<button type="button" class="btn btn-primary" onclick="change_filter()">Filter</button>
							<button type="reset" class="btn">Reset</button>
	              		</fieldset>
              		</form>	
				</small>
			</h2>
		</div>
	<!-- /.col -->
	</div>

	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box-header with-border">
            	<h3 class="box-title"></h3>
            	<div class="box-tools pull-right">
            		
            	</div>
            	<!-- /.box-tools -->
            </div>
		</div>
	<!-- /.col -->
	</div>
	
    <!-- Table row -->
	<div class="row">
		<div class="col-xs-12 table-responsive">
			<table class="table table-striped">
			    <thead>
			        <tr>
			            <th scope="col">ID#</th>
			            <th scope="col">User</th>
			            <th scope="col">@lang('Created on')</th>
			            <th scope="col">@lang('Total')</th>
			            <th scope="col">@lang('Payment Method')</th>
			            <th>@lang('Status')</th>
			        </tr>
			    </thead>
			    <tbody>
			        @foreach( $result as $order )
			        <tr>
			            <td scope="row"><a href="{{ URL('/') }}/admin/order/{{$order["id"]}}/edit">0011{{$order["id"]}}</a></td>
			            <td>{{$order->User()->first()->name}}</td>
			            <td class="uk-hidden-small">{{ Carbon\Carbon::parse($order["created_at"])->format('j F Y') }}</td>

			            <td class="uk-hidden-small"><span class="label label-success">$ {{$order->Total()}}</span></td>
			            <td class="uk-hidden-small">{{$order["pay_type"]}}</td>
			            <td>
			                @if( $order["cancel"]==0 && $order["paid"]==0 && $order["delivered"]==0 && $order["payment_validating"]==0 )
			                	<span class="label label-info">
			                		@lang('Pending')
			                	</span>
			                @elseif( $order["cancel"]==0 && $order["paid"]==1 && $order["delivered"]==0 )
			                	<span class="label label-success">
			                		@lang('Payment received')
			                	</span>
			                @elseif( $order["cancel"]==0 && $order["paid"]==1 && $order["delivered"]==1 )
			                	<span class="label label-success">
				                    @lang('SHIPPED')
				                </span>
			                @elseif( $order["cancel"]==1 )
			                	<span class="label label-danger">
				                    @lang('Canceled')
				                </span>
			                @elseif( $order["cancel"]==0 && $order["paid"]==0 && $order["payment_validating"]==1 )
				                <span class="label label-warning">
				                    @lang('Validating Payment')
				                </span>
			                
			                @endif
			            </td>
			        </tr>
			        @endforeach
			    </tbody>
			</table>
		</div>
	<!-- /.col -->
	</div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-8 pull-right">
        <div id="pagination" class="pull-right">
            {!! $result->render() !!}
        </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  
</section>

    
@stop
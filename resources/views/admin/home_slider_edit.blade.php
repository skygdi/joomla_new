@extends('adminlte::page')

@section('title', 'Dashboard')

@section('adminlte_css')
	@parent
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<style type="text/css">
.bootstrap-select{
	width:100%;
}
</style>
	
@stop

@section('adminlte_js')
    @parent
	<script src="{{ asset('vendor/bootstrap-select/js/bootstrap-select.js') }}"></script>
	<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
	<script>
	$(document).ready(function(){
    	$("[name='content']:lt(1)").ckeditor();
	});
    </script>
@stop

@section('content')
<section class="invoice">
	@if (Session::has('message'))
    <div class="row">
		<div class="col-xs-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-check"></i>Success!</h4>
				{{ Session::get('message') }}
			</div>
		</div>
	</div>
	@elseif(!empty($errors->first()))
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i>Error!</h4>
				{{ $errors->first() }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">
				<i class="fa fa-image"></i>
				Slider Edit
				<small class="pull-right" style="margin-top: -10px;">
					<a href="{{ URL('/') }}/admin/slider"><span class="fa fa-arrow-left"></span></a>
				</small>
			</h2>
		</div>
	<!-- /.col -->
	</div>


    <!-- Table row -->
	<div class="row">
		<div class="col-xs-12 table-responsive">
			
			<form id="form_edit" method="POST" action="{{ URL('/') }}/admin/slider/{{ $slider["id"] }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="box-body">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-12">
								<label for="exampleInputEmail1">File</label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<img src="{{ URL('/') }}/slider/{{ $slider["file"] }}" style="max-width:200px;">
							</div>
							<div class="col-xs-5">
								<input type="file" name="file_image" class="file form-control" value="" accept="image/*">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Content</label>
						<textarea class="form-control" rows="5" name="content" placeholder="Content">{{ $slider["content"] }}</textarea>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-lg-12">
								<label for="exampleInputPassword1">Priority</label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-1">
								<input type="number" min=0 class="form-control" name="priority" value="{{ $slider["priority"] }}">
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
            </form>
			
		</div>
	<!-- /.col -->
	</div>
  <!-- /.row -->

</section>


    
@stop
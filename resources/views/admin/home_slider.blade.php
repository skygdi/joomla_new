@extends('adminlte::page')

@section('title', 'Dashboard')

@section('adminlte_css')
	@parent
	<link rel="stylesheet" href="{{ asset('css/checkbox.css') }}">
<style type="text/css">
.modal-editor {
	width: 750px;
	margin: auto;
}
</style>
@stop

@section('adminlte_js')
    @parent
	<script src="{{ asset('admin/js/slider.js') }}"></script>
	<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
	<script>
	$(document).ready(function(){
    	$("[name='content']:lt(1)").ckeditor();
	});
    </script>
@stop

@section('content')
<section class="invoice">
	@if(!empty($errors->first()))
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i>Error!</h4>
				{{ $errors->first() }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">
				<i class="fa fa-image"></i>
				Slider List
				<small class="pull-right" style="margin-top: -10px;">
					<!--Date: 2/10/2014-->
					<form id="form_filter" method="POST" action="POST">
	            		<!-- form-horizontal -->
	                	<fieldset class="form-inline">
	                		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_add">
								<span class="glyphicon glyphicon-plus"></span>
							</button>
	              		</fieldset>
              		</form>	
				</small>
			</h2>
		</div>
	<!-- /.col -->
	</div>


    <!-- Table row -->
	<div class="row">
		<div class="col-xs-12 table-responsive">
			<table class="table table-striped">
			    <thead>
			        <tr>
			            <th scope="col">File</th>
			            <th scope="col">content</th>
			            <th scope="col">Priority</th>
			            <th scope="col" style="text-align: center;">Suspend</th>
			            <th scope="col" style="text-align: center;"></th>
			        </tr>
			    </thead>
			    <tbody>
			        @foreach( $result as $slider )
			        <tr>
			            <td>
			            	<a href="{{ URL('/') }}/admin/slider/{{ $slider["id"] }}/edit">
			            		<img src="{{ URL('/') }}/slider/{{ $slider["file"] }}" style="max-width:150px;">
			            	</a>
		            	</td>
			            <td>{!! $slider["content"] !!}</td>
			            <td>{{$slider["priority"]}}</td>
			            <td align="center">
			            	<div class="checkbox">
					            <label style="font-size: 2.5em">
					                <input type="checkbox" value="" onchange="SliderSuspend(this,'{{ $slider["id"] }}')"
					                	@if( $slider["suspend"]=="1" )
					                	checked
					                	@endif
				                	>
					                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
					            </label>
					        </div>
			            </td>
			            <td align="center">
			            	<button type="button" class="btn btn-default btn-sm" onclick="RemoveSlider('{{ $slider["id"] }}')"><i class="fa fa-trash-o"></i></button>
			            </td>
			        </tr>
			        @endforeach
			    </tbody>
			</table>
		</div>
	<!-- /.col -->
	</div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-8 pull-right">
        <div id="pagination" class="pull-right">
            {!! $result->render() !!}
        </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  
</section>



<!-- Modal -->
<div class="modal fade" id="Modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-editor" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>
				<h4 class="modal-title"><span class="glyphicon glyphicon-plus"></span></h4>
			</div>
			
			<form id="form_add" method="POST" action="{{ URL('/') }}/admin/slider" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="modal-body">
				
				<div class="input-group col-xs-7">
				    <span class="input-group-addon">
				        <span class="fa fa-image"></span>
				    </span>
				    <input type="file" name="file_image" class="file form-control" value="" accept="image/*" required>
				</div>
				<br>
				<br>
				<div class="input-group col-xs-12">
					<textarea class="form-control" rows="3" name="content" placeholder="Content"></textarea>
				</div>
				

				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Add</button>
			</div>
			</form>
		</div>
	</div>
</div>
    
@stop
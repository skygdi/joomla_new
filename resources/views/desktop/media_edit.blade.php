@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/keyword.css') }}">
@stop

@section('js')
    @parent
    
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/angular.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/submission_keyword.js') }}"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        @if( isset($result_flag) && $result_flag==true )
            swal({
                title: "",
                text: "{{$result_string}}",
                timer: 4000,
                type: 'success'
            });
        @else
            @if( isset($result_flag) )
                swal({
                    title: "",
                    text: "{{$result_string}}",
                    timer: 6000,
                    type: 'error'
                });
            @endif
        @endif
    });

    var template = '<div class="tags">' +
                    '<div ng-repeat="(idx, tag) in tags" class="tag label label-success">@{{tag}} <a class="close" href ng-click="remove(idx)">×</a></div>' +
              '</div>' +
              '<div class="input-group"><input type="text" class="form-control uk-form" style="" placeholder="@lang('add a keyword')..." ng-model="newValue" /> ' +
              '<span class="input-group-btn"><a class="btn btn-default" ng-click="add()" style="height: 40px;line-height: 40px;background-color: #465d7a;color: white;">@lang('Add')</a></span></div>';

    </script>
@stop



@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    <form method="post" action="{{URL('/')}}/media/{{$vars["media"]->id}}" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="put" />
    <input type="hidden" name="id" value="{{$vars["media"]->id}}">
    <div class="row">
        <div class="col-md-12">
            <h3 class="uk-h3">@lang('Edit Media')</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="first_name">@lang('Name'):</label>
                <input type="text" class="form-control uk-form" name="name" required value="{{$vars["media"]->name}}">
            </div>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group btn-file">
                <label for="first_name">@lang('Choose A Image File To Replace'):</label>
                <input type="file" name="file_image" class="filestyle uk-form" data-buttonName="btn-primary" data-buttonText="@lang('CHOOSE IMAGE')" accept="image/*">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <img src="{{URL('/')}}/media/images/{{$vars["media"]->file}}">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 form-group{{ $errors->has('keywords') || $errors->has('keywords.*') ? ' has-error' : '' }}">
            <div class="form-group btn-file" ng-app="myApp" ng-controller="tagsCtrl" ng-init="tags=@if(old('keywords')!==null){{old('keywords')}}@elseif(isset($vars['tags_array']) && strlen($vars['tags_array'])>0)[{!!$vars["tags_array"]!!}]@else[]@endif">
                <label for="first_name">@lang('KeyWords'):</label>
                <tag-manager tags="tags" autocomplete="allTags"></tag-manager>
                <input type="text" style="display:none;" name="keywords" value="@{{tags}}">
            </div>
            @if ($errors->has('keywords'))
                <span class="help-block">
                    <strong>{{ $errors->first('keywords') }}</strong>
                </span>
            @endif

            
            @foreach ( $errors->get('keywords.*') as $messsage)
                @foreach ($messsage as $error)
                    <span class="help-block">
                        <strong>{{ $error }}</strong>
                    </span>
                @endforeach
            @endforeach
            

        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <label for="first_name">@lang('Public'):</label>
            <div class="input-group">
                <div class="radio-inline">
                    
                    <label><input type="radio" name="searchable" value="0"
                    @if( $vars["media"]->searchable==0 )
                    checked
                    @endif
                    >@lang('No')</label>
                </div>
                <div class="radio-inline">
                    <label><input type="radio" name="searchable" value="1"
                    @if( $vars["media"]->searchable==1 )
                    checked
                    @endif
                    >@lang('Yes')</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <button type="submit" class="btn btn-primary">@lang('SUBMIT')</button>
            <a class="btn" href="{{URL('/')}}/media">@lang('CANCEL')</a>
        </div>
    </div>
    </form>
</div>

@stop
@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/item_manage.css') }}">
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/media.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart.js') }}"></script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script type="text/javascript">
var re_order_tips = "@lang('Please note, that your current cart items will be purged. Press OK if you are sure you wish to proceed.')";
</script>
@stop

@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    
    <div class="row">
        <div class="col-md-7">
            <h2 class="uk-h2 uk-display-inline">
                @lang('order_detail.Order') 0011{{$data["id"]}}
                @if( $data["cancel"]==0 && $data["paid"]==0 && $data["delivered"]==0 && $data["payment_validating"]==0 )
                    <span class="uk-badge uk-badge-default" title="This is the status of an order that has yet to be payed, and has just been saved from your cart" data-uk-tooltip="">
                        @lang('Pending')
                    </span>
                    @elseif( $data["cancel"]==0 && $data["paid"]==1 && $data["delivered"]==0 )
                    <span class="uk-badge uk-badge-success" title="" data-uk-tooltip="" data-cached-title="Status for order, when payment is received">
                        @lang('Payment received')
                    </span>
                    @elseif( $data["cancel"]==0 && $data["paid"]==1 && $data["delivered"]==1 )
                    <span class="uk-badge uk-badge-default" title="This is the status of an order that has yet to be payed, and has just been saved from your cart" data-uk-tooltip="">
                        @lang('SHIPPED')
                    </span>
                    @elseif( $data["cancel"]==1 )
                    <span class="uk-badge uk-badge-danger" title="" data-uk-tooltip="" data-cached-title="Status for order that was cancelled by user or administrator">
                        @lang('Canceled')
                    </span>
                    @elseif( $data["cancel"]==0 && $data["paid"]==0 && $data["payment_validating"]==1 )
                    <span class="uk-badge uk-badge-default" title="" data-uk-tooltip="" data-cached-title="Order being validating">
                        @lang('Validating Payment')
                    </span>
                
                @endif
            </h2>
        </div>
        <div class="col-md-5" style="padding-top:40px;">
            <div class="pull-right">
                <a href="#" onclick="window.print();" class="uk-button uk-button-mini">
                    <i class="uk-icon-print" id="toolbar-print"></i> @lang('Print')</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 uk-table" style="padding: 2px;">
            <table class=" uk-table-striped uk-table-condensed" style="width:100%;">
                <thead>
                    <tr>
                        <th></th>
                        <th class="uk-hidden-small">@lang('Quantity')</th>
                        <th class="uk-hidden-small">@lang('Price')</th>
                        <th class="uk-hidden-small">@lang('Total')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $data->OrderItem()->get() as $item )
                    <tr>
                        <td>{{$item["name"]}}</td>
                        <td class="uk-hidden-small">{{$item["quantity"]}}</td>
                        <td class="uk-hidden-small">${{$item["price"]}}</td>
                        <td class="uk-hidden-small">{{ number_format($item["quantity"]*$item["price"],2, '.', ',') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 pull-right">
            <div class="zx-zoocart-tableitems-tax-notice uk-text-small pull-right" style="opacity: 0.5;">@lang('sales taxes not included in item price')  </div>
            <div class="zx-zoocart-tableitems-totals pull-right">
                <table style="width: 100%;line-height: 25px;margin-top: 15px;">
                    <tbody>
                        <tr>
                            <td align="right">@lang('Payment Fee')</td>
                            <td align="right" style="width: 200px;" class="the_data">$0.00</td>
                        </tr>
                        <tr>
                            <td align="right">@lang('Subtotal')</td>
                            <td align="right" class="the_data">${{ number_format($data->Subtotal(), 2, '.', ',') }}</td>
                        </tr>
                        <tr>
                            <td align="right">@lang('Shipping Fee')</td>
                            <td align="right" class="the_data">${{ number_format($data->ShippingFee(), 2, '.', ',') }}</td>
                        </tr>
                        <tr>
                            <td align="right">@lang('TAXES')</td>
                            <td align="right" class="the_data">$0.00</td>
                        </tr>
                        <tr>
                            <td align="right">@lang('Total')</td>
                            <td align="right"><b>${{ number_format($data->Total(), 2, '.', ',') }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 35px;">
        <div class="col-md-12">
            <a class="uk-button uk-button-primary pull-right" onclick="re_order('{{$data["id"]}}')" href="javascript://void()">@lang('Re order')</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 user_detail">
            <div style="color: gray;">@lang('Billing Address')</div>
            <div style="color:#aaaaaa;">{{ $data["BA_name"] }}</div>
            <div><a href="mailto:{{ $data["BA_email"] }}">{{ $data["BA_email"] }}</a></div>
            <div style="color:#aaaaaa;font-size:12.8px;">@lang('The Address this order will be invoiced to')</div>
        </div>
    </div>

    <div class="row" style="padding-left: 14px;">
        <hr style="" class="my_hr">
    </div>

    <div class="row">
        <div class="col-md-12 user_detail">
            <div style="color: gray;">@lang('Shipping Address')</div>
            <div style="color:#aaaaaa;">{{ $data["SA_name"] }}</div>
            <div><a href="mailto:{{ $data["SA_email"] }}">{{ $data["SA_email"] }}</a></div>
            <div style="color:#aaaaaa;font-size:12.8px;">@lang('The Address this order goods will be shipped to')</div>
        </div>
    </div>

    <div class="row" style="padding-left: 14px;">
        <hr style="" class="my_hr">
    </div>

    <div class="row">
        <div class="col-md-12 user_detail">
            <div style="color: gray;">@lang('Shipping Method')</div>
            <div style="color:#aaaaaa;">@lang('Standard Shipping')</div>
            <div style="color:#aaaaaa;font-size:12.8px;">@lang('The Shipping method this order goods will be processed with')</div>
        </div>
    </div>

    @if( $data["paid"]==0 && $data["cancel"]==0 && $data["payment_validating"]==0 )
    <div class="row">
        <div class="col-md-12" style="text-align: center; margin-top: 20px;">
            <div id="paypal-button" style=""></div>
        </div>
    </div>
    @include('desktop.partial.paypal')
    @endif

    

</div>




@stop
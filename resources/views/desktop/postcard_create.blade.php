@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/owl.carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/postcard_new.css') }}">
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/postcard.js') }}"></script>
    <script type="text/javascript">
    var msg_input_name = "@lang('Please Input Post Card Name')";
    var msg_select_image = "@lang('Please Select An Image First')";
    var msg_success = "@lang('Success')";
    @if( isset($preselect_media) )
    current_item_id = {{ $preselect_media->id }};
    @endif
    $(document).ready(function(){
        init_carousel();
    });
    </script>
@stop


@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    <!--<form method="post" action="?" enctype="multipart/form-data">-->
    <div class="row">
        <div class="col-md-12">
            <h3 class="uk-h3">@lang('CREATE POSTCARD')</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>@lang('SELECT IMAGE'): <span>@lang('Front') (@lang('Image File'))</span></label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="owl-carousel owl-theme">
                @foreach ($medias as $media)
                    @php
                        $media->ImageCacheGet();
                    @endphp
                    <div class="item" style="width:100%;height:200px;position: relative;">
                        <img onclick="select_image(this,'{{ $media['id'] }}')" 
                            class="owl-lazy" 
                            data-src="{{ $media->image_cache_url }}" 
                            original_src="media/images/{{ $media->file }}" 
                            alt="{{ $media['alias'] }}"/>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="name">@lang('Name'):</label>
                        <input type="text" class="form-control uk-form" name="name" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">@lang('Content'):</label>
                        <div class="container_backside">
                            <img src="{{ URL('/') }}/images/postcard02.PNG" width="100%" for="backside_text_box">
                            <textarea name="content" value="" maxlength="430" class="user_content container_backside_text" id="backside_text_box"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="text-align: right;padding-top:113px;">
            <img id="big_image" src="@if( isset($preselect_media) ) {{ URL('/') }}/media/images/{{$preselect_media->file}} @endif" style="max-width: 535px;">
        </div>
    </div>


    <div class="row">
        <div class="col-sm-7">
            <button type="button" class="btn btn-primary" onclick="NewPostCard()">@lang('SUBMIT')</button>
        </div>
    </div>
</div>

@stop
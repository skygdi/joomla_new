@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/keyword.css') }}">
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/angular.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/submission_keyword.js') }}"></script>
    <script type="text/javascript">
    var template = '<div class="tags">' +
                    '<div ng-repeat="(idx, tag) in tags" class="tag label label-success">@{{tags}}<a class="close" href ng-click="remove(idx)">×</a></div>' +
                  '</div>' +
                  '<div class="input-group"><input type="text" class="form-control uk-form" style="" placeholder="@lang('add a keyword')..." ng-model="newValue" /> ' +
                  '<span class="input-group-btn"><a class="btn btn-default" ng-click="add()" style="height: 40px;line-height: 40px;background-color: #465d7a;color: white;">@lang('Add')</a></span></div>';
    </script>
@stop



@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    <form method="post" action="{{ URL('/') }}/media" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <h3 class="uk-h3">@lang('Upload Media')</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="name">@lang('Name'):</label>
                <input type="text" class="form-control uk-form" name="name" required>
            </div>
        </div>
    </div>
    <div class="row {{ $errors->has('file_image') ? 'has-error' : '' }}">
        <div class="col-md-5">
            <div class="form-group btn-file">
                <label for="first_name">@lang('Image File'):</label>
                <input type="file" name="file_image" class="filestyle uk-form" data-buttonName="btn-primary" data-buttonText="@lang('CHOOSE IMAGE')" accept="image/*" required>
            </div>
            @if ($errors->has('file_image'))
                <label class="control-label">{{ $errors->first('file_image') }}</label>
            @endif

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group btn-file" ng-app="myApp" ng-controller="tagsCtrl" ng-init="tags=[]">
                <label for="first_name">@lang('KeyWords'):</label>
                <tag-manager tags="tags" autocomplete="allTags"></tag-manager>
                <input type="text" style="display:none;" name="keywords" value="@{{tags}}">
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <label for="first_name">@lang('Public'):</label>
            <div class="input-group">
                <div class="radio-inline">
                    <label><input type="radio" name="searchable" value="0">@lang('No')</label>
                </div>
                <div class="radio-inline">
                    <label><input type="radio" name="searchable" value="1" checked>@lang('Yes')</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <button type="submit" class="btn btn-primary">@lang('SUBMIT')</button>
        </div>
    </div>
    </form>
</div>

@stop
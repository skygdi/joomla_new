@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/keyword.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<style>
.bootstrap-select .btn-default{
    color:white !important;
}
.search_result + .search_result {
    padding-top: 15px;
    border-top: 2px solid rgba(77, 77, 77, 0.89);
}
</style>
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/media.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/search.js') }}"></script>

<script type="text/javascript">

var msg_validation_name = "@lang('Please enter name')";
var ordering = "{{$ordering}}";

$(document).ready(function(){
});
</script>
@stop


@section('content')
<div class="container container_common" style="padding-top:105px !important;">
    <div class="row">
        <div class="col-md-7">
            <form class="form-horizontal" method="post" id="formSearch" action="{{ url('/search') }}">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-9">
                    <div class="input-group">
                        <input type="text" name="word" class="form-control" placeholder="@lang('Search for Word')" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;" value="{{$word}}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit" style="">@lang('Search')</button>
                        </span>
                    </div>
                </div>

                <div class="col-md-3">
                    <select name="ordering" class="selectpicker" style="" data-live-search="false" title="@lang('Ordering')">
                        <option value="newest">@lang('Newest First')</option>
                        <option value="oldest">@lang('Oldest First')</option>
                        <option value="popular">@lang('Most Popular')</option>
                    </select>
                </div>
            </div>
            
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            @foreach( $result as $media )
            @if($media["file"]!=null)
            <div class="search_result">
                <div class="form-inline" style="font-size:18px;">
                    {{$media["name"]}}
                </div>

                <div style="font-size:18px;">
                    <a href="{{ URL('/postcard') }}/{{$media["id"]}}/create" title="{{$media["alias"]}}">
                        <img class="media" src="{{ URL('/') }}/media/images/{{ $media['file'] }}" alt="{{$media["alias"]}}" style="max-height: 100px;">
                    </a>
                    (<a href="{{ URL('/user') }}/{{$media["user_id"]}}/channel" title="{{$media->User()->first()["name"]}}"> {{$media->User()->first()["name"]}} </a>)
                </div>
                <div class="form-inline" style="margin-top:10px;margin-bottom:10px;">
                    <div class="form-group" style="margin-left:5px;">
                        @if( !Auth::check() )
                        <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                        @elseif($media->Liked())
                        <i aria-hidden="true" cn="fa fa-thumbs-o-up" cp="fa fa-thumbs-up" class="fa fa-thumbs-up" cr="p" onclick="change_like('{{$media["id"]}}',this)"></i>
                        @else
                        <i aria-hidden="true" cn="fa fa-thumbs-o-up" cp="fa fa-thumbs-up" class="fa fa-thumbs-o-up" cr="n" onclick="change_like('{{$media["id"]}}',this)"></i>
                        @endif
                        <a><span class="badge">{{$media->Likes()}}</span></a>
                    </div>
                    <div class="form-group" style="margin-left:5px;">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                        <a><span class="badge">{{$media->shows}}</span></a>
                    </div>
                    <div class="form-group" style="margin-left:5px;">
                        <i class="fa fa-usd" aria-hidden="true"></i>
                        <a><span class="badge">{{$media->Sold()}}</span></a>
                    </div>
                    <div class="form-group" style="margin-left:5px;">
                        <span class="glyphicon glyphicon-pencil"></span>
                        <a><span class="badge">{{$media->Comment()->count()}}</span></a>
                    </div>
                </div>

                <div class="form-inline" style="margin-bottom:10px;">
                    <div class="tags">
                        @foreach ($media->KeyWord()->get() as $word)
                        <a href="{{ URL('/') }}/search/{{$word['name']}}">
                            <div class="tag tag_no_remove label label-success ng-scope ng-binding">{{$word['name']}}</div>
                        </a>
                        @endforeach
                    </div>
                </div>

            </div>
            @endif
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="pagination" class="pull-right">
                {!! $result->render() !!}
            </div>
        </div>
    </div>

</div>

@stop
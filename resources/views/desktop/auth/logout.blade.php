@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@stop

@section('js')
    @parent
    
    
    <script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
    <!--
    <script type="text/javascript">
    var msg_validation_name = "@lang('Please enter the recipient name')";
    var msg_validation_password = "@lang('Please enter password')";
    </script>
    -->
    
    
@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">
    <div class="row" style="margin-bottom: 40px;">
        <div class="col-md-8">
            <div class="well" style="border: 2px solid rgba(77, 77, 77, 0.89);">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" onclick="logout()">@lang('LOG OUT')</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
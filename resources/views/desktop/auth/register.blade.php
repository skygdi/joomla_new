@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@stop

@section('js')
    @parent

    
    <script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.number.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.pulse.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>

<script type="text/javascript">
</script>    
@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">

    <div class="row" style="margin-bottom: 40px;">
        <div class="col-md-8">
            <div class="well" style="border: 2px solid rgba(77, 77, 77, 0.89);">
                <div class="row">
                    <div class="col-md-12">
                        <legend>@lang('User Registration')</legend>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" enctype="multipart/form-data" id="formCreateAccount" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('username') ? 'has-error' : '' }}">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="username" value="{{ old('username') }}" required placeholder="@lang('Enter')  @lang('Username')">
                                        </div>
                                    </div>
                                    @if ($errors->has('username'))
                                        <label class="control-label">{{ $errors->first('username') }}</label>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" value="" required placeholder="@lang('Enter') @lang('Password')">
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <label class="control-label">{{ $errors->first('password') }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password_confirmation" value="" required placeholder="@lang('Enter') @lang('Confirm Password')">
                                        </div>
                                    </div>
                                    <label class="control-label"></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="uk-icon-bookmark-o"></i></span>
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required placeholder="@lang('Enter') @lang('Name')">
                                        </div>
                                    </div>
                                    @if ($errors->has('name'))
                                        <label class="control-label">{{ $errors->first('name') }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="@lang('Enter') @lang('Email')">
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <label class="control-label">{{ $errors->first('email') }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                            <input type="email" class="form-control" name="email_confirmation" value="{{ old('email_confirmation') }}" required placeholder="@lang('Enter') @lang('Confirm Email')">
                                        </div>
                                    </div>
                                    <label class="control-label"></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('paypal') ? 'has-error' : '' }}">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-paypal" aria-hidden="true"></i></span>
                                            <input type="email" class="form-control" name="paypal" value="{{ old('paypal') }}" required placeholder="@lang('Enter') @lang('Paypal')">
                                        </div>
                                    </div>
                                    @if ($errors->has('paypal'))
                                        <label class="control-label">{{ $errors->first('paypal') }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">@lang('SUBMIT')</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@stop
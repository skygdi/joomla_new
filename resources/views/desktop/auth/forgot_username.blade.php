@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@stop

@section('js')
    @parent
@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">

    <div class="row" style="margin-bottom: 40px;">

        <div class="col-md-8">
            <div class="well" style="border: 2px solid rgba(77, 77, 77, 0.89);">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12">
                        <legend>@lang('Forgot your username?')</legend>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" id="formForgotUsername" action="{{ route('username.send') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('Enter')  @lang('Email')" required>
                                        </div>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <label class="control-label">{{ $errors->first('email') }}</label>
                                                
                                            </span>
                                        @endif
                                    </div>

                                    
                                    
                                    <div class="col-md-6">
                                        {!! Recaptcha::render() !!}

                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                                <label class="control-label">{{ $errors->first('g-recaptcha-response') }}</label>
                                                
                                            </span>
                                        @endif

                                    </div>
                                    
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">@lang('SUBMIT')</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@stop
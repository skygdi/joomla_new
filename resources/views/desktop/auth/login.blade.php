@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<style>
.btn-social{
    text-align:left;
    padding-left: 15px;
}
.fa-social{
    padding-left: 15px;
    padding-right: 15px;
}
</style>
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
    <!--<script type="text/javascript" src="{{ asset('js/login.js') }}"></script>-->
    <script type="text/javascript">
    /*
    var msg_validation_name = "@lang('Please enter name')";
    var msg_validation_password = "@lang('Please enter password')";
    var msg_username_or_password_error = "@lang('Username and or password error')";
    */
    </script>
@stop


@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-md-8">
            <div class="well" style="border: 2px solid rgba(77, 77, 77, 0.89);background-color: white;">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" id="formLogin" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('username') ? 'has-error' : '' }}">
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="@lang('Enter')  @lang('Username')">
                                        </div>
                                    </div>
                                    @if ($errors->has('username'))
                                        <label class="control-label">{{ $errors->first('username') }}</label>
                                        <script type="text/javascript">
                                        swal({
                                            title: "",
                                            text: "{{ $errors->first('username') }}",
                                            timer: 5000,
                                            type: 'error'
                                        });
                                        </script>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" value="" placeholder="@lang('Enter')  @lang('Password')">
                                        </div>
                                    </div>
                                    @if ($errors->has('password'))
                                        <label class="control-label">{{ $errors->first('password') }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label><input id="remember" type="checkbox" value="yes">@lang('Remember me')</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">@lang('LOG IN')</button>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="col-md-5" style="display:none;">
                        <div class="row">
                            <form class="connect-button" method="POST" action="{{ URL('/') }}/socialauth/login/google-oauth2">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-block btn-social btn-google" style="background-color: #DD4B39;">
                                    <span class="fa fa-social fa-google"></span> Sign in with Google
                                </button>
                            </form>
                        </div>
                        <div class="row">
                            <form class="connect-button" method="POST" action="{{ URL('/') }}/socialauth/login/facebook-oauth2">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-block btn-social btn-facebook" style="background-color: #3B5998;">
                                    <span class="fa fa-social fa-facebook"></span> Sign in with Facebook
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <ul class="list-group">
                <li class="list-group-item"><a href="{{ URL('/password/reset') }}">@lang('Forgot your password?')</a></li>
                <li class="list-group-item"><a href="{{ URL('/username/forgot') }}">@lang('Forgot your username?')</a></li>
                <li class="list-group-item"><a href="{{ URL('/register') }}">@lang('Don\'t have an account?')</a></li>
            </ul>
        </div>
    </div>
</div>

@stop
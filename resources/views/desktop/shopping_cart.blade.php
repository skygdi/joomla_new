@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<style>
.uk-list-line > li:nth-child(n+2) {
    margin-top: 5px;
    padding-top: 5px;
    border-top: 2px solid rgba(77, 77, 77, 0.89);
}
.dropdown-toggle{
    height: 34px;
    border-radius: 0px;
    border: 1px solid #ccc;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
#state_area .bootstrap-select{
    width: 220px;
}
#country_area .dropdown-toggle{
    height: 30px;
    border: 1px solid #ccc;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
</style>
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.number.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.pulse.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/shopping_cart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart_address_public.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart_address_billing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart_address_shipping.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script type="text/javascript">
    var msg_validation_name = "@lang('Please enter the recipient name')";
    var msg_validation_email = "@lang('Please enter a valid email address')";
    var msg_validation_state = "@lang('Please type or select a state')";
    var msg_validation_address1 = "@lang('Please enter a valid address')";
    var msg_validation_zipcode = "@lang('Please enter a valid zipcode, digits only')";
    var msg_validation_country = "@lang('Please select a country')";
    var msg_order_empty = "@lang('Please add item first')";
    var msg_confirm = "@lang('Are you sure?')";
    </script>



@stop




@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        <div class="col-md-12">
            <h3 class="title">@lang('Your Cart')</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table width="100%" id="cart_item">
                <thead>
                    <tr>
                        <th></th>
                        <th width=151 class="uk-text-center">@lang('Quantity')</th>
                        <th width=151 class="text-right">@lang('Price')</th>
                        <th width=151 class="text-right">@lang('Total')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($result as $item)
                    <tr item_id="{{ $item["id"] }}">
                        <td>
                            <div class="zx-zoocart-tableitems-row-name">
                                <a href="javascript://void()" onclick="Obj_ShoppingCart.Remove('{{ $item["id"] }}',this);" class="zx-zoocart-tableitems-row-remove" title="Remove the Item from the Cart">
                                    <i class="uk-icon-times"></i>
                                </a>
                                <div class="uk-display-inline">{{$item->PostCard()->first()->name}}</div>
                            </div>
                        </td>
                        <td>
                            <div class="zx-zoocart-tableitems-row-quantity uk-text-center">
                                <input type="number" min="1" class="uk-form-width-mini uk-form-small uk-form-blank uk-text-center" name="" value="{{$item->quantity}}" item_id='{{ $item["id"] }}' onchange="Obj_ShoppingCart.Change('{{ $item["id"] }}',this);">
                            </div>
                        </td>
                        <td>
                            <div class="text-right price">
                                <div class="uk-display-inline">$<span>{{$item->PostCard()->first()->Price()}}</span></div>
                            </div>
                        </td>
                        <td>
                            <div class="text-right total">
                                <i class=""></i>
                                <div class="uk-display-inline">$<span>{{$item->Total()}}</span></div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="zx-zoocart-tableitems-tax-notice uk-text-small pull-right">@lang('sales taxes not included in item price')</div>
        </div>
    </div>

    <div class="row" style="font-size: 16px;">
        <div class="col-md-12 text-center">
            <span class="" data-toggle="tooltip" title="The fee applied by the Shipping method" style="margin-left:10px;margin-right:10px;">
                <span class="">@lang('Shipping Fee')</span>
                <span class="zx-x-value">$<span class="shipping_fee">0.00</span></span>
            </span>

            <span class="" data-toggle="tooltip" title="The fee applied by the Payment method" style="margin-left:10px;margin-right:10px;">
                <span class="">@lang('Payment Fee')</span>
                <span class="zx-x-value">$0.00</span>
            </span>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table style="width:270px;line-height: 30px;margin-top: 25px;margin-bottom: 25px;" class="pull-right">
                <tbody>
                    <tr>
                        <td align="right">@lang('SUBTOTAL')</td>
                        <td align="right" style="width:194px;"><span class="sub_total"></span></td>
                    </tr>
                    <tr>
                        <td align="right">@lang('TAXES')</td>
                        <td align="right">$0.00</td>
                    </tr>
                    <tr>
                        <td align="right">@lang('TOTAL')</td>
                        <td align="right" style="font-weight:bold;">$<span class="total"></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    

    <div class="row" style="margin-bottom: 40px;">
        <div class="col-md-6">
            @include('desktop.partial.shopping_cart.billing_address')
        </div>

        <div class="col-md-6">
            @include('desktop.partial.shopping_cart.shipping_address')
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-6">
            @include('desktop.partial.shopping_cart.shipping_methods')
        </div>

        <div class="col-md-6">
            @include('desktop.partial.shopping_cart.agreement')
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 pull-right">
            <button type="button" class="zx-zoocart-checkout-placeorder uk-button uk-button-success pull-right zx-animate-shake" onclick="checkout(this)">
            <i class="uk-icon-shopping-cart"></i>&nbsp;&nbsp;&nbsp;@lang('Checkout')</button>
            <div id="paypal-button" style="float:right;display: none;"></div>
        </div>
    </div>
</div>



@include('partial.paypal')


@include('desktop.partial.shopping_cart.modal_billing_address_new')
@include('desktop.partial.shopping_cart.modal_billing_address_edit')

@include('desktop.partial.shopping_cart.modal_shipping_address_new')
@include('desktop.partial.shopping_cart.modal_shipping_address_edit')


@stop
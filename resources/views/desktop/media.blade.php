@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/item_manage.css') }}">
    <link rel="stylesheet" href="{{ asset('css/keyword.css') }}">
@stop

@section('js')
    @parent

    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/comment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/media.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/esimakin-twbs-pagination/jquery.twbsPagination.js') }}"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    });
    var comment_string_min_tip = "@lang('Content length should be in between 2~255 character')";
    var item_confirm_string =  "@lang('Are you sure want to delete')";
    </script>
@stop

@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        <div class="col-md-12">
            <h1 class="uk-h1">@lang('Media Management')</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>@lang('You can edit your uploaded images or add new images here'): <a class="btn btn-primary" href="{{ URL('/') }}/media/new">@lang('Add')</a></p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="zo-item">
                @foreach ($items as $item)
                <div class="uk-panel-box" onclick="toggle_detail(this)">
                    <a href="javascript://void()" onclick="MediaRemove('{{ $item["id"] }}',this)" title="Delete Item" class="delete-item pull-right"><i class="uk-icon-times"></i></a>
                    <a href="{{ URL('/') }}/media/{{ $item['id'] }}/edit" title="Edit Item" class="pull-right"><i class="uk-icon-edit"></i></a>
                    <h3 class="uk-h5 pull-left">{{ $item['name'] }} (@lang('Media'))</h3>
                </div>
                <div class="preview">
                    @if($item["file"]!=null)
                        <img class="media" src="{{ URL('/') }}/media/images/{{ $item['file'] }}" alt="{{ $item['alias'] }}">
                    @endif
                    
                    <div class="form-inline" style="margin-top:10px;">
                        <div class="form-group" style="margin-left:5px;">
                            @if($item->Liked())
                            <i aria-hidden="true" cn="fa fa-thumbs-o-up" cp="fa fa-thumbs-up" class="fa fa-thumbs-up" cr="p" onclick="change_like('{{ $item['id'] }}',this)"></i>
                            @else
                            <i aria-hidden="true" cn="fa fa-thumbs-o-up" cp="fa fa-thumbs-up" class="fa fa-thumbs-o-up" cr="n" onclick="change_like('{{ $item['id'] }}',this)"></i>
                            @endif
                            <a><span class="badge">{{$item->Likes()}}</span></a>
                        </div>
                        <div class="form-group" style="margin-left:5px;">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                            <a><span class="badge">{{$item->shows}}</span></a>
                        </div>
                        <div class="form-group" style="margin-left:5px;">
                            <i class="fa fa-usd" aria-hidden="true"></i>
                            <a><span class="badge">{{$item->Sold()}}</span></a>
                        </div>
                        <div class="form-group" style="margin-left:5px;">
                            <span class="glyphicon glyphicon-pencil"></span>
                            <a><span class="badge">{{$item->Comment()->count()}}</span></a>
                        </div>
                    </div>


                    <div class="form-inline" style="margin-top:15px;margin-bottom:10px;">
                        <label style="margin-bottom: 10px;">@lang('KeyWords'):</label>
                        <div class="tags">
                            @foreach ($item->KeyWord()->get() as $word)
                            <a href="{{ URL('/') }}/search/{{$word['name']}}">
                                <div class="tag tag_no_remove label label-success ng-scope ng-binding">{{$word['name']}}</div>
                            </a>
                            @endforeach

                        </div>
                    </div>

                    @if(  $item['user_id']==Auth::user()->id  )
                    <div class="form-group" style="margin-top:10px;">
                        <label>@lang('Disable comment'):</label>
                        <div class="material-switch" style="margin-top:10px;margin-left:5px;">
                            <input id="someSwitchOptionWarning{{ $item['id'] }}" type="checkbox"
                            @if(  $item->disable_comment==1  )
                            checked="true"
                            @endif
                            onchange="CommentPermission('{{ $item['id'] }}',this)" />
                            <label for="someSwitchOptionWarning{{ $item['id'] }}" class="label-warning"></label>
                        </div>
                    </div>
                    @endif

                    @if (Auth::check())
                    <div style="margin-top:10px;">
                        <div class="form-group">
                            <label>@lang('Comment'):</label>
                            <textarea class="form-control comment_input" rows="5" style="margin-bottom: 5px;"></textarea>
                            <button class="uk-button uk-button-primary" onclick="CommentSubmit('{{ $item['id'] }}',this)">@lang('Submit')</button>
                        </div>
                    </div>
                    @endif
                    
                    <div class="panel-group comment">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse{{ $item['id'] }}">@lang('Comments')</a>
                                </h4>
                            </div>
                            <div id="collapse{{ $item['id'] }}" class="panel-collapse collapse in ">
                                <ul class="list-group">
                                    @foreach( $item->Comment()->OrderBy("created_at","DESC")->get() as $comment )
                                    <li class="list-group-item">
                                        <div>
                                            <span class="user">
                                                {{ $comment->User()->first()->name }}
                                            </span>
                                            <span class="issue_datetime">
                                                @php
                                                    echo date("F j, Y H:i:s",strtotime($comment["created_at"]));
                                                @endphp
                                            </span>
                                            @if(  $comment['user_id']==Auth::user()->id  )
                                            <span><a class="btn" onclick="CommentDelete('{{ $comment["id"] }}',this)">@lang('Delete')</a>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="content">{{ $comment['content'] }}</div>
                                        
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default" style="margin-top:20px;">
                <div class="panel-heading" style="background-color: #465d7a;color: white;">@lang('Balance'):</div>
                <div class="panel-body" style="padding:3px;">
                    <ul class="list-group">
                        <li class="list-group-item">@lang('Sold on this page'): 
                            <span class="badge">
                                @php
                                    echo $items->sum(function ($item) {
                                        return $item->Sold();
                                    });
                                @endphp
                            </span>
                        </li>
                        <li class="list-group-item">@lang('Total Sold'): 
                            <span class="badge">{{$total_sold}}</span>
                        </li>
                        <li class="list-group-item">@lang('Total Sold price'): 
                            <span class="pull-right">$ {{$total_balance}}</span>
                        </li>
                        <li class="list-group-item">@lang('Unpaid balance'): 
                            <span class="pull-right">$ {{$unpaied_balance}}</span>
                        </li> 

                    </ul>
                </div>
            </div>
            
        </div>
        <div class="col-md-8">
            <div id="pagination" class="pull-right">
                {!! $items->render() !!}
            </div>
        </div>
    </div>
    
    <!--</form>-->
</div>

<div style="display:none;" id="div_comment">
<!--
    <li class="list-group-item">
        <div>
            <span class="user">{{ Auth::user()->name }}</span>
            <span class="issue_datetime">{created_at}</span>
            <span><a class="btn" onclick="CommentDelete('{id}',this)">@lang('Delete')</a></span>
        </div>
        <div class="content">{content}</div>
    </li>
-->
</div>
@stop
@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/item_manage.css') }}">
	<link rel="stylesheet" href="{{ asset('css/keyword.css') }}">
@stop

@section('js')
    @parent

    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/comment.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/media.js') }}"></script>
	<script type="text/javascript">
	$(document).ready(function(){

    });
    var comment_string_min_tip = "@lang('Content length should be in between 2~255 character')";
    var item_confirm_string =  "@lang('Are you sure want to delete')";
    

	</script>
@stop

@section('content')
<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        <div class="col-md-12">
            @if( $vars["media"]->file!=null )
                <img class="media" src="{{ URL('/') }}/media/images/{{ $vars["media"]->file }}" alt="{{$vars["media"]->alias}}">
            @endif

            <div class="form-inline" style="margin-top:10px;margin-bottom:10px;">
                <div class="form-group" style="margin-left:5px;">
                    @if(!Auth::check())
                    <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                    @elseif( $vars["media"]->Liked()  )
                    <i aria-hidden="true" cn="fa fa-thumbs-o-up" cp="fa fa-thumbs-up" class="fa fa-thumbs-up" cr="p" onclick="change_like('{{ $vars["media"]->id }}',this)"></i>
                    @else
                    <i aria-hidden="true" cn="fa fa-thumbs-o-up" cp="fa fa-thumbs-up" class="fa fa-thumbs-o-up" cr="n" onclick="change_like('{{ $vars["media"]->id }}',this)"></i>
                    @endif
                    <a><span class="badge">{{ $vars["media"]->Likes() }}</span></a>
                </div>
                <div class="form-group" style="margin-left:5px;">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    <a><span class="badge">{{ $vars["media"]->shows }}</span></a>
                </div>
                <div class="form-group" style="margin-left:5px;">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                    <a><span class="badge">{{ $vars["media"]->Sold() }}</span></a>
                </div>
                <div class="form-group" style="margin-left:5px;">
                    <span class="glyphicon glyphicon-pencil"></span>
                    <a><span class="badge">{{ $vars["media"]->Comment()->count() }}</span></a>
                </div>
            </div>

            <div class="form-group tags">
                @foreach ( $vars["media"]->KeyWord() as $tag )
                <a href="{{ URL('/') }}/search/{{ $tag->name }}">
                    <div class="tag tag_no_remove label label-success ng-scope ng-binding">{{ $tag->name }}</div>
                </a>
                @endforeach
            </div>



            @if( Auth::check() && $vars["media"]->id==Auth::user()->id )
            <div class="form-group" style="margin-top:10px;">
                <label>@lang('Disable comment'):</label>
                <div class="material-switch" style="margin-top:10px;margin-left:5px;">
                    <input id="someSwitchOptionWarning{{ $vars["media"]->id }}" type="checkbox"
                    @if( $vars["media"]->PropertyDisableComment() == 'true' )
                    checked="true"
                    @endif
                    onchange="comment_permission_set('{{ $vars["media"]->id }}',this)" />
                    <label for="someSwitchOptionWarning{{ $vars["media"]->id }}" class="label-warning"></label>
                </div>
            </div>
            @endif




            @if( Auth::check() )
            <div style="margin-top:10px;">
                <div class="form-group">
                    <label>@lang('Comment'):</label>
                    <textarea class="form-control comment_input" rows="5" style="margin-bottom: 5px;"></textarea>
                    <button class="uk-button uk-button-primary" onclick="CommentSubmit('{{ $vars["media"]->id }}',this)">@lang('Submit')</button>
                </div>
            </div>
            @endif
            
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse{{ $vars["media"]->id }}">@lang('Comments')</a>
                        </h4>
                    </div>
                    <div id="collapse{{ $vars["media"]->id }}" class="panel-collapse collapse in ">
                        <ul class="list-group">
                            @foreach( $vars["media"]->Comment()->OrderBy("created_at","DESC")->get() as $comment )
                            <li class="list-group-item">
                                <div>
                                    <span class="user">{{ $comment->User()->first()->name }}</span>
                                    <span class="issue_datetime">
                                        @php
                                            echo date("F j, Y H:i:s",strtotime($comment->created_at));
                                        @endphp
                                    </span>
                                    @if( Auth::check() && $comment->user_id==Auth::user()->id )
                                    <span><a class="btn" onclick="CommentDelete('{{ $comment->id }}',this)">@lang('Delete')</a>
                                    @endif
                                    </span>
                                </div>
                                <div class="content">{{ $comment->content }}</div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div style="display:none;" id="div_comment">
<!--
    <li class="list-group-item">
        <div>
            <span class="user">
                @if( Auth::check() )
                {{ Auth::user()->name }}
                @endif
            </span>
            <span class="issue_datetime">{created_at}</span>
            <span><a class="btn" onclick="CommentDelete('{id}',this)">@lang('Delete')</a></span>
        </div>
        <div class="content">{content}</div>
    </li>
-->
</div>


@stop
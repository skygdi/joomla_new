@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/owl.carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user_channel.css') }}">
@stop

@section('js')
    @parent
    
    <script type="text/javascript" src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/user_channel.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        init_carousel();
    });
    </script>
@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">
    
    <div class="row" style="margin:0px;">
        <div class="col-md-4">
            <h3 class="uk-panel-title">@lang('profile')</h3>
            <dl class="dl-horizontal">
                <dt>@lang('user')</dt>
                <dd>
                    @if($user!=null)
                    {{ $user->name }}
                    @endif
                </dd>
            </dl>
        </div>
        <div class="col-md-8" style="text-align: center;">
            <div class="thumbnail">
                @if($user!=null)
                <img src="{{ URL('/') }}/user/avatar/{{$user->id}}"/ style="max-width:200px;">
                @endif
                @if($user->Profile!=null)
                <p>{{$user->Profile->description}}</p>
                @endif
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="owl-carousel owl-theme">
                @foreach ($medias as $media)
                    @php
                        $media->ImageCacheGet();
                    @endphp
                    <div class="item" style="width:100%;height:200px;position: relative;">
                        <a href="{{ URL('/') }}/postcard/{{$media->id}}/create">
                        <img 
                            class="owl-lazy" 
                            data-src="{{ $media->image_cache_url }}" 
                            original_src="media/images/{{ $media->file }}" 
                            alt="{{ $media['alias'] }}"/>

                        </a>
                    </div>
                @endforeach
                    
            </div>
        </div>
    </div>
    
</div>
@stop
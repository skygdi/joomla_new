@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/owl.carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/item_manage.css') }}">
@stop

@section('js')
    @parent
    
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/postcard.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('js/html2canvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/postcard-download.js') }}"></script>

    
    <script type="text/javascript">
    $(document).ready(function(){
    });
    var confirm_string = "@lang('Are you sure?')";
    </script>
@stop


@section('content')



<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        <div class="col-md-12">
            <h1 class="uk-h1">@lang('MY POSTCARDS')</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>@lang('You can edit your uploaded postcards or add new postcards and make order here'): <a class="btn btn-primary" href="{{ URL('/') }}/postcard/create">@lang('Add')</a></p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="zo-item">
                @foreach ($items as $item)
                <div class="uk-panel-box" onclick="toggle_detail(this)">
                    <a href="javascript://void()" onclick="PostCardRemove('{{ $item['id'] }}',this)" title="Delete Item" class="delete-item pull-right"><i class="uk-icon-times"></i></a>
                    <a href="{{ URL('/') }}/postcard/{{ $item['id'] }}/edit" title="Edit Item" class="pull-right"><i class="uk-icon-edit"></i></a>
                    <h3 class="uk-h5 pull-left">{{ $item['name'] }} ( @lang('POSTCARD') )</h3>
                </div>
                <div class="preview">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="uk-margin-remove uk-margin-bottom" style="color:#919191;">
                                <span class="issue_datetime">
                                    @php
                                        echo date( "l, j F Y" ,  strtotime($item['updated_at']) );
                                    @endphp
                                </span>
                            </h4>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-md-12">
                            {{ $item->Media()->first()->name }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @lang('Price'):
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="uk-text-success uk-margin-small">$ {{ $item->Price( Auth::id() ) }}</h3>
                        </div>
                    </div>
                    
                    @if (  $item->Media()->first()->file!="" )
                    <div class="row">
                        <div class="col-md-12">
                            <span class="pull-left">
                                <img class="media" src="{{ URL('/') }}/media/images/{{ $item->Media()->first()->file }}" alt="{{ $item['name'] }}" style="max-width: 720px;max-height: 266px;">
                            </span>
                            <span class="pull-left">

                                <div class="container_backside" iid="{{ $item['id'] }}">
                                    <img src="{{ URL('/') }}/images/postcard02.PNG" width="100%" for="backside_text_box"/>
                                    <div class="user_content container_backside_text">{{ $item['content'] }}　</div>
                                    @if (  $item->UserDefaultReturnAddress()!=null )
                                    <div class="user_address">
                                        {{ $item->UserDefaultReturnAddress()->name }}<BR>
                                        {{ $item->UserDefaultReturnAddress()->address1 }}<BR>
                                        {{ $item->UserDefaultReturnAddress()->address2 }}<BR>
                                        {{ $item->UserDefaultReturnAddress()->city }}<BR>
                                        {{ $item->UserDefaultReturnAddress()->state }}&nbsp;{{ $item->UserDefaultReturnAddress()->zipcode }}<BR>
                                        {{ $item->UserDefaultReturnAddress()->country }}<BR>
                                    </div>
                                    @endif
                                </div>
                                
                                <div class="new_image_container" tag="{{ $item['id'] }}"></div>

                            </span>
                        </div>
                    </div>

                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                            <button type="button" class="uk-button uk-button-small uk-button-primary" onclick="OBJ_ShoppingCartMini.AddToCart(this,'{{ $item['id'] }}')">
                                <span class="zx-x-incart-quant"></span>
                                    <!--class="uk-icon-shopping-cart"-->
                                    <i class="uk-icon-shopping-cart"></i>&nbsp;&nbsp;
                                <span class="zx-x-text">@lang('Add to Cart')</span>
                            </button>


                            @if ( $item->created_by==$item->Media()->first()->created_by )
                            <button type="button"
                               class="uk-button uk-button-small uk-button-primary" 
                               href="#"
                               onclick="GenerateDownload('{{ $item['id'] }}')">
                                @lang('Generate Download')
                            </button>
                            <a type="button" 
                               class="btn btn-default download_canvas_button" 
                               onclick="DownloadCanvas(this,'{{ $item['id'] }}','back.png')"
                               style="display:none;"
                               tag="{{ $item['id'] }}">
                                @lang('The Back')
                            </a>
                            <a type="button" 
                               class="btn btn-default download_front_button" 
                               style="display:none;"
                               tag="{{ $item['id'] }}"
                               href="{{ URL('/') }}/media/images/{{ $item->Media()->first()->file }}" 
                               download="{{ $item['id'] }}_front">
                                @lang('The Front')
                           </a>
                           @else
                           <div class="alert alert-warning download_front_button">@lang('Postcard cannot be downloaded because you are not the owner of the image.')</div>

                           
                           @endif
                        </div>
                    </div>
                    @endif
                    

                    

                </div>

                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-8">
            <div id="pagination" class="pull-right">
                {!! $items->render() !!}
            </div>

        </div>
    </div>
    
    <!--</form>-->
</div>

<!-- shooter DIV -->
<div class="div_container_backside"></div>



@stop
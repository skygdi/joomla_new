<DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="{{ url('/') }}" />
	<meta name="keywords" content="UrWorks custom cards, urworks cards, custom cards, greeting cards, postcards, custom printing" />
	<meta name="rights" content="© Copyright 2015 UrWorks, All Rights Reserved" />
	<meta name="description" content="Create custom Greeting Cards, Postcards or Audio Cards with your artwork or photos. Upload your media and design your card, or choose from public images in our gallery." />
	<meta name="generator" content="Joomla! - Open Source Content Management" />

	<title>Home</title>

	@section('css')
	<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.css') }}"/>
	<link rel="apple-touch-icon-precomposed" href="{{ asset('images/apple_touch_icon.png') }}"/>
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('urworks_old/css/common.css') }}"/>
	<link rel="stylesheet" href="{{ asset('urworks_old/css/theme.css') }}"/>
	<link rel="stylesheet" href="{{ asset('urworks_old/css/item_property.css') }}"/>
	<link rel="stylesheet" href="{{ asset('urworks_old/css/yoo_gusto_custom.css') }}"/>
	<link rel="stylesheet" href="{{ asset('css/shopping_cart_mini.css') }}"/>
	<link rel="stylesheet" href="{{ asset('css/main.css') }}"/>
	<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}"/>
	@show

	@section('js')
	<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/shopping_cart_mini.js') }}"></script>
	@show

</head>

<script type="text/javascript">
var system_url = "{{ url('/') }}";
var root_url = "{{ url('/') }}";

var JSON_ShoppingCartMini = {!! $vars["shoppingcart_json"] !!};
var OBJ_ShoppingCartMini;
$(document).ready(function () {
	OBJ_ShoppingCartMini = new ShoppingCartMini();
});
</script>

<body>

	@include('desktop.partial.shopping_cart_mini')
	@include('desktop.partial.top')
	<section style="background-color: #f0f0f1;">
		@yield('content')
	</section>
	@include('desktop.partial.bottom')

</body>
</html>
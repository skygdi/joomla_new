@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/kenwheeler/slick/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/kenwheeler/slick/slick-theme.css') }}">
@stop

@section('js')
    @parent
    

    <script type="text/javascript" src="{{ asset('js/media_more.js') }}"></script>
	<script type="text/javascript" src="{{ asset('vendor/kenwheeler/slick/slick.min.js') }}"></script>
	<script type="text/javascript">
	var load_each = {{ $vars["load_each"] }};
	var item_width=180;
	$(document).ready(function(){
	    //return;
	    $('#slidershow').slick({
	        autoplay: true,
	        autoplaySpeed:6000,
	        dots:true,
	        fade:true,
	        cssEase: 'linear',
	        speed: 1000
	    });
	});
	</script>
@stop

@section('content')
<div id="slidershow" style="">
    @foreach( $sliders as $slider )
	<div style="height:100vh;width:100%;background:url('{{ URL('/') }}/slider/{{ $slider["file"]  }}');background-size: cover;">
        <div class="uk-panel uk-panel-space uk-text-right uk-margin-large-top">
            {!! $slider["content"]  !!}
        </div>
    </div>
    @endforeach
</div>

<div class="container container_common" style="">
    <div class="row">
        <div class="col-md-6">
            <h3 class="uk-panel-title">@lang('Top Photos')</h3>
            <ul class="zoo-itempro-default zoo-default uk-list" id="item_list_click">
                
                @foreach ($vars["result"]["click"] as $r)
                <li class="uk-width-1-1 uk-width-small-1-3 uk-float-left" style="width:180px;">
                    <figure class="uk-overlay uk-overlay-hover">
                        <a href="{{ URL('/postcard') }}/{{ $r["id"] }}/create">
                        <img src="{{ $r->image_cache_url }}" alt="{{ $r["alias"] }}" width="180" height="180">
                        </a>
                        <figcaption class="uk-overlay-panel uk-padding-remove uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle">
                            <ul class="uk-list">
                                <li class="uk-margin-top"><a href="{{ URL('/media') }}/{{ $r["id"] }}"><i class="fa fa-eye" aria-hidden="true"></i>@lang('View')</a></li>
                                <li class="uk-margin-top"><a href="{{ URL('/postcard') }}/{{ $r["id"] }}/create"><i class="uk-icon-plus"></i>@lang('Postcard')</a></li>
                                <li class="uk-margin-top"><a href="{{ URL('/user') }}/{{ $r["user_id"] }}/channel"><i class="fa fa-user" aria-hidden="true"></i>{{ $r->User()->first()["name"] }}</a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="col-md-6">
            <h3 class="uk-panel-title">@lang('Newest')</h3>
            <ul class="zoo-itempro-default zoo-default uk-list" id="item_list_newest">
                @foreach ($vars["result"]["newest"] as $r)
                
                <li class="uk-width-1-1 uk-width-small-1-3 uk-float-left" style="width:180px;">
                    <figure class="uk-overlay uk-overlay-hover">
                        <a href="{{ URL('/postcard') }}/{{ $r["id"] }}/create">
                        <img src="{{ $r->image_cache_url }}" alt="{{ $r["alias"] }}" width="180" height="180">
                        </a>
                        <figcaption class="uk-overlay-panel uk-padding-remove uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle">
                            <ul class="uk-list">
                                <li class="uk-margin-top"><a href="{{ URL('/media') }}/{{ $r["id"] }}"><i class="fa fa-eye" aria-hidden="true"></i>@lang('View')</a></li>
                                <li class="uk-margin-top"><a href="{{ URL('/postcard') }}/{{ $r["id"] }}/create"><i class="uk-icon-plus"></i>@lang('Postcard')</a></li>
                                <li class="uk-margin-top"><a href="{{ URL('/user') }}/{{ $r["user_id"] }}/channel"><i class="fa fa-user" aria-hidden="true"></i>{{ $r->User()->first()["name"] }}</a></li>
                            </ul>
                        </figcaption>
                    </figure>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row" style="margin-top:15px;">
        <div class="col-md-12 text-center"><a class="btn btn-info" onclick="obj_IM.Refresh()">@lang('Load More')....</a></div>
    </div>
</div>


<div id="model_item" style="display:none;width:0px;height:0px">
<!--
    <li class="uk-width-1-1 uk-width-small-1-3 uk-float-left" style="width:{item_width}px;">
        <figure class="uk-overlay uk-overlay-hover">
            <a href="{{ URL('/postcard') }}/{id}/create">
                <img src="{image_cache_url}" alt="{alias}" width="{item_width}" height="{item_width}">
            </a>
            <figcaption class="uk-overlay-panel uk-padding-remove uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle">
                <ul class="uk-list">
                    <li class="uk-margin-top"><a href="{{ URL('/media') }}/{id}"><i class="fa fa-eye" aria-hidden="true"></i>@lang('View')</a></li>
                    <li class="uk-margin-top"><a href="{{ URL('/postcard') }}/{id}/create"><i class="uk-icon-plus"></i>@lang('Postcard')</a></li>
                    <li class="uk-margin-top"><a href="{{ URL('/user') }}/{user_id}/channel"><i class="fa fa-user" aria-hidden="true"></i>{name}</a></li>
                </ul>
            </figcaption>
        </figure>
    </li>
-->
</div>

@stop
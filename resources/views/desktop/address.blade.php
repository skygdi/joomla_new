@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<style>
.uk-list-line > li:nth-child(n+2) {
    margin-top: 5px;
    padding-top: 5px;
    border-top: 2px solid rgba(77, 77, 77, 0.89);
}
.dropdown-toggle{
    height: 34px;
    border-radius: 0px;
    border: 1px solid #ccc;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
#state_area .bootstrap-select{
    width: 220px;
}
#country_area .dropdown-toggle{
    height: 30px;
    border: 1px solid #ccc;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
</style>
@stop

@section('js')
    @parent

    <script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.number.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.pulse.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('js/shopping_cart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart_address_public.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart_address_billing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/shopping_cart_address_shipping.js') }}"></script>

    <script type="text/javascript" src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>

    <script type="text/javascript">
    var Obj_ShoppingCart;
    var msg_validation_name = "@lang('Please enter the recipient name')";
    var msg_validation_email = "@lang('Please enter a valid email address')";
    var msg_validation_state = "@lang('Please type or select a state')";
    var msg_validation_address1 = "@lang('Please enter a valid address')";
    var msg_validation_zipcode = "@lang('Please enter a valid zipcode, digits only')";
    var msg_validation_country = "@lang('Please select a country')";
    var msg_order_empty = "@lang('Please add item first')";
    var msg_confirm = "@lang('Are you sure?')";

    
    </script>
@stop


@section('content')

<div class="container container_common" style="padding-top:130px !important;">

    <div class="row" style="margin-bottom: 40px;">
        <div class="col-md-6">
            <div class="uk-panel uk-panel-box uk-panel-header zx-zoocart-checkout-fieldset" style="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="uk-text-primary uk-margin-bottom zx-zoocart-checkout-fieldset-title">@lang('address.BILLING')</div>
                    </div>
                </div>

                <div style="margin-top:10px;" id="billing_address_list">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="zx-zoocart-address-manager-rows uk-list uk-list-line">
                                @foreach ($BA as $address)
                                <li data-id="{{ $address->id }}">
                                    <!-- title -->
                                    <strong class="zx-zoocart-address-manager-row-title uk-float-left">{{ $address->name }}</strong>

                                    <!-- preview -->
                                    <div style="display: none;width:0px;height: 0px;">
                                        {{ $address->name }}<BR>
                                        @if( $address->phone!="" )
                                        {{ $address->phone }},
                                        @endif
                                        <a href="mailto:{{ $address->email }}">{{ $address->email }}</a>
                                        <input type="hidden" name="id" value="{{ $address->id }}">
                                    </div>

                                    <!-- btns -->
                                    <div class="uk-button-group uk-float-right">

                                        <!-- edit -->
                                        <button type="button" class="skygdi-button-mini skygdi-button-first tooltip-custom" onclick="edit_address_show('{{ $address->id }}',this,'billing','ModalEditBillingAddress')" data-toggle="tooltip" title="@lang('Edit this Address')">
                                            <i class="uk-icon-pencil"></i>
                                        </button>

                                        <!-- return address  -->
                                        @if( $address->display_as_return==1 )
                                        <button type="button" class="change_return skygdi-button-mini skygdi-button-middle" onclick="ChangeReturnAddress('{{ $address->id }}',this,'billing')" title="">
                                            <i class="uk-icon-dot-circle-o"></i>
                                        </button>
                                        @else
                                        <button type="button" class="change_return skygdi-button-mini skygdi-button-middle" onclick="ChangeReturnAddress('{{ $address->id }}',this,'billing')" title="@lang('Display as return address')">
                                            <i class="uk-icon-circle-o"></i>
                                        </button>
                                        @endif

                                        <!-- default  title="Set this Address as default" -->
                                        @if( $address->is_default==1 )
                                        <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{ $address->id }}',this,'billing')" disabled="">
                                            <i class="uk-icon-dot-circle-o"></i>
                                        </button>
                                        @else
                                        <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{ $address->id }}',this,'billing')" title="@lang('Set this Address as default')">
                                            <i class="uk-icon-circle-o"></i>
                                        </button>
                                        @endif

                                        <button type="button" class="skygdi-button-mini skygdi-button-last tooltip-custom" title="@lang('Remove this Address')" data-toggle="tooltip" onclick="remove_address2(this,'{{ $address->id }}');">
                                            <i class="uk-icon-times"></i>
                                        </button>

                                    </div>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="zx-zoocart-address-manager-row-add uk-button uk-button-mini uk-float-right" style="display: block;" data-toggle="modal" data-target="#ModalNewBillingAddress">@lang('<i class="uk-icon-plus"></i>&nbsp;&nbsp;Address')
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-6">
            <div class="uk-panel uk-panel-box uk-panel-header zx-zoocart-checkout-fieldset" style="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="uk-text-primary uk-margin-bottom zx-zoocart-checkout-fieldset-title">@lang('address.SHIPPING')</div>
                    </div>
                </div>


                <div style="margin-top:10px;" id="shipping_address_list">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="zx-zoocart-address-manager-rows uk-list uk-list-line">
                                @foreach ($SA as $address)
                                <li data-id="{{ $address->id }}">
                                    <!-- title -->
                                    <strong class="zx-zoocart-address-manager-row-title uk-float-left">{{ $address->name }}</strong>

                                    <!-- preview -->
                                    <div style="display: none;width:0px;height: 0px;">
                                        {{ $address->name }}<BR>
                                        @if( $address->phone!="" )
                                        {{ $address->phone }},
                                        @endif
                                        <a href="mailto:{{ $address->email }}">{{ $address->email }}</a>
                                        <input type="hidden" name="id" value="{{ $address->id }}">
                                        <input type="hidden" name="country" value="{{ $address->country }}">
                                    </div>

                                    <!-- btns -->
                                    <div class="uk-button-group uk-float-right">

                                        <!-- edit -->
                                        <button type="button" class="skygdi-button-mini skygdi-button-first tooltip-custom" onclick="edit_address_show('{{ $address->id }}',this,'shipping','ModalEditBillingAddress')" data-toggle="tooltip" title="@lang('Edit this Address')">
                                            <i class="uk-icon-pencil"></i>
                                        </button>

                                        <!-- default  title="Set this Address as default" -->
                                        @if( $address->is_default==1 )
                                        <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{ $address->id }}',this,'shipping')" disabled="">
                                            <i class="uk-icon-dot-circle-o"></i>
                                        </button>
                                        @else
                                        <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{ $address->id }}',this,'shipping')" title="@lang('Set this Address as default')">
                                            <i class="uk-icon-circle-o"></i>
                                        </button>
                                        @endif

                                        <button type="button" class="skygdi-button-mini skygdi-button-last tooltip-custom" title="@lang('Remove this Address')" data-toggle="tooltip" onclick="remove_address2(this,'{{ $address->id }}');">
                                            <i class="uk-icon-times"></i>
                                        </button>

                                    </div>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="zx-zoocart-address-manager-row-add uk-button uk-button-mini uk-float-right" style="display: block;" data-toggle="modal" data-target="#ModalNewShippingAddress">@lang('<i class="uk-icon-plus"></i>&nbsp;&nbsp;Address')
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
</script>

@include('desktop.partial.shopping_cart.modal_billing_address_new')
@include('desktop.partial.shopping_cart.modal_billing_address_edit')

@include('desktop.partial.shopping_cart.modal_shipping_address_new')
@include('desktop.partial.shopping_cart.modal_shipping_address_edit')



@stop
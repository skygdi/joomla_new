@extends('desktop.layouts.main')

@section('css')
    @parent

@stop

@section('js')
    @parent

@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        <div class="col-md-12">
            <div class="uk-panel uk-panel-box uk-panel-box-secondary data-uk-scrollspy">
                <div class="uk-scrollspy-init-inview uk-scrollspy-inview">
                    <div class="uk-margin">
                        <p>User Agreement</p>
<p>Welcome to Urworks.com. Urworks provide website features and other products and services to you when you visit or shop at Urworks.com, use Urworks products or services, use Urworks applications for mobile, or use software provided by Urworks in connection with any of the foregoing (collectively, "Urworks Services"). Urworks provides the Urworks Services subject to the following conditions.</p>
<p>By using Urworks Services, you agree to these conditions. Please read them carefully.</p>
<p>We offer a wide range of Urworks Services, and sometimes additional terms may apply. If these Conditions of Use are inconsistent with the Service Terms, those Service Terms will control.</p>
</div>
                </div>
            </div>


        </div>
    </div>

</div>
@stop
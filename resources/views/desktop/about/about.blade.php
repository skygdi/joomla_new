@extends('desktop.layouts.main')

@section('css')
    @parent

@stop

@section('js')
    @parent

@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        <div class="col-md-12">
            <div class="uk-panel uk-panel-box uk-panel-box-secondary data-uk-scrollspy">
                <div class="uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-scale-down">
                    <p style="text-indent: 2em;">Urworks ultimately provides you with the one stop service sending greetings to family and friends -- By making a creative postcard with a picture you just took, your heartwarming greeting will timely be delivered. We also offer independent Artists platform with freedom to present and promote their great artworks, develop great opportunities to allocate their market values.</p>
                </div>
            </div>


        </div>
    </div>

</div>
@stop
@extends('desktop.layouts.main')

@section('css')
    @parent

@stop

@section('js')
    @parent

@stop


@section('content')
<div class="container container_common" style="padding-top:130px !important;">
    <div class="row">
        
        <div class="col-md-8">
            <form action="{{ URL('/') }}/about/contact" name="form_contact" method="post">
            @if( $errors->count()>0 )
                <div class="alert alert-danger" role="alert">
                    <ul>
                    @foreach ( $errors->all() as $messsage)
                        <li>{{ $messsage }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            @if( isset($state) && $state=="success" )
                <div class="alert alert-success" role="alert">
                    <strong>Thank you,</strong> We might contact you soon.
                </div>
            @endif

                

            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="uk-width-1-1 uk-margin-top uk-margin-bottom"><h3>@lang('SEND US A MESSAGE')</h3></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                        <label for="first_name">@lang('First Name'):</label>
                        <input type="text" class="form-control uk-form" value="@if( old('first_name')!="" ){{old('first_name')}}@endif" name="first_name" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="last_name">@lang('Last Name'):</label>
                        <input type="text" class="form-control uk-form" name="last_name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">@lang('E-mail'):</label>
                        <input type="email" class="form-control uk-form" value="@if( old('email')!="" ){{old('email')}}@endif" name="email" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="phone">@lang('Phone'):</label>
                        <input type="text" class="form-control uk-form" name="phone">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                        <label for="message">@lang('Message'):</label>
                        <textarea cols="50" rows="5" name="message" class="form-control uk-form" style="border-radius: 40px; z-index: auto; position: relative; line-height: 24px; font-size: 16px; transition: none; background: transparent !important;" required>@if( old('message')!="" ){{old('message')}}@endif</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <button type="submit" name="Submit" class="uk-button uk-button-primary uk-button-large">@lang('Send')</button>
                </div>
                <div class="col-md-8">
                    {!! Recaptcha::render() !!}
                </div>

            </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="uk-panel uk-panel-box uk-panel-box-secondary data-uk-scrollspy">
                <div class="uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-scale-down">
                    <h3>@lang('CONTACT US')</h3>
                    <ul class="uk-list uk-list-line">
                        <li>@lang('Toll Free')</li>
                        <li>Fax</li>
                    </ul>
                    <ul class="uk-list uk-list-line">
                        <li>
                            <h4 style="color:black;">UrWorks Inc</h4>
                        </li>
                        <li>2492 Autumnvale Dr</li>
                        <li>San Jose, CA 95131</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

</div>
@stop
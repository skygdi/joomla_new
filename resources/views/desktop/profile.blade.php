@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
@stop

@section('js')
    @parent
    
    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/profile.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        @if( @$vars["result_flag"]=="true" )
            swal({
                title: "",
                text: "{{@$vars["result_string"]}}",
                timer: 2000,
                type: 'success'
            });
        @else
            @if( @$vars["result_flag"]!=null )
                swal({
                    title: "",
                    text: "{{@$vars["result_string"]}}",
                    timer: 5000
                });
            @endif
        @endif

        @if( count($errors)>0 )
        $("#div_edit_area").collapse("show");
        @endif
    });
    </script>
@stop


@section('content')
<div class="container container_common" style="margin-top:81px;">
    <div class="row">
        <div class="col-md-10">
            
            <div class="row">
                <div class="col-md-12">

                    <legend>@lang('Profile')
                        <a class="pull-right" data-toggle="collapse" data-target="#div_edit_area">
                            <span style="font-size:15px;margin-right:10px;font-weight: bold;">@lang('Edit')</span>
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                    </legend>
                </div>
            </div>
            <div class="row collapse" id="div_edit_area">
                <div class="col-md-12">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="name" value="@if( old('name')!="" ){{old('name')}}@else{{Auth::User()->name}}@endif" placeholder="@lang('Enter') @lang('Name')" required>
                            </div>
                        </div>
                        @if ($errors->has('name'))
                            <label class="control-label">{{ $errors->first('name') }}</label>
                        @endif
                    </div>

                    <div class="form-group row {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password" value="" placeholder="@lang('Enter') @lang('Password')">
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <label class="control-label">{{ $errors->first('password') }}</label>
                        @endif
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password_confirmation" value="" placeholder="@lang('Enter') @lang('Confirm Password')">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('email') ? 'has-error' : '' }}">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="email" value="@if( old('email')!="" ){{old('email')}}@else{{Auth::User()->email}}@endif" placeholder="@lang('Enter') @lang('Email')">
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <label class="control-label">{{ $errors->first('email') }}</label>
                        @endif
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="description" value="{{@Auth::User()->Profile->description}}" placeholder="@lang('Enter') @lang('Description')">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('paypal') ? 'has-error' : '' }}">
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-paypal" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="paypal" value="@if( old('paypal')!="" ){{old('paypal')}}@else{{@Auth::User()->Profile->paypal}}@endif" placeholder="@lang('Enter') @lang('Paypal')">
                            </div>
                        </div>
                        @if ($errors->has('paypal'))
                            <label class="control-label">{{ $errors->first('paypal') }}</label>
                        @endif
                    </div>

                    <div class="form-group row {{ $errors->has('avatar_file') ? 'has-error' : '' }}">
                        <div class="col-md-4">
                            <div class="input-group btn-file">
                                <span class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></span>
                                <input type="file" name="avatar_file" class="filestyle" data-buttonName="btn-primary" data-buttonText="@lang('CHOOSE IMAGE')">
                            </div>
                        </div>
                        @if ($errors->has('avatar_file'))
                            <label class="control-label">{{ $errors->first('avatar_file') }}</label>
                        @endif
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-primary">@lang('SUBMIT')</button>
                            <button type="button" class="btn btn-default" style="margin-left:40px;" data-toggle="collapse" data-target="#div_edit_area">@lang('CANCEL')</button>
                        </div>
                    </div>

                    </form>
                </div>
            </div>



            <div class="row">
                <div class="col-md-7">
                    <dl class="dl-horizontal">
                        <dt>@lang('Name')</dt>
                        <dd>{{Auth::User()->name}}</dd>
                        <dt>@lang('Username')</dt>
                        <dd>{{Auth::User()->username}}</dd>
                        <dt>@lang('Registered Date')</dt>
                        <dd>
                            @php
                                echo date("D, j M Y", strtotime(Auth::User()->registerDate) );
                            @endphp
                        </dd>
                        <dt>@lang('Last Visited Date')</dt>
                        <dd>
                            @php
                                echo date("D, j M Y", strtotime(Auth::User()->lastvisitDate) );
                            @endphp
                        </dd>
                    </dl>
                </div>
                <div class="col-md-5">
                    <div class="thumbnail">
                        @if( @Auth::User()->Profile->avatar!="" )
                        <img src="{{ URL('/') }}/user/avatar"/ style="max-width:200px;">
                        @endif
                        @if( @Auth::User()->Profile->description!="" )
                        <p>{{@Auth::User()->Profile->description}}</p>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>
@stop
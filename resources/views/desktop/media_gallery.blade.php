@extends('desktop.layouts.main')

@section('css')
    @parent
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/gallery.js') }}"></script>
    <script type="text/javascript">
       
    </script>
@stop

@section('content')

<div class="container container_common" style="padding-top:110px !important;">
    <div class="row">
        <div class="col-md-12">
            <h3 class="uk-panel-title">@lang('Top Photos')</h3>
            <ul class="zoo-itempro-default zoo-default uk-list" id="item_list_click">
                
            </ul>
        </div>
    </div>
    <div class="row" style="margin-top:15px;">
        <div class="col-md-12 text-center"><a class="btn btn-info" onclick="data_load()">@lang('Load More')....</a></div>
    </div>
</div>



<div id="model_item" style="display:none;width:0px;height:0px">
<!--
    <li class="uk-width-1-1 uk-width-small-1-3 uk-float-left" style="width:280px;">
        <figure class="uk-overlay uk-overlay-hover">
            <a href="{{ URL('/postcard') }}/{id}/create">
                <img src="{image_cache_url}" alt="{alias}" width="280px" height="280px">
            </a>
            <figcaption class="uk-overlay-panel uk-padding-remove uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle">
                <ul class="uk-list">
                    <li class="uk-margin-top"><a href="{{ URL('/media') }}/{id}"><i class="fa fa-eye" aria-hidden="true"></i>@lang('View')</a></li>
                    <li class="uk-margin-top"><a href="{{ URL('/postcard') }}/{id}/create"><i class="uk-icon-plus"></i>@lang('Postcard')</a></li>
                    <li class="uk-margin-top"><a href="{{ URL('/user') }}/{user_id}/channel"><i class="fa fa-user" aria-hidden="true"></i>{name}</a></li>
                </ul>
            </figcaption>
        </figure>
    </li>

-->
</div>

@stop
<!-- Modal -->
<div class="modal fade" id="ModalShoppingCartMini" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">@lang('YOUR CART')</h4>
			</div>
			<div class="modal-body">
				<div id="shopping_cart_mini_content">
					<table id="shopping_cart_mini_table" class="uk-table">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>@lang('Quantity')</th>
								<th>@lang('Price')</th>
								<th>@lang('Total')</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					<div class="total"></div>
					<div style="text-align: center;margin-bottom: 40px;">
						<a class="uk-button uk-button-primary uk-margin-top" href="{{ URL('/shoppingcart') }}">@lang('Proceed to Checkout')</a>
					</div>
				</div>
				<div class="loading">
					<img src="{{ URL('/') }}/public/images/loading.svg" style="margin-top:20px;">
				</div>
				<div class="shopping_cart_empty">
					@lang('Your cart is empty')
				</div>
			</div>

		</div>

	</div>
</div>

<table id="model_shopping_cart_mini" style="display:none;">
	<tbody>
		<tr data-id="112">
			<td style="text-align: center;"><a href="javascript://void(0)" onclick="OBJ_ShoppingCartMini.Remove(this);" class="zx-zoocart-minicart-row-remove"><i class="uk-icon-times"></i></a></td>
			<td style="text-align: left;"></td>
			<td style="padding-top:0px;">
				<input type="number" min="1" class="uk-form-width-mini uk-form-small uk-form-blank uk-text-center" onchange="OBJ_ShoppingCartMini.ChangeQuantity(this);" value="1" style="padding-top: 0px;height: 33px;">
			</td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
</table>
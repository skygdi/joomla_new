<nav class="navbar-fixed-top hidden-xs" style="">
	<div class="" style="background-color: #eee;border-bottom: 2px solid #465d7a;min-height: 30px;">
		<div class="container">
			<div class="row" style="">
				<div class="col-md-4" style="padding-left: 40px;padding-top:4px;">
					<a class="pull-left" style="" data-toggle="modal" data-target="#ModalShoppingCartMini">
						<strong id="shopping_cart_sumarry_count">{{ $vars["shoppingcart_item_count"] }}</strong> Items	 | 
						<strong id="shopping_cart_sumarry_total">$
							@php
							echo number_format($vars["shoppingcart_item_total"],2, '.', ',');
							@endphp
							
						</strong>
					</a>

					<div class="pull-left" style="margin-left: 20px;">
						<a href="{{ URL('/shoppingcart') }}">Checkout<i class="uk-icon-shopping-cart"></i></a>
					</div>

					<div class="pull-left" style="margin-left: 20px;">
						@if( Auth::check() )
						<a href="{{ URL('/user') }}/{{Auth::id()}}/channel">
							{{Auth::user()->name}}
						</a>
						@endif			
					</div>


				</div>
				<div class="col-md-8">
					<div class="pull-right">
						<select class="form-control input-sm" style="margin-bottom:0px;" onchange="switch_language(this.value)">
							@if( $vars["locale"]=="en" )
								<option value="en" SELECTED>English</option>
								<option value="cn">中文</option>
							@else
								<option value="en">English</option>
								<option value="cn" SELECTED>中文</option>
							@endif
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="navbar-inverse" style="opacity: 0.7;min-height: 50px;">
		<div class="container">
			<div class="row" style="padding-top:8px;">
				<div class="col-md-3" style="padding-left: 40px;">
					<a href="{{ URL('/') }}">
						<img src="{{ URL('/images/logowhite.png') }}" alt="UR Works" style="max-height:35px;">
					</a>
				</div>

				<div class="col-md-9">
					<ul class="pull-right uk-navbar-nav">
						<li><a href="{{ URL('/') }}">@lang('Home')</a></li>
						<li><a href="{{ URL('/media/gallery') }}">@lang('Gallery')</a></li>
						<li><a href="{{ URL('/postcard') }}">@lang('Cards')</a></li>
						<li><a href="{{ URL('/about') }}">@lang('About')</a></li>
						<li><a href="{{ URL('/about/contact') }}">@lang('Contact')</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div>
		<div class="tm-search">
		    <form class="uk-search" action="{{ URL('/search') }}" method="post" >
		    	{!! csrf_field() !!}
		        <input class="uk-search-field" type="search" name="word" placeholder="@lang('search')..." autocomplete="off">
		    </form>
		</div>

	</div>
</nav>
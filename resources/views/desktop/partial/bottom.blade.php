<footer>
	<div class="uk-block uk-block-muted hidden-xs">
	    <div class="uk-container uk-container-center" id="bottom_menu">
	        <section class="tm-bottom-c uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
	            <div class="uk-width-1-1 uk-width-medium-1-3">
	                <div class="uk-panel" style="min-height: 165px;">
	                    <h3 class="uk-panel-title">@lang('Account')</h3>
	                    <ul class="uk-nav uk-nav-parent-icon uk-nav-side">
	                    	@if( !Auth::check() )
	                    	<li><a href="{{ URL('/login') }}">@lang('Login or Logout')</a></li>
	                        <li><a href="{{ URL('/register') }}">@lang('Create an Account')</a></li>
	                        @else
	                        <li><a href="{{ URL('/user/logout_show') }}">@lang('Login or Logout')</a></li>
	                        @endif
	                        @if( Auth::check() )
	                        <li><a href="{{ URL('/user/profile') }}">@lang('Profile')</a></li>
	                        @endif
	                        
	                    </ul>
	                </div>
	            </div>

	            <div class="uk-width-1-1 uk-width-medium-1-3">
	                <div class="uk-panel" style="min-height: 165px;">
	                    <h3 class="uk-panel-title">@lang('My Works')</h3>
	                    <ul class="uk-nav uk-nav-parent-icon uk-nav-side">
	                        <li><a href="{{ URL('/media/create') }}">@lang('Step 1 Upload')</a></li>
	                        <li><a href="{{ URL('/media') }}">@lang('MEDIA MANAGEMENT')</a></li>
	                        <li><a href="{{ URL('/postcard/create') }}">@lang('Step 2 Postcard')</a></li>
	                        <li><a href="{{ URL('/postcard') }}">@lang('Step 3 Order')</a></li>
	                    </ul>
	                </div>
	            </div>

	            <div class="uk-width-1-1 uk-width-medium-1-3">
	                <div class="uk-panel _menu" style="min-height: 165px;">
	                    <h3 class="uk-panel-title">@lang('menu_bottom.Orders')</h3>
	                    <ul class="uk-nav uk-nav-parent-icon uk-nav-side">
	                        <li><a href="{{ URL('/shoppingcart') }}">@lang('Shopping Cart')</a></li>
	                        <li><a href="{{ URL('/order') }}">@lang('Orders')</a></li>
	                        <li><a href="{{ URL('/address') }}">@lang('Addresses')</a></li>
	                    </ul>
	                </div>
	            </div>

	            
	        </section>
	    </div>
	</div>

	<div id="tm-footer" class="tm-block-footer">
        <div class="uk-container uk-container-center">
            <footer class="tm-footer tm-link-muted">
            	<a id="tm-anchor-bottom" class="tm-totop-scroller" data-uk-smooth-scroll="" href="{{ url()->current() }}#"></a>
                <div class="uk-panel">
                    <ul class="uk-subnav uk-subnav-line uk-flex-center">
                        <li><a href="{{ URL('/about/privacy') }}">@lang('Privacy Policy')</a></li>
                        <li><a href="{{ URL('/about/terms') }}">@lang('Terms of Use')</a></li>
                        <li><a href="{{ URL('/about/tos') }}">@lang('TOS')</a></li>
                    </ul>
                </div>
                <div class="uk-panel">© Copyright 2015 UrWorks, @lang('All Rights Reserved') ^L<br></div>
                <div id="qr" style=""><img src="{{ URL('/') }}/images/qr.png"></div>
            </footer>
            
        </div>
    </div>

	<!-- for menu default select -->
	<script type="text/javascript">
	$(document).ready(function () {
	    $("#bottom_menu").find("a[route='{{ URL('/') }}']").parent().addClass("uk-active");
	});
	</script>


</footer>
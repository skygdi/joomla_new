<div class="uk-panel uk-panel-box uk-panel-header zx-zoocart-checkout-fieldset" style="">
    <div class="row">
        <div class="col-md-12">
            <div class="uk-text-primary uk-margin-bottom uk-text-center zx-zoocart-checkout-fieldset-title" id="checkout_text">@lang('Is important that you <span class="uk-text-bold">read &amp; agree</span> to the terms')</div>
        </div>
    </div>
    <div class="row" style="text-align: center;">
        <div class="col-md-12" style="font-weight: 500;font-size: 16px;" id="billing_address_current">
            <a href="{{ URL('/about/tos') }}" target="_blank">@lang('Terms and Conditions')</a>
            <i class="uk-icon-external-link-square"></i>
        </div>
    </div>
    <div class="row" style="text-align: center;">
        <div class="col-md-12">
            <button type="button" class="uk-button uk-button-mini" onclick="agreeing(this)" id="checking_button">
            <i class="uk-icon-square-o"></i>
            <!-- uk-icon-check-square-o -->
            @lang('I agree to the Terms and Conditions')     </button>
        </div>
    </div>
</div>


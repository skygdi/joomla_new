<div class="uk-panel uk-panel-box uk-panel-header zx-zoocart-checkout-fieldset" style="">
    <div class="row">
        <div class="col-md-12">
            <div class="uk-text-primary uk-margin-bottom uk-text-center zx-zoocart-checkout-fieldset-title">@lang('Input or choose the <span class="uk-text-bold">billing</span> Address')</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9" style="font-weight: 500;font-size: 16px;" id="billing_address_current">
            {{$BAD->name}}<BR>
            @if( !empty($BAD->phone) )
            {{$BAD->phone}},
            @endif
            <a href="mailto:{{$BAD->email}}">{{$BAD->email}}</a>
            <input type="hidden" name="id" value="{{$BAD->id}}">
        </div>
        <div class="col-md-3">
            <a href="javascript://void()" onclick="button_change_address(this,'billing_address_list')" class="uk-button uk-button-mini tooltip-custom" style="width: 86px;" data-toggle="tooltip" title="@lang('Change the current Address')">
                <span>@lang('CHANGE')</span>
                <span style="display: none;"><i class="uk-icon-chevron-up" style="line-height: 25px;"></i></span>
            </a>
        </div>
    </div>

    <div style="margin-top:10px;display: none;" id="billing_address_list">
        <div class="row">
            <div class="col-md-12">
                <ul class="zx-zoocart-address-manager-rows uk-list uk-list-line">
                    @foreach( $BA as $address )
                    <li data-id="{{$address->id}}">
                        <!-- title -->
                        <strong class="zx-zoocart-address-manager-row-title uk-float-left">{{$address->name}}</strong>

                        <!-- preview -->
                        <div style="display: none;width:0px;height: 0px;">
                            {{$address->name}}<BR>
                            @if( !empty($address->phone) )
                            {{$address->phone}},
                            @endif
                            <a href="mailto:{{$address->email}}">{{$address->email}}</a>
                            <input type="hidden" name="id" value="{{$address->id}}">
                        </div>

                        <!-- btns -->
                        <div class="uk-button-group uk-float-right">

                            <!-- edit -->
                            <button type="button" class="skygdi-button-mini skygdi-button-first tooltip-custom" onclick="edit_address_show('{{$address->id}}',this,'billing','ModalEditBillingAddress')" data-toggle="tooltip" title="@lang('Edit this Address')">
                                <i class="uk-icon-pencil"></i>
                            </button>

                            <!-- return address  -->
                            @if( $address->display_as_return==1 )
                            <button type="button" class="change_return skygdi-button-mini skygdi-button-middle" onclick="ChangeReturnAddress('{{$address->id}}',this,'billing')" disabled="" title="">
                                <i class="uk-icon-dot-circle-o"></i>
                            </button>
                            @else
                            <button type="button" class="change_return skygdi-button-mini skygdi-button-middle" onclick="ChangeReturnAddress('{{$address->id}}',this,'billing')" title="@lang('Display as return address')">
                                <i class="uk-icon-circle-o"></i>
                            </button>
                            @endif

                            <!-- default  -->
                            @if( $address->is_default==1 )
                            <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{$address->id}}',this,'billing')" disabled="" title="">
                                <i class="uk-icon-dot-circle-o"></i>
                            </button>
                            @else
                            <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{$address->id}}',this,'billing')" title="@lang('Set this Address as default')">
                                <i class="uk-icon-circle-o"></i>
                            </button>
                            @endif

                            <!-- choose -->
                            <button type="button" class="skygdi-button-mini skygdi-button-last tooltip-custom" title="@lang('Choose this Address')" data-toggle="tooltip" onclick="choose_billing_address(this);">
                                <i class="uk-icon-check"></i>
                            </button>

                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="zx-zoocart-address-manager-row-add uk-button uk-button-mini uk-float-right" style="display: block;" data-toggle="modal" data-target="#ModalNewBillingAddress">@lang('<i class="uk-icon-plus"></i>&nbsp;&nbsp;Address')
                </button>
            </div>
        </div>
    </div>

</div>
<div class="uk-panel uk-panel-box uk-panel-header zx-zoocart-checkout-fieldset" style="height:150px;">
    <div class="row">
        <div class="col-md-12">
            <div class="uk-text-primary uk-margin-bottom uk-text-center zx-zoocart-checkout-fieldset-title">@lang('Available <span class="uk-text-bold">shipping</span> methods')</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="font-weight: 500;font-size: 16px;    text-align: center;">
            @lang('Standard Shipping') - $<span class="shipping_fee">0.00</span>
        </div>
    </div>
</div>
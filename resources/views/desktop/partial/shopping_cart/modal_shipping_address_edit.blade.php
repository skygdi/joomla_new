<!-- Modal -->
<div class="modal fade" id="ModalEditShippingAddress" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form id="formEditShippingAddress" method="POST" action="">
            <input type="hidden" name="id" value="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:black;">@lang('Edit Shipping Address')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-bookmark-o"></i></span>
                                    <input type="text" class="form-control" name="name" value="" placeholder="Enter Name">
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-phone"></i></span>
                                    <input type="text" class="form-control" name="phone" value="" placeholder="Enter Phone">
                                </div>
                            </div>
                            <label class="col-md-7 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-envelope"></i></span>
                                    <input type="email" class="form-control" name="email" value="" placeholder="Enter Email" >
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-building-o"></i></span>
                                    <input type="text" class="form-control" name="address1" value="" placeholder="Enter Address" >
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-building-o"></i></span>
                                    <input type="text" class="form-control" name="address2" value="" placeholder="Enter Address2">
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-globe"></i></span>
                                    <input type="text" class="form-control" name="city" value="" placeholder="Enter City" >
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12" style="padding-right:0px;">
                                <div class="input-group" id="state_area">
                                    <span class="input-group-addon"><i class="uk-icon-globe"></i></span>
                                    <input type="text" class="form-control" name="state" value="" placeholder="Enter State or Select one" style="width:190px;">

                                    <select name="state_select" class="selectpicker form-control input-sm" data-live-search="true" onchange="select_state_change(this,this.value);$( '#formNewShippingAddress' ).valid();">
                                        <option value="">Please select</option>
                                        @include('partial.select_option_US_state')
                                    </select>

                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="uk-icon-globe"></i></span>
                                    <input type="text" class="form-control" name="zipcode" value="" placeholder="Enter Zip Code">
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-5">
                                <div class="input-group" id="country_area">
                                    <span class="input-group-addon"><i class="uk-icon-globe"></i></span>
                                    <select name="country" class="selectpicker form-control input-sm" data-live-search="true" style="height: 30px;">
                                        <option value="">Country</option>
                                        @include('partial.select_option_country')
                                    </select>
                                </div>
                            </div>
                            <label class="col-md-12 control-label"></label>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="edit_address('formEditShippingAddress');">@lang('Submit')</button>
                <button type="button" class="btn btn-default" onclick="remove_address(this)">@lang('Remove')</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('Close')</button>
            </div>
            </form>
        </div>

    </div>
</div>
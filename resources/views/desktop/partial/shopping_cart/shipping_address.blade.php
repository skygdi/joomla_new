<div class="uk-panel uk-panel-box uk-panel-header zx-zoocart-checkout-fieldset" style="">
    <div class="row">
        <div class="col-md-12">
            <div class="uk-text-primary uk-margin-bottom uk-text-center zx-zoocart-checkout-fieldset-title">@lang('Input or choose the <span class="uk-text-bold">shipping</span> Address')</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9" style="font-weight: 500;font-size: 16px;" id="shipping_address_current">
            {{$SAD->name}}<BR>
            @if( !empty($SAD->phone) )
            {{$SAD->phone}},
            @endif
            <a href="mailto:{{$SAD->email}}">{{$SAD->email}}</a>
            <input type="hidden" name="id" value="{{$SAD->id}}">
            <input type="hidden" name="country" value="{{$SAD->country}}">
        </div>
        <div class="col-md-3">
            <a href="javascript://void()" onclick="button_change_address(this,'shipping_address_list')" class="uk-button uk-button-mini tooltip-custom" style="width: 86px;" data-toggle="tooltip" title="@lang('Change the current Address')">
                <span>@lang('CHANGE')</span>
                <span style="display: none;"><i class="uk-icon-chevron-up" style="line-height: 25px;"></i></span>
            </a>
        </div>
    </div>

    <div style="margin-top:10px;display: none;" id="shipping_address_list">
        <div class="row">
            <div class="col-md-12">
                <ul class="zx-zoocart-address-manager-rows uk-list uk-list-line">
                    @foreach( $SA as $address )
                    <li data-id="{{$address->id}}">
                        <!-- title -->
                        <strong class="zx-zoocart-address-manager-row-title uk-float-left">{{$address->name}}</strong>

                        <!-- preview -->
                        <div style="display: none;width:0px;height: 0px;">
                            {{$address->name}}<BR>
                            @if( !empty($address->phone) )
                            {{$address->phone}},
                            @endif
                            <a href="mailto:{{$address->email}}">{{$address->email}}</a>
                            <input type="hidden" name="id" value="{{$address->id}}">
                            <input type="hidden" name="country" value="{{$address->country}}">
                        </div>

                        <!-- btns -->
                        <div class="uk-button-group uk-float-right">

                            <!-- edit -->
                            <button type="button" class="skygdi-button-mini skygdi-button-first tooltip-custom" onclick="edit_address_show('{{$address->id}}',this,'shipping','ModalEditBillingAddress')" data-toggle="tooltip" title="@lang('Edit this Address')">
                                <i class="uk-icon-pencil"></i>
                            </button>

                            <!-- default  title="Set this Address as default" -->
                            @if( $address->is_default==1 )
                            <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{$address->id}}',this,'shipping')" disabled="">
                                <i class="uk-icon-dot-circle-o"></i>
                            </button>
                            @else
                            <button type="button" class="change_default skygdi-button-mini skygdi-button-middle" onclick="change_default_address('{{$address->id}}',this,'shipping')" title="@lang('Set this Address as default')">
                                <i class="uk-icon-circle-o"></i>
                            </button>
                            @endif

                            <!-- choose -->
                            <button type="button" class="skygdi-button-mini skygdi-button-last tooltip-custom" title="@lang('Choose this Address')" data-toggle="tooltip" onclick="choose_shipping_address(this);">
                                <i class="uk-icon-check"></i>
                            </button>

                        </div>
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="zx-zoocart-address-manager-row-add uk-button uk-button-mini uk-float-right" style="display: block;" data-toggle="modal" data-target="#ModalNewShippingAddress">@lang('<i class="uk-icon-plus"></i>&nbsp;&nbsp;Address')
                </button>
            </div>
        </div>
    </div>

</div>
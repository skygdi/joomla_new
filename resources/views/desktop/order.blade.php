@extends('desktop.layouts.main')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/item_manage.css') }}">
@stop

@section('js')
    @parent

    <script type="text/javascript" src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap-filestyle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/media.js') }}"></script>

<script type="text/javascript">

</script>

@stop

@section('content')

<div class="container container_common" style="padding-top:130px !important;">
    
    <div class="row">
        <div class="col-md-12">
            <table class="uk-table uk-table-striped uk-table-condensed">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th class="uk-hidden-small">@lang('Created on')</th>
                        <th class="uk-hidden-small">@lang('Total')</th>
                        <th class="uk-hidden-small">@lang('Payment Method')</th>
                        <th>@lang('Status')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $result as $order )
                    <tr>
                        <td><a href="{{ URL('/') }}/order/{{$order["id"]}}">0011{{$order["id"]}}</a></td>
                        <td class="uk-hidden-small">{{ Carbon\Carbon::parse($order["created_on"])->format('j F Y') }}</td>

                        <td class="uk-hidden-small">${{$order->Total()}}</td>
                        <td class="uk-hidden-small">{{$order["pay_type"]}}</td>
                        <td>
                            @if( $order["cancel"]==0 && $order["paid"]==0 && $order["delivered"]==0 && $order["payment_validating"]==0 )
                            <span class="uk-badge uk-badge-default" title="This is the status of an order that has yet to be payed, and has just been saved from your cart" data-uk-tooltip="">
                                @lang('Pending')
                            </span>
                            @elseif( $order["cancel"]==0 && $order["paid"]==1 && $order["delivered"]==0 )
                            <span class="uk-badge uk-badge-success" title="" data-uk-tooltip="" data-cached-title="Status for order, when payment is received">
                                @lang('Payment received')
                            </span>
                            @elseif( $order["cancel"]==0 && $order["paid"]==1 && $order["delivered"]==1 )
                            <span class="uk-badge uk-badge-default" title="This is the status of an order that has yet to be payed, and has just been saved from your cart" data-uk-tooltip="">
                                @lang('SHIPPED')
                            </span>
                            @elseif( $order["cancel"]==1 )
                            <span class="uk-badge uk-badge-danger" title="" data-uk-tooltip="" data-cached-title="Status for order that was cancelled by user or administrator">
                                @lang('Canceled')
                            </span>
                            @elseif( $order["cancel"]==0 && $order["paid"]==0 && $order["payment_validating"]==1 )
                            <span class="uk-badge uk-badge-default" title="" data-uk-tooltip="" data-cached-title="Order being validating">
                                @lang('Validating Payment')
                            </span>
                            
                            @endif

                            

                            

                        </td>
                    </tr>
                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 pull-right">
            <div id="pagination" class="pull-right">
                {!! $result->render() !!}
            </div>
        </div>
    </div>

</div>


@stop
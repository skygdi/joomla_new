<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码至少需要6个字并且需和确认密码一致.',
    'reset' => '已重置你的密码!',
    'sent' => '我们已经向你发送一封含有复位链接的邮件!',
    'token' => '密码重置的Token无效.',
    'user' => "我们无法查询到匹配该Email的用户.",

];

﻿<?php

return [

    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    

    'custom' => [
        'name' => [
            'required'  =>  '登录名不能为空',
            'unique' => '登录名已存在',
            'min'   => '登录名至少 :min 个字',
            'max'   =>  '最大不能超过 :max 个字',
        ],
        'username'  =>  [
            'required'  =>  '名称不能为空',
            'max'   =>  '最大不能超过 :max 个字',
        ],
        'email' => [
            'unique' => '你的电邮已经存在',
            'email' =>  '邮箱格式不正确',
            'max'   =>  '最大不能超过 :max 个字',
            'exists'    =>  '邮箱不存在',
        ],
        'password'  =>[
            'required'  =>  '密码不能为空',
            'min'   =>  '密码必须至少为 :min 个字',
            'confirmed'  =>  '确认密码和密码不相同',
            'max'   =>  '最大不能超过 :max 个字',
        ],
        'paypal'    =>  [
            'required'  =>  '不能为空',
            'email'     =>  '必须为邮箱格式',
            'unique'    =>  '该PayPal账号已存在',
            'max'   =>  '最大不能超过 :max 个字',
        ],
        'g-recaptcha-response' =>[
            'required'  =>  '请验证',
            'recaptcha'  =>  '请验证',
        ],
        'first_name'    =>  [
            'required'  =>  '不能为空',
            'min'   => '名称至少 :min 个字',
        ],
        'message'    =>  [
            'required'  =>  '不能为空',
            'min'   => '信息至少 :min 个字',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];

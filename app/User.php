<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

use Validator;
use Redirect;
use App\Notifications\MailResetPasswordToken;

use App\Models\UserProfile;
use App\Traits\DesktopPage;
//use Image;
//use Intervention\Image\ImageManagerStatic as Image;
//use Intervention\Image\Facades\Image;

//use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
//use Mail;


class User extends Authenticatable
{
    use Notifiable;

    use DesktopPage;

    const CREATED_AT = 'registerDate';
    const UPDATED_AT = 'lastvisitDate';


    public function SaveAvatar($request){
        //try remove previous avatar file
        @unlink(storage_path()."/app/images/avatar/" .$this->profile->avatar);

        //save
        $ext = $request->avatar_file->extension();
        $file_name = $this->id."_".time().".".$ext;
        $destination_file = storage_path()."/app/images/avatar/" . $file_name;
        if( file_exists($destination_file) ) unlink($destination_file);
        $request->avatar_file->storeAs("images/avatar", $file_name);

        $this->Profile()->updateOrCreate(["user_id"=>$this->id],["avatar"=>$file_name]);
        //$this->profile->avatar = $file_name;
        //$this->profile->save();
    }

    public function SaveAvatarFromUrl($url){
        $ext = pathinfo(parse_url($url)['path'], PATHINFO_EXTENSION);

        //try remove previous avatar file
        @unlink(storage_path()."/app/images/avatar/" .$this->profile->avatar);

        $file_name = $this->id."_".time().".".$ext;
        $destination_file = storage_path()."/app/images/avatar/" . $file_name;
        if( file_exists($destination_file) ) unlink($destination_file);

        $file = file_get_contents($url);
        file_put_contents($destination_file, $file);

        //Image::make($url)->save(storage_path('images/avatar/' . $filename));
        //$request->avatar_file->storeAs("images/avatar", $file_name);
        
        $this->Profile()->updateOrCreate(["user_id"=>$this->id],["avatar"=>$file_name]);
    }

    public function Profile()
    {
        return $this->hasOne('App\Models\UserProfile');
    }

    public function isAdmin()
    {
        return $this->admin;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'name', 
        'password',
        'username',
        'activation',
        'google_id',
        'facebook_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

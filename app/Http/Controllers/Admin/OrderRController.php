<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
//use Input;
use Illuminate\Support\Facades\Input;
use App\Models\Order;
use App\Models\OrderItem;

class OrderRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery = Order::Orderby("id","DESC");


        /*
        if( Session::has('order_filter_array') ) $filter_array = Session::get('order_filter_array');
        else{
            $filter_array["pay_state"] = "";
            $filter_array["date_form"] = "";
            $filter_array["date_to"] = "";
        }
        */
        $page_limited = 20;
        
        if(Input::has("pay_state") && Input::get("pay_state")=="pending"){
            $Equery->where("cancel",0)
                    ->where("paid",0)
                    ->where("delivered",0)
                    ->where("payment_validating",0);
        }
        else if(Input::has("pay_state") && Input::get("pay_state")=="payment_received"){
            $Equery->where("cancel",0)
                    ->where("paid",1)
                    ->where("delivered",0);
        }
        else if(Input::has("pay_state") && Input::get("pay_state")=="shipped"){
            $Equery->where("cancel",0)
                    ->where("paid",1)
                    ->where("delivered",1);
        }
        else if(Input::has("pay_state") && Input::get("pay_state")=="canceled"){
            $Equery->where("cancel",1);
        }
        else if(Input::has("pay_state") && Input::get("pay_state")=="validating_payment"){
            $Equery->where("cancel",0)
                    ->where("paid",0)
                    ->where("payment_validating",1);
        }

        if( Input::has("date_from") && Input::get("date_from")!="" ){
            $Equery->whereDate('created_at', '>=', date("Y-m-d",strtotime(Input::get("date_from" ))) );
        }

        if(Input::has("date_to") && Input::get("date_to")!="" ){
            $Equery->whereDate('created_at', '<=', date("Y-m-d",strtotime(Input::get("date_to" ))) );
        }

        if( count(Input::all())!=0 ){
            $result = $Equery->paginate($page_limited)
                ->appends([
                         "pay_state" =>  @Input::get("pay_state"),
                         "date_from" =>  @Input::get("date_from"),
                         "date_to" =>  @Input::get("date_to"),
                    ]);
        }
        else{
            $result = $Equery->paginate($page_limited);
        }

        return view('admin.order')
                ->with('pay_state',@Input::get("pay_state"))
                ->with('date_from',@Input::get("date_from"))
                ->with('date_to',@Input::get("date_to"))
                ->with('result',$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Order::where("id",$id)->first();
        return view('admin.order_edit')
                ->with('data',$result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form_data = $request->all();

        //Update Quantity
        $qty_array = $request->get("qty");
        foreach( $qty_array as $oid => $qty ){
            if( $qty<=0 ) OrderItem::find($oid)->delete();
            else OrderItem::find($oid)->update(["quantity"=>$qty]);
        }


        if($request->has("pay_state") && $request->get("pay_state")=="pending"){
            $form_data["cancel"] = 0;
            $form_data["paid"] = 0;
            $form_data["delivered"] = 0;
            $form_data["payment_validating"] = 0;
        }
        else if($request->has("pay_state") && $request->get("pay_state")=="payment_received"){
            $form_data["cancel"] = 0;
            $form_data["paid"] = 1;
            $form_data["delivered"] = 0;
        }
        else if($request->has("pay_state") && $request->get("pay_state")=="shipped"){
            $form_data["cancel"] = 0;
            $form_data["paid"] = 1;
            $form_data["delivered"] = 1;
            
        }
        else if($request->has("pay_state") && $request->get("pay_state")=="canceled"){
            $form_data["cancel"] = 1;
        }
        else if($request->has("pay_state") && $request->get("pay_state")=="validating_payment"){
            $form_data["cancel"] = 0;
            $form_data["paid"] = 0;
            $form_data["payment_validating"] = 1;
        }

        Order::find($id)->update($form_data);

        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

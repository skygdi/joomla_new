<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class MenuController extends Controller
{
    function Dashboard(){
        return view('admin.dashboard');
    }


    function Login(){
    	if( Auth::check() ){
    		//return redirect()->intended('admin/order');
    		return redirect()->intended('admin/dashboard');
    	}
        else return view('admin.login');
    }
}

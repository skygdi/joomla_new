<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Auth;
use Session;
use Storage;

use App\Models\HomePageSlider;

class SliderRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$result = HomePageSlider::where("suspend","0")->Orderby("priority","DESC")->paginate(10);
        $result = HomePageSlider::Orderby("priority","DESC")->paginate(10);
        return view('admin.home_slider')
                ->with('result',$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file_image' => 'required|image|mimes:jpeg,png,jpg,gif|min:1|max:10000',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $filename = Auth::id()."_".time().".".$request->file_image->extension();
        $request->file_image->storeAs("images/slider", $filename );
        $pars = array();
        //$pars["content"] = html_entity_decode( $request->get("content") );
        $pars["content"] = $request->get("content");
        $pars["file"] = $filename;
        HomePageSlider::create($pars);

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $slider = HomePageSlider::find($id);
        return view('admin.home_slider_edit')
                ->with('slider',$slider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = HomePageSlider::find($id);
        if( !$slider ){
            return redirect()->back()->withInput()->withErrors("slider not found!");
        }


        //dd($request->file('file_image'));

        $pars = array();
        if( $request->has("file_image") ){
            $validator = Validator::make($request->all(), [
                'file_image' => 'required|image|mimes:jpeg,png,jpg,gif|min:1|max:10000',
            ]);
            if ($validator->fails())
            {
                return redirect()->back()->withInput()->withErrors($validator);
            }

            $filename = Auth::id()."_".time().".".$request->file_image->extension();
            $request->file_image->storeAs("images/slider", $filename );

            //Delete file
            //$old_file = storage_path()."/images/slider/".$slider->file;
            $old_file = "/images/slider/".$slider->file;
            //dd($old_file);
            if( Storage::exists($old_file) ){
                Storage::delete($old_file);
            }
            
            
            $pars["file"] = $filename;
        }


        $pars["content"] = $request->get("content");
        $pars["priority"] = $request->get("priority");

        
        $slider->update($pars);

        Session::flash('message', 'Changed');
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HomePageSlider::find($id)->delete();
    }

    public function Suspend($id,$value){
        //header('HTTP/1.1 500 Internal Server Error');
        $slider = HomePageSlider::find($id);
        $slider->update(['suspend'=>$value]);

        //dump($value);
    }
}

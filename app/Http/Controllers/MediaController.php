<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use File;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;

use App\Models\Media;


class MediaController extends Controller
{
    static public $cache = true;

    static public function store(Request $request)
    {
        $pars = Input::all();
            
        $pars["user_id"] = Auth::id();
        $pars["original_created_by"] = Auth::id();
        //$pars["searchable"] = Input::get("searchable");

        //Image
        /*
        $validator = Validator::make(Input::all(), [
            'file_image' => 'required|image|mimes:jpeg,png,jpg,gif|min:1|max:2000',
        ]);
        if ($validator->fails())
        {
            return $validator;
        }
        $filename = Auth::id()."_".time().".".$request->file_image->extension();
        $request->file_image->storeAs("images/media", $filename );
        $pars["file"] = $filename;
        */
        $ret = self::_validateImageAndStore($request,$pars,true);
        if( $ret!==true ) return $ret;

        //Keyword
        $keywords = array();
        $ret = self::_validateKeywords($request,$keywords);
        if( $ret!==true ) return $ret;

        $m = Media::Create($pars);
        if( $m ) $m->SaveWord( $keywords );

        //return $this->create();
        return true;
    }

    static public function update(Request $request, $id)
    {
        $pars = $request->all();
        //var_dump($pars);

        //Image
        if( $request->has('file_image') ){
            $ret = self::_validateImageAndStore($request,$pars,false);
            if( $ret!==true ) return $ret;
        }

        //Keyword
        $keywords = array();
        $ret = self::_validateKeywords($request,$keywords);
        if( $ret!==true ) return $ret;

        //Normal
        $ret = self::_validateNormal($request);
        if( $ret!==true ) return $ret;
        
        
        $media = Media::Self(Auth::id())->find($id);
        if( !$media ) abort(404);
        $media->fill( $pars );
        $media->save();
        $media->SaveWord( $keywords );


        return true;
    }

    private static function _validateImageAndStore($request,&$pars,$required = false){
        if( $required ) $required_str = "required";
        else $required_str = "sometimes";

        $validator = Validator::make($request->all(), [
            'file_image' => $required_str.'|image|mimes:jpeg,png,jpg,gif|min:1|max:2000',
        ]);
        if ($validator->fails())
        {
            return $validator;
        }
        $filename = Auth::id()."_".time().".".$request->file_image->extension();
        $request->file_image->storeAs("images/media", $filename );
        $pars["file"] = $filename;
        return true;
    }

    private static function _validateKeywords($request,&$keywords){
        $keywords = json_decode($request->get('keywords'));
        if( count($keywords)>0 ){
            $validator = Validator::make(['keywords'=>$keywords], [
                'keywords'  =>  'required|array|between:0,5',
                'keywords.*' => 'required|min:3|max:24',
            ]);
            if ($validator->fails())
            {
                return $validator;
            }
        }
        return true;
    }

    private static function _validateNormal($request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:128',
        ]);
        if ($validator->fails())
        {
            return $validator;
        }
        
        return true;
    }

    

    public function destroy($id)
    {
        Media::where("id",$id)
                ->Self(Auth::id())
                ->delete();
        return ['state' => 'success'];
    }

    public function Like($id,$value){
        if( $value=="n" ){
            //Like
            $obj = \App\Models\MediaLike::firstOrNew(array('media_id' => $id,'user_id' => Auth::id()));
            $obj->save();

        }
        else{
            //Dislike
            $obj = \App\Models\MediaLike::where("media_id",$id)->where("user_id",Auth::id())->delete();
        }

        return ['state' => 'success'];
    }

    public function GalleryLoad($pos){
        $limit = 12;
        $result_popular = array();
        $Equery_MP = Media::Searchable()->skip($pos)->take($limit);
        $Equery_MP->where("shows","!=",0);
        $Equery_MP->Orderby("shows","DESC");
        $Equery_MP_result = $Equery_MP->get();
        foreach( $Equery_MP_result as $media ){
            $media->ImageCacheGet();
            $result_popular[] = $media;
        }
        return $result_popular;
    }

    

    public static function GetItem($pos,$limit){
        $cache_key = "GetItem_".$pos."_".$limit;

        if( self::$cache && Cache::has($cache_key) ) return Cache::get($cache_key);
        //dump("GetItem real load");

        $result_popular = array();
        $Equery_MP = Media::Searchable()->skip($pos)->take($limit);
        $Equery_MP->where("shows","!=",0);
        $Equery_MP->Orderby("shows","DESC");
        $Equery_MP_result = $Equery_MP->get();
        foreach( $Equery_MP_result as $media ){
            $media->ImageCacheGet();
            $media_data = $media->toArray();
            $media_data["name"] = $media->User()->first()["name"];
            //$result_popular[] = $media_data;
            $result_popular[] = $media;
        }

        $result_newest = array();
        $Equery_NR = Media::Searchable()->skip($pos)->take($limit);
        $Equery_NR->Orderby("created_at","DESC");
        $Equery_NR_result = $Equery_NR->get();
        foreach( $Equery_NR_result as $media ){
            $media->ImageCacheGet();
            $media_data = $media->toArray();
            $media_data["name"] = $media->User()->first()["name"];
            //$result_newest[] = $media_data;
            $result_newest[] = $media;
        }

        $data = [$result_popular,$result_newest];
        
        if( self::$cache ){
            Cache::add($cache_key,$data,5);
        }

        return $data;

    }

    function Thumb($filename){
        $path = storage_path().'/app/images/media_thumb/'.$filename;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        $response->header("Cache-Control", " private, max-age=".Media::$image_cache_lifetime);
        return $response;
    }

    function Image($filename){
        $path = storage_path().'/app/images/media/'.$filename;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        $response->header("Cache-Control", " private, max-age=1800");
        return $response;
    }

    function HomeMore(){
        $final_array = array();
        list($final_array["click"],$final_array["newest"]) = $this->GetItem(Input::get('loaded_click'),Input::get('load_each'));

        $data_tmp =  array();
        foreach( $final_array["click"] as $data ){
            $data_array = $data->toArray();
            $data_array["user_name"] = $data->User()->first()["name"];
            $data_tmp[] = $data_array;
        }
        $final_array["click"] = $data_tmp;

        $data_tmp =  array();
        foreach( $final_array["newest"] as $data ){
            $data_array = $data->toArray();
            $data_array["user_name"] = $data->User()->first()["name"];
            $data_tmp[] = $data_array;
        }
        $final_array["newest"] = $data_tmp;

        echo json_encode($final_array);
        exit();
    }
}

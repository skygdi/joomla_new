<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Traits\DesktopPage;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ShoppingCartItem;

use App\Http\Controllers\OrderController;

class OrderRController extends Controller
{
    use DesktopPage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Order::Self( Auth::id() )->Orderby("id","DESC")->paginate(20);
        return view('desktop.order')
                ->with('vars', $this->vars)
                ->with('result',$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $item_array = json_decode($request->get("item_info"));
        $addresses = json_decode($request->get("addresses"),true);

        OrderController::store($item_array,$addresses);

        return ['state'=>'success'];
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Order::Self( Auth::id() )->where("id",$id)->first();
        return view('desktop.order_detail')
                ->with('vars', $this->vars)
                ->with('PAYPAL_ENV',$_ENV['PAYPAL_ENV'])
                ->with('data',$result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
    }
}

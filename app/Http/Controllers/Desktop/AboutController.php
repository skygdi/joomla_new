<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Redirect;
use Response;
use Illuminate\Support\Facades\Input;

use App\Traits\DesktopPage;

class AboutController extends Controller
{
	use DesktopPage;

    function about(){
    	return view('desktop.about.about')->with('vars', $this->vars);
    }

    function privacy(){
    	return view('desktop.about.privacy')->with('vars', $this->vars);
    }

    function terms(){
    	return view('desktop.about.terms')->with('vars', $this->vars);
    }

    function tos(){
    	return view('desktop.about.tos')->with('vars', $this->vars);
    }

    function contact(){
    	return view('desktop.about.contact')->with('vars', $this->vars);
    }

    function contact_save(){
        //Image
        $validator = Validator::make(Input::all(), [
            'first_name'    => 'required|min:3|max:64',
            'email'         => 'required|email|max:128',
            'message'       => 'required|min:10|max:128',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
        
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        //Sending Email from here.

        return view('desktop.about.contact')->with('vars', $this->vars)->with('state', 'success');
    }
    
    

    

    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
    }
}

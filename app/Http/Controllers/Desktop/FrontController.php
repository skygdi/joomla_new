<?php
namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use DB;
use Auth;
use App\User;
use Validator;
use Redirect;
use Session;

use Jenssegers\Agent\Agent;
use App\Traits\DesktopPage;
use App\Models\Media;
use App\Models\HomePageSlider;

use App\Http\Controllers\MediaController;
use App\Http\Controllers\MiscController;


class FrontController extends Controller
{
    
    use DesktopPage;

    function Home(){
        //Move to Mobile
        $agent = new Agent();
        if( !$agent->isDesktop() ){
            return \Redirect::route('mobile.home');
        }
        //dump($agent->isDesktop());

        //Slider
        $sliders = HomePageSlider::where("suspend","0")->Orderby("priority","DESC")->limit(10)->get();
        /*
        //Slidershow
        $slidershow = array();
        $Equery = DB::table("widgetkit");
        $Equery->where("name","=","Home Slideshow");
        $Equery->take(1);
        if( $Equery->count()==1 ){
            $row = $Equery->get();
            $data = json_decode($row[0]->data,true);
            $current_data = array();
            foreach( $data["items"] as $row ){
                $current_data["media"] = $row["media"];
                $current_data["content"] = $row["content"];
                //var_dump($row["content"]);
                $slidershow[] = $current_data;
            }
        }
        $this->vars["slidershow"] = $slidershow;
        */
        $this->vars["load_each"] = $load_each = 12;

        list($this->vars["result"]["click"],$this->vars["result"]["newest"]) = MediaController::GetItem(0,$load_each);

        //Fix if not enough
        $mini = 12;
        if( count($this->vars["result"]["click"])<$mini ){
            $media_array = &$this->vars["result"]["click"];
            $limit = $mini - count($this->vars["result"]["click"]);

            $Equery_NR = Media::take($limit);
            $Equery_NR->Where("searchable","=","1");


            $Equery_NR->where(function ($query) use ($media_array) {
                foreach( $media_array as $media ){
                    $query->Where("id","!=",$media->id);
                }
                
            });


            $Equery_NR->Orderby("created_at","DESC");
            $Equery_NR->skip(0);
            $mps = $Equery_NR->get();
            foreach( $mps as $mp ){
                $mp->ImageCacheGet();
                $this->vars["result"]["click"][] = $mp;
                //$result_newest[] = $mp;
            }
        }

        return view('desktop.index')
                ->with("sliders",$sliders)
                ->with('vars', $this->vars);
    }

    function SearchWord($word=null){
        MiscController::SearchWord($word);
        return $this->SearchPage(0);
    }

    function SearchPage($page=0){
        $Equery_result = MiscController::SearchPage($page);
        $Equery_result->withPath( url('/search_page') );
        
        return view('desktop.search')
                ->with('result',$Equery_result)
                ->with('word',Session::get('search_word'))
                ->with('ordering',Session::get('search_ordering'))
                ->with('vars', $this->vars);
    }

    function __construct(){
        //$this->PageRenderInitialize();
        ///*
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
        //*/
    }
}

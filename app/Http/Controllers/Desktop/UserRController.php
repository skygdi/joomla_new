<?php

namespace App\Http\Controllers\Desktop;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use Auth;
//use Session;
//use Validator;
//use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\User;


use App\Traits\DesktopPage;
use App\Models\Media;
use App\Http\Controllers\UserController;

class UserRController extends Controller
{
	use DesktopPage;

    function Profile(Request $request) {
        //Save
        if( Input::has('email') ){
            $validator = UserController::Update();
            if( $validator!==true ){
                return Redirect::to('profile')->withInput()->withErrors($validator);
            }

            $validator = UserController::UpdateAvatar($request);
            if( $validator!==true ){
                return Redirect::to('profile')->withInput()->withErrors($validator);
            }
        }

        return view('desktop.profile')->with('vars', $this->vars);
    }

    

    public function showLogoutForm()
    {
        return view('desktop.auth.logout')->with('vars', $this->vars);
    }
    
    function Channel($id){
        $user = User::where("id",$id)->first();
        if( !$user ) abort(404);
       
        $Equery = Media::where("user_id",$user->id)->take(100);
        $Equery->orderby("updated_at","DESC");
        $medias = $Equery->get();

        return view('desktop.user_channel')
                ->with('vars', $this->vars)
                ->with('medias', $medias)
                ->with('user',$user);
    }

    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
    }
}

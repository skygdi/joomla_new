<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Traits\DesktopPage;

class StaticDocumentController extends Controller
{
	use DesktopPage;
    //
    //
    function about(){
    	return view('desktop.static_document.about')
            ->with('vars', $this->vars);
    }
}

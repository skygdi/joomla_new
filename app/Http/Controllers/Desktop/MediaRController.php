<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Session;
use Validator;
use Redirect;
use File;
use Response;
use Illuminate\Support\Facades\Input;
use App\Traits\DesktopPage;
use App\Models\Media;
use App\Models\Expenditure;
use App\Models\MediaPopular;



use App\Http\Controllers\MediaController;

class MediaRController extends Controller
{
    use DesktopPage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery = Media::where("user_id",Auth::id());
        $Equery->OrderBy("created_at","DESC");
        $Equery_result = $Equery->get();
        $total_sold = $Equery_result->sum(function ($media) {
            return $media->Sold();
        });
        $total_balance = $Equery_result->sum(function ($media) {
            return $media->Balance();
        });

        $paid = Expenditure::where("user_id",Auth::id())->sum("money");
        $unpaied_balance = $total_balance - $paid;

        $items = $Equery->Paginate(20);
        return view('desktop.media')
            ->with("items",$items)
            ->with("total_sold",$total_sold)
            ->with("total_balance",$total_balance)
            ->with("unpaied_balance",$unpaied_balance)
            ->with('vars', $this->vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('desktop.media_create')->with('vars', $this->vars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = MediaController::store($request);
        if( $validator!==true ){
            
            return redirect()->back()->withInput()->withErrors($validator);
        }
        return redirect()->back();
        //return $this->create();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $media = Media::where("id",$id)->first();
        //mark click
        $media->Click();
        //tags
        $result_tags = $media->KeyWord()->orderby("name","DESC")->get();
        $tags_array = array();
        foreach( $result_tags as $result_tag ){
            $tags_array[] = "'".$result_tag->name."'";
        }
        $this->vars["item_keyword"] = join(",",$tags_array);
        $this->vars["media"] = $media;

        return view('desktop.media_show')->with('vars', $this->vars);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd("1");
        $media = Media::where("id",$id)->first();
        $this->vars["media"] = $media;

        $tags_array = array();
        foreach ($media->KeyWord()->get() as $tag){
            $tags_array[] = "'".$tag->name."'";
        }
        $this->vars["tags_array"] = join(",",$tags_array);
        //dump($this->vars["tags_array"]);

        return view('desktop.media_edit')->with('vars', $this->vars);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = MediaController::update($request,$id);
        if( $validator!==true ){
            //dd("kjk");
            //dd($validator);
            return redirect()->back()->withInput()->withErrors($validator);
        }
        return redirect()->back();
    }

    function Gallery(){
        return view('desktop.media_gallery')
                ->with('load_each',12)
                ->with('vars', $this->vars);
    }

    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });

        $this->middleware(
            'auth', 
            ['except' => ['show']]
        );
    }
}

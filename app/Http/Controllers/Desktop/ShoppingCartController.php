<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Traits\DesktopPage;
use App\Models\PostCard;
use App\Models\ShoppingCartItem;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;

class ShoppingCartController extends Controller
{
    use DesktopPage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery_BA = Address::Self( Auth::id() );
        $Equery_BA->where("type","billing");
        $Equery_BA->orderBy("created_at","DESC");
        $BA = $Equery_BA->get();
        

        $BAD = Address::Self( Auth::id() )->where("type","billing")->Default()->first();

        $Equery_SA = Address::Self( Auth::id() );
        $Equery_SA->where("type","shipping");
        $Equery_SA->orderBy("created_at","DESC");
        $SA = $Equery_SA->get();


        $SAD = Address::Self( Auth::id() )->where("type","shipping")->Default()->first();

        //echo count($BAD);exit();
        if( count($BA)==0 || count($SA)==0 || count($BAD)==0 || count($SAD)==0 ){
            return \Redirect::to('/address');
        }

        $result = ShoppingCartItem::Self( Auth::id() )->orderby("id","DESC")->get();
        //$Config::get('paypal');
        return view('desktop.shopping_cart')
            ->with('vars', $this->vars)
            ->with('PAYPAL_ENV',$_ENV['PAYPAL_ENV'])
            ->with('after_paid_url',url('/order'))
            ->with('BA',$BA)
            ->with('SA',$SA)
            ->with('BAD',$BAD)
            ->with('SAD',$SAD)
            ->with('result',$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postcard = PostCard::Self( Auth::id() )->where("id",$request->get('id'))->first();
        if( !$postcard ) abort(404);

        //var_dump($postcard->id);

        $SCI = ShoppingCartItem::Self( Auth::id() )->firstOrNew(["postcard_id"=>$postcard->id]);
        $SCI->user_id = Auth::id();
        $SCI->quantity++;
        $SCI->save();


        return ["state" =>  "success","id"  =>  $SCI->id, "minicart" =>$this->MiniCartInfo() ];
    }

    public function reorder($id){
        
        //clear shopping cart
        //ShoppingCartItem::Self( Auth::id() )->delete();
        
        //get item from old order
        $order_item_array = Order::Self( Auth::id() )->where("id",$id)->first()->OrderItem()->get();
        
        foreach( $order_item_array as $order_item ){

            $SCI = ShoppingCartItem::Self( Auth::id() )->firstOrNew(["postcard_id"=>$order_item->postcard_id]);
            $SCI->user_id = Auth::id();
            $SCI->quantity += $order_item->quantity;
            $SCI->save();

        }

        //die("success");
        return ["state"=>"success"];
    
    }

    public function MiniCartInfo(){
        $data = array();
        $data["data"] = $this->ItemList();
        $data["count"] = $this->SummaryCount();
        $data["total"] = $this->SummaryTotal();

        return $data;
    }

    public static function SummaryCount(){
        $SCI = ShoppingCartItem::Self( Auth::id() )->get();

        $total = $SCI->sum(function ($item){
                return  $item->quantity;
             });
        return $total;
    }

    public static function SummaryTotal(){
        $SCI = ShoppingCartItem::Self( Auth::id() )->get();

        $prices = $SCI->sum(function ($item){
                return ($item->PostCard()->first()->Price() * $item->quantity );
             });
        return round($prices,2);
    }

    public static function ItemList(){
        $Equery = ShoppingCartItem::Self( Auth::id() );
        $Equery->orderby("id","DESC");
        $result_cartitems = $Equery->get();
        $data_array = array();

        foreach( $result_cartitems as $cartitems ){
            $data = array();
            $data["id"] = $cartitems->id;
            $data["name"] = $cartitems->PostCard()->first()->name;
            $data["price"] = $cartitems->PostCard()->first()->Price();
            $data["quantity"] = $cartitems->quantity;
            $data["total"] = number_format($data["price"]*$data["quantity"], 2, '.', '');

            $data_array[] = $data;
        }
        return $data_array;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ShoppingCartItem::Self( Auth::id() )->where("id",$id)->update(["quantity"=>$request->get("quantity")]);
        return ["state" =>  "success", "minicart" =>$this->MiniCartInfo() ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShoppingCartItem::Self( Auth::id() )->where("id",$id)->delete();

        return ["state" =>  "success", "minicart" =>$this->MiniCartInfo() ];
    }

    public function updates(Request $request){
        //dump( );
        $data = json_decode($request->get("item_info"),true);
        foreach ($data as $key => $value) {
            $id = $value["id"];
            $quantity = $value["quantity"];

            ShoppingCartItem::Self( Auth::id() )->where("id",$id)->update(["quantity"=>$quantity]);
        }
        return ["state" =>  "success"];

    }
    

    public function deletes(Request $request){
        foreach( $request->get("item_remove") as $id ){
            ShoppingCartItem::Self( Auth::id() )->where("id",$id)->delete();
        }

        return ["state" => "success"];
    }

    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
    }
}

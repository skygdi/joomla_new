<?php

namespace App\Http\Controllers\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

use App\User;
use Auth;

use App\Models\Media;
use App\Models\PostCard;
use App\Traits\DesktopPage;

class PostCardController extends Controller
{
    use DesktopPage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery = PostCard::where("user_id",Auth::id());
        $Equery->has('Media');
        $Equery->OrderBy("created_at","DESC");
        //$Equery_result = $Equery->get();

        $items = $Equery->Paginate(20);
        //var_dump($items);exit();
        return view('desktop.postcard')
            ->with("items",$items)
            ->with('vars', $this->vars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Equery = Media::Searchable()->take(100);
        $Equery->orderby("updated_at","DESC");
        $medias = $Equery->get();

        return view('desktop.postcard_create')
                ->with('vars', $this->vars)
                ->with('medias',$medias);
    }

    public function preselect_create($id){
        $Equery = Media::Searchable()->take(100);
        $Equery->orderby("updated_at","DESC");
        $medias = $Equery->get();

        $media = Media::Searchable()->find($id);

        return view('desktop.postcard_create')
                ->with('vars', $this->vars)
                ->with('medias',$medias)
                ->with('preselect_media',$media);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !Input::has("id") ) abort(404);
        $media = Media::Searchable()->where("id", Input::get("id") )->first();
        if( !$media ) abort(404);
        
        //echo Input::get("id");

        //'original_user_id'   =>  $media->user_id,
        PostCard::create([
            'name'           =>  Input::get("name"),
            'user_id'        =>  Auth::id(),
            'media_id'       =>  $media->id,
            'content'        =>  Input::get("content"),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Equery = Media::Searchable()->take(100);
        $Equery->orderby("updated_at","DESC");
        $medias = $Equery->get();

        $postcard = PostCard::Self(Auth::id())->find($id);

        return view('desktop.postcard_edit')
                ->with('vars', $this->vars)
                ->with('medias',$medias)
                ->with('postcard',$postcard);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postcard = PostCard::Self(Auth::id())->find($id);
        if( !$postcard ) abort(404);
        $postcard->name = Input::get("name");
        $postcard->content = Input::get("content");
        $postcard->media_id = Input::get("id");
        $postcard->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postcard = PostCard::Self(Auth::id())->find($id);
        if( !$postcard ) abort(404);
        $postcard->delete();

        return ['state' => 'success'];
    }

    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
    }
}

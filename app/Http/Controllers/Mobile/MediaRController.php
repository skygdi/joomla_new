<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\Media;
use App\Models\Expenditure;
//use App\Traits\DesktopPage;

use App\Http\Controllers\MediaController;

class MediaRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery = Media::where("user_id",Auth::id());
        $Equery->OrderBy("created_at","DESC");
        $Equery_result = $Equery->get();
        $total_sold = $Equery_result->sum(function ($media) {
            return $media->Sold();
        });
        $total_balance = $Equery_result->sum(function ($media) {
            return $media->Balance();
        });

        $paid = Expenditure::where("user_id",Auth::id())->sum("money");
        $unpaied_balance = $total_balance - $paid;

        $per_page = 20;
        $medias = $Equery->Paginate($per_page);


        $current_page = $medias->toArray()["current_page"];
        $max_page = ceil($medias->toArray()["total"] / $per_page);

        //Pagination
        if( $current_page<=1 ) $previous_page = null;
        else $previous_page = $current_page-1;

        if( $current_page>=$max_page ) $next_page = null;
        else $next_page = $current_page+1;
        
        return view('mobile.page.media_management.data')
            ->with("current_page",$current_page)
            ->with("next_page",$next_page)
            ->with("previous_page",$previous_page)
            ->with("max_page",$max_page)
            ->with("medias",$medias)
            ->with("total_sold",$total_sold)
            ->with("total_balance",$total_balance)
            ->with("unpaied_balance",$unpaied_balance);
    }

    public function store(Request $request)
    {
        $validator = MediaController::store($request);
        if( $validator!==true ){
            return ["state"=>"error","message"=>$validator->errors()->all()];
            //return redirect()->back()->withInput()->withErrors($validator);
        }
        return ["state"=>"success"];
        //return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $media = Media::where("id",$id)->first();
        //die("j");
        //mark click
        $media->Click();

        $result_tags = $media->KeyWord()->orderby("name","DESC")->get();
        $tags_array = array();
        $this->vars["item_keyword"] = join(",",$tags_array);
        $this->vars["media"] = $media;

        return view('mobile.page.media_view.data')
            ->with('vars', $this->vars)
            ->with('media',$media);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = MediaController::update($request,$id);
        if( $validator!==true ){
        }
        return ["state"=>"success"];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $media = Media::where("id",$id)->first();
        $media->Click();

        return view('mobile.page.media_management.data_edit')
            //->with('vars', $this->vars)
            ->with('media',$media);
    }
}

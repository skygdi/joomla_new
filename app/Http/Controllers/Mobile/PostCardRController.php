<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

use App\User;
use Auth;
use App\Models\Media;
use App\Models\PostCard;

class PostCardRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery = PostCard::where("user_id",Auth::id());
        $Equery->has('Media');
        $Equery->OrderBy("created_at","DESC");
        //$Equery_result = $Equery->get();

        $items = $Equery->Paginate(20);
        return view('mobile.page.postcard_management.data')
            ->with("items",$items);
    }

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $Equery = Media::Searchable()->take(100);
        $Equery->orderby("updated_at","DESC");
        $medias = $Equery->get();

        return view('mobile.page.postcard_new.data')
                ->with('medias',$medias);
    }


    public function preselect_create($id){
        $Equery = Media::Searchable()->take(100);
        $Equery->orderby("updated_at","DESC");
        //$medias = $Equery->get();

        $media = Media::Searchable()->find($id);
        $media->ImageCacheGet();

        return view('mobile.page.postcard_new.data')
                //->with('vars', $this->vars)
                //->with('medias',$medias)
                ->with('preselect_media',$media);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !Input::has("id") ) abort(404);
        $media = Media::Searchable()->where("id", Input::get("id") )->first();
        if( !$media ) abort(404);
        
        //echo Input::get("id");

        //'original_user_id'   =>  $media->user_id,
        PostCard::create([
            'name'           =>  Input::get("name"),
            'user_id'        =>  Auth::id(),
            'media_id'       =>  $media->id,
            'content'        =>  Input::get("content"),
        ]);

        return ["state"	=>	"success"];
    }
}

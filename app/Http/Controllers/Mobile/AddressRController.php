<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Traits\DesktopPage;
use App\Models\Address;

use App\Http\Controllers\AddressController;

class AddressRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery_BA = Address::Self( Auth::id() );
        $Equery_BA->where("type","billing");
        $Equery_BA->orderBy("created_at","DESC");
        $BA = $Equery_BA->get();

        $Equery_SA = Address::Self( Auth::id() );
        $Equery_SA->where("type","shipping");
        $Equery_SA->orderBy("created_at","DESC");
        $SA = $Equery_SA->get();
        

        return view('mobile.page.addresses_management.data')
            ->with("BA",$BA)
            ->with("SA",$SA);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $addr = Address::Self(Auth::id())->find($id);
        return view('mobile.page.addresses_management.edit_data')
                ->with('address', $addr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $obj = new AddressController();
        $validator = $obj->update_validator();
        if ($validator->fails())
        {
            return ["state" => "validate error","text"  =>  $validator->errors()->all() ];
        }
        $obj->update($request, $id);
        return ['state' => 'success'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

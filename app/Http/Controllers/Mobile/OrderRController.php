<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

use App\Http\Controllers\OrderController;
use App\Models\Order;

class OrderRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per_page = 20;

        $result = Order::Self( Auth::id() )->Orderby("id","DESC")->paginate($per_page);
        $current_page = $result->toArray()["current_page"];
        $max_page = ceil($result->toArray()["total"] / $per_page);

        //Pagination
        if( $current_page<=1 ) $previous_page = null;
        else $previous_page = $current_page-1;

        if( $current_page>=$max_page ) $next_page = null;
        else $next_page = $current_page+1;

        return view('mobile.page.orders.data')
                ->with("current_page",$current_page)
                ->with("next_page",$next_page)
                ->with("previous_page",$previous_page)
                ->with("max_page",$max_page)
                ->with('result',$result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $item_array = json_decode($request->get("item_info"));
        $addresses = json_decode($request->get("addresses"),true);

        OrderController::store($item_array,$addresses);

        return ['state'=>'success'];
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Order::Self( Auth::id() )->where("id",$id)->first();
        return view('mobile.page.orders_detail.data')
                //->with('vars', $this->vars)
                //->with('PAYPAL_ENV',$_ENV['PAYPAL_ENV'])
                ->with('data',$result);
    }
}

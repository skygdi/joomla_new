<?php
namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use DB;
use Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use App\User;
use Validator;
use Redirect;
use Config;
use Illuminate\Validation\Rule;

use App\Traits\DesktopPage;
use App\Models\Media;
use Jenssegers\Agent\Agent;
use Session;
//use App\Http\Controllers\Skygdi\media;
use App\Http\Controllers\MediaController;

use App\Http\Controllers\MiscController;

class FrontController extends Controller
{
    
    use DesktopPage;

    function Home(){
        $this->PageRenderInitialize();
        
        $this->vars["load_each"] = $load_each = 12;
        list($this->vars["result"]["click"],$this->vars["result"]["newest"]) = MediaController::GetItem(0,$load_each);

        
        return view('mobile.index')
                ->with('google_recaptcha_sitekey', Config::get('recaptcha')["public_key"])
                ->with('vars',$this->vars);
    }

    function Search($page=0,$word=null){
        MiscController::SearchWord($word);

        $Equery_result = MiscController::SearchPage($page);

        $per_page = 20;
        $current_page = $Equery_result->toArray()["current_page"];
        $max_page = ceil($Equery_result->toArray()["total"] / $per_page);

        //Pagination
        if( $current_page<=1 ) $previous_page = null;
        else $previous_page = $current_page-1;

        if( $current_page>=$max_page ) $next_page = null;
        else $next_page = $current_page+1;


        //$Equery_result->withPath( url('/search_page') );
        
        return view('mobile.page.search.data')
                ->with('result',$Equery_result)
                ->with("current_page",$current_page)
                ->with("next_page",$next_page)
                ->with("previous_page",$previous_page)
                ->with("max_page",$max_page)
                ->with('word',Session::get('search_word'))
                ->with('ordering',Session::get('search_ordering'));
    }

    /*
    function SearchWord($word=null){
        MiscController::SearchWord($word);
        return $this->SearchPage(0);
    }

    function SearchPage($page=0){
        $Equery_result = MiscController::SearchPage($page);
        $Equery_result->withPath( url('/search_page') );
        
        return view('desktop.search')
                ->with('result',$Equery_result)
                ->with('word',Session::get('search_word'))
                ->with('ordering',Session::get('search_ordering'));
    }
    */

}

<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//use File;
use Response;
//use Auth;
//use Session;
//use Validator;
//use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;

use App\User;

use App\Models\Media;

class UserRController extends Controller
{
    function Profile(Request $request) {
        //Save
        if( Input::has('email') ){
            $validator = \App\Http\Controllers\UserController::Update();
            if( $validator!==true ){
            	return response()->json($validator->messages(), 422);
            }

            $validator = \App\Http\Controllers\UserController::UpdateAvatar($request);
            if( $validator!==true ){
            	return response()->json($validator->messages(), 422);
            }
        }

        return ["state"=>"success"];
    }

    function Channel($id){
        $user = User::where("id",$id)->first();
        if( !$user ) abort(404);
       
        $Equery = Media::where("user_id",$user->id)->take(100);
        $Equery->orderby("updated_at","DESC");
        $medias = $Equery->get();

        return view('mobile.page.user_channel.data')
                ->with('medias', $medias)
                ->with('user',$user);
    }
}

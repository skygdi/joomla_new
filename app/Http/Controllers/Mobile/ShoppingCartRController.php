<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\PostCard;
use App\Models\ShoppingCartItem;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;

class ShoppingCartRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Equery_BA = Address::Self( Auth::id() );
        $Equery_BA->where("type","billing");
        $Equery_BA->orderBy("created_at","DESC");
        $BA = $Equery_BA->get();

        $BAD = Address::Self( Auth::id() )->where("type","billing")->Default()->first();

        $Equery_SA = Address::Self( Auth::id() );
        $Equery_SA->where("type","shipping");
        $Equery_SA->orderBy("created_at","DESC");
        $SA = $Equery_SA->get();

        $SAD = Address::Self( Auth::id() )->where("type","shipping")->Default()->first();

        $result = ShoppingCartItem::Self( Auth::id() )->orderby("id","DESC")->get();
        //$Config::get('paypal');
        return view('mobile.page.shopping_cart.data')
            ->with('PAYPAL_ENV',$_ENV['PAYPAL_ENV'])
            ->with('after_paid_url',url('/mobile#div_page_orders'))
            ->with('BA',$BA)
            ->with('SA',$SA)
            ->with('BAD',$BAD)
            ->with('SAD',$SAD)
            ->with('result',$result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postcard = PostCard::Self( Auth::id() )->where("id",$request->get('id'))->first();
        if( !$postcard ) abort(404);

        //var_dump($postcard->id);

        $SCI = ShoppingCartItem::Self( Auth::id() )->firstOrNew(["postcard_id"=>$postcard->id]);
        $SCI->user_id = Auth::id();
        $SCI->quantity++;
        $SCI->save();


        return ["state" =>  "success","id"  =>  $SCI->id ];
    }

    public function updates(Request $request){
        //dump( );
        $data = json_decode($request->get("item_info"),true);
        foreach ($data as $key => $value) {
            $id = $value["id"];
            $quantity = $value["quantity"];

            ShoppingCartItem::Self( Auth::id() )->where("id",$id)->update(["quantity"=>$quantity]);
        }
        return ["state" =>  "success"];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShoppingCartItem::Self( Auth::id() )->where("id",$id)->delete();

        return ["state" =>  "success" ];
    }
}

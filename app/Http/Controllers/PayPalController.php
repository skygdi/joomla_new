<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Api\OpenIdTokeninfo;
use PayPal\Api\OpenIdUserinfo;

use Auth;
use Config;
use Session;

use App\Models\Order;

class PayPalController extends Controller
{
	private $log = true;
    public function Create(Request $request){
    	$this->InitializeApiContext();
		//get last ID
		if( !$request->has("id") ){
			$obj_order = Order::Self(Auth::id())
							->Paid(false)
							->OrderBy("id","DESC")
							->first();
		}
		else $obj_order = Order::Self(Auth::id())
							->Paid(false)
							->Where("id",$request->get("id"))
							->OrderBy("id","DESC")
							->first();
		if( !$obj_order ) return ["state"=>"error","text"=>"no order"];
		

		//Paypal
		//$i = $this->CreateBase($obj_order->Total(),$obj_order->id);
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($_ENV['APP_URL']."/payment/paypal/return")
		    ->setCancelUrl($_ENV['APP_URL']."/payment/paypal/cancel");
		$amount = new Amount();
		$amount->setCurrency("USD")
		    ->setTotal($obj_order->Total());

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setDescription($obj_order->id)
			->setInvoiceNumber($_ENV['PAYPAL_ENV'].$obj_order->id);

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$obj_payment = new Payment();
		$obj_payment->setPayer($payer)
			->setIntent("sale")
			->setRedirectUrls($redirectUrls)
		    ->setTransactions([$transaction]);
		$i = $obj_payment->create($this->apiContext);

		//$_SESSION["ordering_id"] = $obj_order->id;
		Session::put('ordering_id', $obj_order->id);

		echo json_encode( array("id"=>$i->id) );
	}

	public function Execute(Request $request){
		$this->InitializeApiContext();
		/*
		$_POST["paymentID"] = "PAY-7V134197HY484634PLHTB35A";
        $_POST["payerID"] = "VLXCYC4VLPCKC";
        */
        //exit();
        if( Session::has('ordering_id') ){
        	$obj_order = Order::Self(Auth::id())->PaymentValidating(false)->where("id",Session::get('ordering_id'))->first();
        	if( $obj_order ){
        		$obj_order->payment_validating = true;
        		$obj_order->pay_type = "paypal";
				$obj_order->save();
        	}
        	
        }
        

        if( !$request->has("paymentID") || !$request->has("payerID") ) return ["state"=>"error","text"=>"parameter required"];

        $p = $this->ExecuteBase($request);
        //var_dump($p);exit();
        //var_dump($p->transactions[0]);exit();
        if( $p->state=="approved" ){
        	//Mark order as finished
        	$obj_order = Order::Self(Auth::id())->where("id",$p->transactions[0]->description)->first();
			if( !$obj_order ) return ["state"=>"error","text"=>"order not found"];
			$obj_order->payment_validating = 0;
			$obj_order->paid = 1;
			$obj_order->transaction_id = $_POST["paymentID"];
			$obj_order->save();

			/*
			PaymentTransation::Create(["order_id"	=>	$obj_order->id,
				"type"	=>	"paypal",
				"transaction_id"	=>	$_POST["paymentID"],
			]);
			*/

			return ["state"=>"success"];
			//$this->JsonShowSuccess();
        }
        //echo json_encode( $p );
	}

	public function ExecuteBase($request){
		
		$paymentID = $request->get('paymentID');
		$payerID = $request->get('payerID');

		//$this->error_msg = "test";
		//return;

		$payment = Payment::get($paymentID, $this->apiContext);
		

		try{
	    	$execution = new PaymentExecution();
	    	$execution->setPayerId($payerID);
	    	$result = $payment->execute($execution, $this->apiContext);
	    	$p = Payment::get($paymentID, $this->apiContext);
	    	return $p;
	    }
	    catch (\PayPal\Exception\PayPalConnectionException $ex) {
		    //echo $ex->getCode(); // Prints the Error Code
		    $ex_json = json_decode( $ex->getData(),true ); // Prints the detailed error message 
		    //$this->JsonShowError($ex_json["name"],$ex->getCode());
		    //$this->JsonShowError($ex_json["name"],200);

		    return ["state"=>"error","text"=>$ex_json["name"]];
		} 
	    catch (Exception $ex) {
        	//ResultPrinter::printError("Executed Payment", "Payment", null, null, $ex);
        	//var_dump($ex);
        	//$this->error_msg = "un";
		    return ["state"=>"error","text"=>"unknow"];
		    exit();
        	//exit(1);
    	}
	    
	}

	function __construct(){
		$this->clientId = Config::get('payment')["paypal"][$_ENV["PAYPAL_ENV"]]["clientId"];
		$this->clientSecret = Config::get('payment')["paypal"][$_ENV["PAYPAL_ENV"]]["clientSecret"];
	}

	private function InitializeApiContext(){
		$this->apiContext = new \PayPal\Rest\ApiContext(
		    new \PayPal\Auth\OAuthTokenCredential(
		        $this->clientId,     // ClientID
		        $this->clientSecret  // ClientSecret
		    )
		);

		if( $this->log ){
			$this->apiContext->setConfig(
		        array(
		            'log.LogEnabled' => true,
		            'log.FileName' => storage_path().'/logs/PayPal.log',
		            'log.LogLevel' => 'DEBUG',
		        )
		    );
	    }

	    if( $_ENV["PAYPAL_ENV"]=="production" ){
		    $this->apiContext->setConfig(
		      array(
		        'mode' => 'live',
		      )
			);
		}
	}
}

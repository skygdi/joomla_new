<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Traits\DesktopPage;
use App\Models\Address;

class AddressController extends Controller
{
    //use DesktopPage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = self::update_validator();

        if ($validator->fails())
        {
            return ["state" => "validate error","text"  =>  $validator->errors()->all() ];
        }
        //if( !Input::has('name') ) die("error");

        $par = Input::all();
        $par["user_id"] = Auth::id();
        $addr = Address::Create($par);
        $addr->save();

        return ['state' => 'success'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $addr = Address::Self(Auth::id())->find($id);
        return $addr;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = self::update_validator();

        if ($validator->fails())
        {
            return ["state" => "validate error","text"  =>  $validator->errors()->all() ];
        }
        
        $addr = Address::Self(Auth::id())
            ->where("id",$id)
            ->first();
        $addr->fill(Input::all());
        $addr->save();

        return ['state' => 'success'];

        
    }

    public function update_validator(){
        $validator = Validator::make(Input::all(), [
            'name' => 'required|string|min:3|max:255',
            'phone' => 'required|string|min:6|max:64',
            'email' => 'required|string|email|max:255',
            'address1' => 'sometimes|required|string|max:255',
            'address2' => 'sometimes|required|string|max:255',
            'city' => 'required|string|max:255',
            'state' => 'required|string|min:1|max:255',
            'country' => 'required|string|min:1|max:255',
        ]);
        return $validator;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Address::Self(Auth::id())
            ->where("id",$id)
            ->delete();
        return ['state' => 'success'];
    }

    
    public function ChangeDefault($id){
        $ad = Address::Self(Auth::id())
            ->where("id",$id)
            ->first();
        if( !$ad ) abort(404);

        Address::Self(Auth::id())
            ->where("type",$ad->type)
            ->update(["is_default"    =>  0]);

        Address::Self(Auth::id())
            ->where("id",$ad->id)
            ->update(["is_default"    =>  1]);

        return ["state" => "success"];
    }

    public function ChangeDefaultReturn($id){
        $ad = Address::Self(Auth::id())
            ->where("id",$id)
            ->first();
        if( !$ad ) abort(404);

        Address::Self(Auth::id())
            ->where("type",$ad->type)
            ->update(["display_as_return"    =>  0]);

        Address::Self(Auth::id())
            ->where("id",$ad->id)
            ->update(["display_as_return"    =>  1]);

        return ["state" => "success"];
    }

    /*
    function __construct(){
        $this->middleware(function ($request, $next) {
            $this->PageRenderInitialize();

            return $next($request);
        });
    }
    */
}

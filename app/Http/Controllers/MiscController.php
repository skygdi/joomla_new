<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use File;
use Response;

use App\Models\Media;

class MiscController extends Controller
{
    static function SearchWord($word=null){
    	if( $word!=null ){
    		//from get
    		Session::put('search_word', $word);
    	}
        else if( Input::has("word") ){
        	//from post
            Session::put('search_word', Input::get("word"));
        }
        else Session::put('search_word', "");

    	if( Input::has("ordering") ){
    		Session::put('search_ordering', Input::get("ordering"));
    	}
    	else Session::put('search_ordering', "newest");
    	
    }

    static function SearchPage($page=0){
    	//Word
        if( !Session::has('search_word') ){
            Session::put('search_word', "");
        }
        $word = Session::get('search_word');
        //$Equery = Media::Self( Auth::id() )->Searchable();
        $Equery = Media::Searchable();
        if( $word!="" ){
        	$Equery->where("name","LIKE","%".$word."%")
        		->OrWhereHas('KeyWord',function ($query) use ($word) {
        			$query->Where("name","like","%".$word."%");
	            });
        }

        //Order
        if( !Session::has('search_ordering') ){
            Session::put('search_ordering', "newest");
        }
        $ordering = Session::get('search_ordering');
        if( $ordering=="popular" ){
        	$Equery->Orderby("shows","desc");
        }
        else if( $ordering=="oldest" ){
        	$Equery->Orderby("created_at","asc");
        }
        else{
        	//newest
        	$Equery->Orderby("created_at","desc");
        }

        $max_per_page = 5;
        return $Equery->Paginate($max_per_page);
        //Media::Self( Auth::id() )->where("name","LIKE","%".Input::get("word")."%");
    }

    function Slider($filename){
        $path = storage_path().'/app/images/slider/'.$filename;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        $response->header("Cache-Control", " private, max-age=12800");
        return $response;
    }

}

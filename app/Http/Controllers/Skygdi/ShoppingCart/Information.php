<?php

namespace App\Http\Controllers\Skygdi\ShoppingCart;

//use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
//extends Controller
//use App\User;
use Auth;
use DB;

class Information
{
	static public function MiniCartListAndSumarry(){
		$final_array = array();
		$data_array = self::MiniCartList();
		$sumarry = self::MiniCartSumarryCount($data_array);
		$final_array["data"] = $data_array;
		$final_array["sumarry"] = $sumarry;
		return $final_array;
	}

	static public function MiniCartList(){
		$data_array = array();
		if( !Auth::check() ) return $data_array;

		$Equery = DB::table("zoo_zl_zoocart_cartitems");
		$Equery->where("user_id","=",Auth()->id());
		$Equery->select("id","item_id","quantity");
		$Equery->orderby("id","DESC");
		$result_cartitems = $Equery->get();

		foreach( $result_cartitems as $cartitems ){
			$data = array();
			//$obj_item = Item::where("id","=",$cartitems->item_id)->first();
			$obj_item = \Skygdi\Model\PostCard::find($cartitems->item_id);

			$data["id"] = $cartitems->id;
			$data["name"] = $obj_item->name;
			//$data["price"] = $obj_item->item_price();
			$data["price"] = $obj_item->Price();
			$data["quantity"] = $cartitems->quantity;
			$data["total"] = number_format($data["price"]*$data["quantity"], 2, '.', '');

			$data_array[] = $data;
		}
		return $data_array;
	}

	static public function MiniCartSumarry(){
		$data_array = self::MiniCartList();
		return self::MiniCartSumarryCount($data_array);
	}

	static public function MiniCartSumarryCount(&$data_array){
		$count = 0;
		$total = 0;
		foreach( $data_array as $data ){
			//$count += $data["quantity"];
			$total += $data["price"]*$data["quantity"];
		}
		$count = count($data_array);
		return [$count,number_format($total, 2, '.', '')];
	}

	function __construct(){
		
	}
}

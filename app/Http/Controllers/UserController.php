<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use File;
use Auth;
use Validator;
use Response;
use Session;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public static function KeepAlive(){
        //Auth::User()->save();
        if( Auth::check() ){
            Auth::User()->touch();
            return ["state"=>"success"];
        }
        else return ["state"=>"timeout"];
        
    }

    public static function Update(){
        //dump(Auth::User()->profile);
        if( Auth::User()->profile==null ){
            Auth::User()->Profile()->create(["user_id"=>Auth::id()]);
            
            //Auth::login(Auth::User());
            //dump(Auth::User()->Profile()->first());
        }
        $profile = Auth::User()->Profile()->first();
        //Auth::User()->Profile()->first()->update(["paypal"=>"kk@1.com"]);
        
        //$k->paypal = "k.";
        //dd( $k );
    	//Common validation and save
        $validator = Validator::make(Input::all(), [
            'email' => [
                'required',
                'min:6',
                'max:255',
                Rule::unique('users')->ignore(Auth::User()->id),
            ],
            'name' => 'sometimes|nullable|max:128',
            'password' => 'sometimes|nullable|max:16|confirmed',
            'paypal' => 'required|max:255|unique:users_profile,paypal,' . $profile->id,
        ]);
        //Auth::User()->profile->id,
        if( !$validator->errors()->has('email') ) Auth::User()->email = Input::get('email');
        if( !$validator->errors()->has('password') && strlen(Input::get('password'))>0 ) Auth::User()->password = bcrypt( Input::get('password') );
        if( !$validator->errors()->has('paypal') ) $profile->paypal = Input::get('paypal');
        if( !$validator->errors()->has('name') ) Auth::User()->name = Input::get('name');
        $profile->description = Input::get('description');
        Auth::User()->save();
        $profile->save();
        if ($validator->fails())
        {
            //return response()->json($validator->messages(), 422);
            return $validator;
        }
        else return true;
    }

    public static function UpdateAvatar(Request $request){
    	//Avatar image
        $validator = Validator::make(Input::all(), [
            'avatar_file' => 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif|min:1|max:1000',
        ]);

        if ($validator->fails())
        {
            //return response()->json($validator->messages(), 422);
            return $validator;
        }

        if( !$validator->fails() && $request->hasFile('avatar_file') ){
            Auth::User()->SaveAvatar($request);
        }

        return true;
    }

    function Avatar($id=null){
        if( $id!=null ){
            $user = User::find($id);
            if( !isset($user->profile->avatar) ) abort(404);
            $path = storage_path().'/app/images/avatar/'.$user->profile->avatar;
        }
        else{
            $path = storage_path().'/app/images/avatar/'.Auth::User()->profile->avatar;
        }

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    function Language($language){
        if( !($language=="cn" || $language=="en") ) return "error";
        Session::put('locale',$language);
        return "success";
    }
}

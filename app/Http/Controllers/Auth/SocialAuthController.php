<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallbackFacebook()
    {
    	$social_key = "facebook_id";
        
        
        $user = Socialite::driver('facebook')->stateless()->user();
        //dump($user);

        // All Providers
        $id = $user->getId();
        $nickname = $user->getNickname();
        $name = $user->getName();
        $email = $user->getEmail();
        $avatar = $user->getAvatar();


        //dump(1);
        $user = $this->RegisterUser($social_key,$id,$nickname,$name,$email,$avatar);
        if( !$user ) return;
        //$user->update([$social_key	=>	$id]);
        
        //Login
        Auth::login($user,true);
        //Return to /
        return \Redirect::to('/');

        // $user->token;
    }
    

    public function handleProviderCallbackGoogle()
    {
    	$social_key = "google_id";
        //dump(0);
        //Socialite::driver('google')->stateless()->user();
        //$user = Socialite::driver('google')->user();
        
        $user = Socialite::driver('google')->stateless()->user();
        //dump($user);

        // All Providers
        $id = $user->getId();
        $nickname = $user->getNickname();
        $name = $user->getName();
        $email = $user->getEmail();
        $avatar = $user->getAvatar();
        /*
        dump($id);
        dump($nickname);
        dump($name);
        dump($email);
        dump($avatar);
        */
       
        /*
        //Debug
        $avatar = "https://lh5.googleusercontent.com/-qJFFXwwe3Nw/AAAAAAAAAAI/AAAAAAAAABI/wUJTk7st1HI/photo.jpg?sz=50";
        $id = "108553467880576988988";
        $nickname = "skygdi";
        $name = "Peter George";
        $email = "skygdi834@gmail.com";
        */


        //dump(1);
        $user = $this->RegisterUser($social_key,$id,$nickname,$name,$email,$avatar);
        if( !$user ) return;
        //$user->update([$social_key	=>	$id]);
        
        //Login
        Auth::login($user,true);
        //Return to /
        return \Redirect::to('/');

        // $user->token;
    }

    function RegisterUser($social_key,$social_id,$nickname,$name,$email,$avatar){
    	$user = User::where($social_key,$social_id)->first();
    	if( !$user ){
    		$user = User::where("email",$email)->first();
    		//Update the existing account social key
    		if( $user ) $user->update([$social_key	=>	$social_id]);
    	}
    	if( $user ){
    		return $user;
    	}

    	$pars = array();
    	$pars["username"] = $email;
    	$pars["name"] = $name;
    	$pars[$social_key] = $social_id;
    	$pars["email"] = $email;

    	$user = User::create($pars);
    	$user->SaveAvatarFromUrl($avatar);

    	return $user;

    }
}

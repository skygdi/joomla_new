<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Input;
use Auth;
use Request;
//use Session;

use App\Traits\DesktopPage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers;

    use DesktopPage;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    public function showLoginForm()
    {
        $this->PageRenderInitialize();
        return view('desktop.auth.login')->with('vars', $this->vars);
        //return view('desktop.auth.login');
    }

    public function logout()
    {
        if( Auth::check() ){
            Auth::logout();
        }

        if (Request::ajax()){
            return "success";
        }
        else{
            return \Redirect::to($this->redirectTo);
        }

        //return "success";
    }

    /**
     * The user has been authenticated.
     * If the user had logon, it won't be fired.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    ///*
    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {

        if( $user->isAdmin() && $request->get('for_backend') ){
            //Session::put('for_backend', "1");
            //return redirect()->intended('admin/dashboard');
            return redirect()->intended('admin/order');
        }
        if ($request->ajax()){
            return response()->json([
                'auth' => auth()->check(),
                'user' => $user,
            ]);

        }
        
        
    }
    //*/
}

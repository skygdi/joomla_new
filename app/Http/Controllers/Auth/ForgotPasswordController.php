<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use Session;
use Validator;
use Redirect;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Password;

use App\Traits\DesktopPage;
use App\Notifications\ForgotUsername;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    use DesktopPage;

    
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        //$this->validateEmail($request);
        $this->LanguageInitialize();
        //Validate
        $validator = Validator::make(Input::all(), [
            'email' => 'required|string|min:3|max:255|exists:users',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    
    

    /*
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|min:3|max:255|exists:users',
                'g-recaptcha-response' => 'required|captcha',
        ]);
    }
    */

    
    

    public function showLinkRequestForm()
    {
        $this->PageRenderInitialize();
        return view('desktop.auth.passwords.email')->with('vars', $this->vars);
    }

    public function showForgotUsernameForm(){
        $this->PageRenderInitialize();
        return view('desktop.auth.forgot_username')->with('vars', $this->vars);
    }

    public function sendEmailToUser(){
        $this->LanguageInitialize();

        //Validate
        $validator = Validator::make(Input::all(), [
            'email' => 'required|string|min:3|max:255|exists:users',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        //Send notification
        $user = User::where("email",Input::get("email") )->first();
        $user->notify( new ForgotUsername($user->email) );
        
        Session::flash('status', __('Email sent'));

        return Redirect::back();
    }
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use DB;

use App\Models\Media;
use App\Models\MediaOld;
use App\Models\PostCard;
use App\Models\ShoppingCartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\MediaKeyword;
use App\Models\UserProfile;
use App\User;


use App\Http\Controllers\Desktop\ShoppingCartController;


class Upgrade extends Controller
{
    function All(){
    	$u = User::find(40);//40
    	//dd($u->Profile()->first()->avatar);
    	//dd($u->Profile1->avatar);
    	//dd($u->Profile1->avatar);
    	dd( $u->Profile()->exists() );
    	//dd($u->Profile);
    	
    	//echo Order::find(6)->ShippingFee();
    	/*
    	$SCI = ShoppingCartItem::Self( Auth::id() )->get();
    	$total = $SCI->sum(function ($item){
                return  $item->quantity;
             });
        $prices = $SCI->sum(function ($item){
                return ($item->PostCard()->first()->Price() * $item->quantity );
             });
        dump( ShoppingCartController::ItemList() );
        */
       
        //dump($total);
        //dump($prices);
    	/*
    	$k = PostCard::all()->sum(function ($item){
                return $item->Price();
             });
    	dump($k);
    	*/
    	

    	/*
    	$this->ConverPostCardFromOldMedia();
		$this->Media1();
		$this->CopyMediaPropety();
		$this->CopyAddress();
		*/
		//$this->CopyOrder();
		//$this->CopyOrderItem();
		//$this->CopyKeyword();
		//
		//$this->CopyUsersProfiles();
	}

	function CopyUsersProfiles(){
		$result = DB::table('user_profiles')->select("user_id")->groupBy("user_id")->get();
		foreach( $result as $row ){
			$data = array();
			$data_result = DB::table('user_profiles')->where("user_id",$row->user_id)->get();

			foreach( $data_result as $u_data ){
				if( $u_data->profile_key=="avatar" ){
					$data["avatar"] = $u_data->profile_value;
				}
				else if( $u_data->profile_key=="paypal" ){
					$data["paypal"] = $u_data->profile_value;
				}
				else if( $u_data->profile_key=="description" ){
					$data["description"] = $u_data->profile_value;
				}
			}
			//echo count($data);
			if( count($data)==0 ) continue;

			$data["user_id"] = $row->user_id;
			
			$ret = UserProfile::where("user_id",$row->user_id)->first();
			//dump($ret);
	        if( $ret ) continue;
	        //dump($data);
	        UserProfile::create($data);

			//UserProfile::where("user_id",$row->user_id)->update($data);
			
		}
	}

	function CopyKeyword(){
		$result = DB::table('zoo_tag')->get();
		foreach( $result as $row ){
			//MediaKeyword::
			$data["id"] = $row->id;
			$data["media_id"] = $row->item_id;
			$data["name"] = $row->name;

			$ret = MediaKeyword::find($row->id);
	        if( $ret ) continue;
	        MediaKeyword::create($data);
		}
	}

	function CopyOrderItem(){
		$result = DB::table('zoo_zl_zoocart_orderitems')->get();
		foreach( $result as $row ){
			$data = array();

			$data["name"] = $row->name;
			$data["quantity"] = $row->quantity;
			$data["price"] = $row->price;
			$data["postcard_id"] = $row->item_id;

			$order = Order::find($row->order_id);
			//if( $order==null ) continue;
			$data["order_id"] = $order->id;
			$data["id"] = $row->id;

			//$postcard = PostCard::find($data["postcard_id"]);
			$postcard = PostCard::where("tmp",$data["postcard_id"])->first();
			if( $postcard==null ){
				continue;
				//dd($row->id);
				//dd($postcard);
			}
			$data["content"] = $postcard->content;

			$e = json_decode($row->elements,true);
			//echo $row->id;
			//dd($e);

			$data["media_id"] = @$e["536c2dbe-8287-4e2c-abde-34203a619fa4"]["item"][0];
			if( $data["media_id"]==null ) continue;
			if( $data["media_id"]!=null ){
				//dump($data["media_id"]);
				//dump($row->id);
				$media = Media::find($data["media_id"]);
				if( $media==null ) continue;
				$data["file"] = $media->file;
			}

			//OrderItem::create($data);
			$ret = OrderItem::find($row->id);
	        if( $ret ) continue;
	        OrderItem::create($data);

			
			//break;
			
		}
	}

	function CopyOrder(){
		$result = DB::table('zoo_zl_zoocart_orders')->get();
		//$result = DB::table('zoo_zl_zoocart_orders')->where("id",166)->get();
		foreach( $result as $row ){
			$data = array();


			//shipping address
			$e = json_decode($row->shipping_address,true);
			$data["SA_name"] = @$e["elements"]["033d9bcd-ace8-4443-af02-2c32b4215dc2"][0]["value"];
			$data["SA_address1"] = @$e["elements"]["7f20cfe3-08c5-4c4a-aebf-39ff24a67b7f"][0]["value"];
			$data["SA_address2"] = @$e["elements"]["8c82ae89-9f62-467f-8194-794655e91a81"][0]["value"];
			$data["SA_city"] = @$e["elements"]["b5657ffa-6b78-4011-a524-474c9ffa5f11"][0]["value"];
			$data["SA_country"] = @$e["elements"]["dca5d838-57c8-407d-b705-63176d234c27"][0]["value"];

			//dd($data);
			if($row->shipping_address_id!=0){
				$data_t = $this->GetOldAddress($row->shipping_address_id);
				//echo $row->shipping_address_id;
				//echo $row->id;
				//dump($data_t->name);

				if( $data_t->name!=null ){
					$data["SA_name"] = $data_t->name;
					$data["SA_address1"] = $data_t->address1;
					$data["SA_address2"] = $data_t->address2;
					$data["SA_city"] = $data_t->city;
					$data["SA_country"] = $data_t->country;

					$data["SA_phone"] = $data_t->phone;
					$data["SA_email"] = $data_t->email;
					$data["SA_state"] = $data_t->state;
					$data["SA_zipcode"] = $data_t->zipcode;
				}
			}
			//dd($data);

			//billing address
			$e = json_decode($row->billing_address,true);
			$data["BA_name"] = @$e["elements"]["033d9bcd-ace8-4443-af02-2c32b4215dc2"][0]["value"];
			$data["BA_address1"] = @$e["elements"]["7f20cfe3-08c5-4c4a-aebf-39ff24a67b7f"][0]["value"];
			$data["BA_address2"] = @$e["elements"]["8c82ae89-9f62-467f-8194-794655e91a81"][0]["value"];
			$data["BA_city"] = @$e["elements"]["b5657ffa-6b78-4011-a524-474c9ffa5f11"][0]["value"];
			$data["BA_country"] = @$e["elements"]["dca5d838-57c8-407d-b705-63176d234c27"][0]["value"];

			//default country
			if( $data["SA_country"]==null ) $data["SA_country"] =  "US";
			if( $data["BA_country"]==null ) $data["BA_country"] =  "US";

			if($row->billing_address_id!=0){
				$data_t = $this->GetOldAddress($row->billing_address_id);

				if( $data_t->name!=null ){
					$data["BA_name"] = $data_t->name;
					$data["BA_address1"] = $data_t->address1;
					$data["BA_address2"] = $data_t->address2;
					$data["BA_city"] = $data_t->city;
					$data["BA_country"] = $data_t->country;

					$data["BA_phone"] = $data_t->phone;
					$data["BA_email"] = $data_t->email;
					$data["BA_state"] = $data_t->state;
					$data["BA_zipcode"] = $data_t->zipcode;
				}
			}
			
			$data["tmp"] = $row->id;
			$data["user_id"] = $row->user_id;
			$data["pay_type"] = $row->payment_method;
			$data["id"] = $row->id;

			if( $row->state==1 ){
			}
			else if( $row->state==2 ){
				$data["paid"] = 1;
			}
			else if( $row->state==4 ){
				$data["paid"] = 1;
				$data["delivered"] = 1;
			}
			else if( $row->state==7 ){
				$data["cancel"] = 1;
			}
			else if( $row->state==9 ){
				$data["payment_validating"] = 1;
			}

			$data["created_at"] = $row->created_on;

			/*
			$ret = Order::firstOrNew(
	            //['created_at' => $row->created_on , 'id'	=> $row->id ]
	            ['id'	=> $row->id ]
	        );
	        $ret->save();
	        $ret->update($data);
	        */
	        $ret = Order::find($row->id);
	        if( $ret ) continue;
	        Order::create($data);
	        
		}
	}

	function GetOldAddress($id){
		//$result = DB::table('skygdi_address')->select(["name","phone","email","address1","address2","city","state","zipcode","country"])
		$result = DB::table('skygdi_address')->where("id",$id)
			->first();
		//dump($result);
		return $result;
		//if( !$result ) return $result;
	}

	function CopyAddress(){
		$result = DB::table('skygdi_address')->where("for_order","0")->get();
		foreach( $result as $row ){
			$mc = \App\Models\Address::firstOrNew(array('tmp' => $row->id));
			/*
			$mc->user_id = $row->created_by;
			$mc->media_id = $row->item_id;
			$mc->content = $row->content;
			*/
			//dump($row);
			//dump($row->toArray());
			//$mc->fill($row);
			$mc->is_default = $row->is_default;
			$mc->display_as_return = $row->display_as_return_address;
			$mc->type = $row->type;
			$mc->user_id = $row->user_id;
			$mc->name = $row->name;
			$mc->phone = $row->phone;
			$mc->email = $row->email;
			$mc->address1 = $row->address1;
			$mc->address2 = $row->address2;
			$mc->city = $row->city;
			$mc->state = $row->state;
			$mc->zipcode = $row->zipcode;
			$mc->country = $row->country;

			$mc->created_at = $row->created_at;
			$mc->updated_at = $row->updated_at;
			$mc->tmp = $row->id;
        	$mc->save();
        }
	}

	function CopyMeidaComment(){
		$result = DB::table('zoo_item_comment')->get();
		foreach( $result as $row ){
			$mc = \App\Models\MediaComment::firstOrNew(array('tmp' => $row->id));
			$mc->user_id = $row->created_by;
			$mc->media_id = $row->item_id;
			$mc->content = $row->content;
			$mc->created_at = $row->datetime;
			$mc->tmp = $row->id;
        	$mc->save();
        }
	}
	function CopyMediaPropety(){
		$this->CopyShowFromPropety();
		$this->CopyLikeFromPropety();
		$this->CopyCommentFunction();
		$this->CopyMeidaComment();
	}

	function CopyCommentFunction(){
		$result = DB::table('zoo_item_property')->where("name","disable_comment")->get();
        foreach( $result as $row ){
        	$m = Media::find($row->item_id);
        	if( !$m ) continue;
        	$m->disable_comment = $row->value;
        	$m->save();
        }
	}
	
	function CopyLikeFromPropety(){
		$result = DB::table('zoo_item_property')->where("name","like")->get();
        foreach( $result as $row ){
        	$p = \App\Models\MediaLike::firstOrNew(array('media_id' => $row->item_id,'user_id'=>$row->created_by));
        	$p->save();
        }
	}

	/**
	 * After update user_id in table: media and postcard
	 */
	function CopyShowFromPropety(){
		$result = DB::table('zoo_item_property')->where("name","click")->get();
        foreach( $result as $row ){
        	$m = Media::find($row->item_id);
        	//dd($m);
        	if( !$m ) continue;
        	$m->shows = $row->value;
        	$m->save();
        }
	}

	function ConverPostCardFromOldMedia(){
		$p_result = \App\Models\MediaOld::withoutGlobalScopes()->where('type', 'postcard')->where("original_media_id","!=","0")->get();
		foreach( $p_result as $o_p ){
			$elements = json_decode($o_p->elements,true);
			$content = "";
	        if( isset( $elements["910c3b75-0e61-4aa2-b670-17e2b7e68ba0"][0] ) ){
	            $content = $elements["910c3b75-0e61-4aa2-b670-17e2b7e68ba0"][0]["value"];
	        }

	        //dd($o_p);
	        //PostCard::create()
	        $p = PostCard::firstOrNew(array('tmp' => $o_p->id));
			$p->content = substr($content, 0,254);
			$p->name = $o_p->name;
			$p->media_id = $o_p->original_media_id;
			//$p->user_id = $o_p->user_id;
			$p->user_id = $o_p->created_by;

			$p->save();
	        
		}
		//echo $p_result->count();

	}

	private function Media1(){
		$this->FixMediaDBFile();
		//return;
		$this->CopyPics();
		$this->MediaRemove404();
		$this->CopyNewMedia();
		$this->CopyAvatar();
		
	}

	function CopyNewMedia(){
		$m_result = \App\Models\MediaOld::withoutGlobalScopes()->where('type', 'media')->whereNotNull('media_file')->get();
		foreach( $m_result as $m ) {
			//dd($m);
	        $n_m = Media::firstOrNew(array('id' => $m->id));
			$n_m->name = $m->name;
			$n_m->id = $m->id;
			$file_name = pathinfo($m->media_file, PATHINFO_BASENAME);
			$n_m->file = $file_name;
			$n_m->searchable = $m->searchable;
			//$n_m->user_id = $m->user_id;
			$n_m->user_id = $m->created_by;
			$n_m->created_at = $m->created;
			$n_m->save();
	    }
	}

	function CopyAvatar(){
		$dest_dir = storage_path()."/app/images/avatar";
        $base_path = public_path()."/images/avatar";

        $dp = dir($base_path);
        while($file=$dp->read()){
            if( $file=='.' || $file=='..' ) continue;
            if( $file==".gitignore" ) continue;
            $dest = $dest_dir."/".$file;
            $source = $base_path.'/'.$file;
            if (!file_exists(dirname($dest))) {
	            mkdir(dirname($dest), 0777,true);
	        }
            if( file_exists($dest) ){
            	continue;
            }
            copy($source,$dest);
        }
	}

	function MediaRemove404(){
		$m_result = \App\Models\MediaOld::all();
	    foreach( $m_result as $m ) {
	    	$file = base_path()."/storage/app/".$m->media_file;
	    	if( !file_exists($file) ){
	    		$m->media_file = null;
		    	$m->save();

	    	}
	    }
	}

    function FixMediaDBFile(){
    	//$m_result = \App\Models\Media::all();
    	//$m_result = \App\Models\Media::whereNotNull("elements")->get();
    	$m_result = \App\Models\MediaOld::withoutGlobalScopes()->where('type', 'media')->get();
    	//echo $m_result->count();
    	//exit();
	    foreach( $m_result as $m ) {
	    	//echo $m->id;
	    	$m->media_file = null;
	    	$m->save();
	    	//exit();
	        $m->FixOldPicture();
	        //exit();
	        /*
	        if( $m->id==112 ){
	        	dump($m->media_file);
	        }
	        */

	    }
	    echo "Pics fixed<BR>";
    }

    function CopyPicTest(){
    	$m = \App\Models\MediaOld::where("id",96)->first();
    	echo $m->media_file;
    }

    function CopyPics(){

    	//$m_result = \App\Models\MediaOld::all();
    	$m_result = \App\Models\MediaOld::withoutGlobalScopes()->where('type', 'media')->get();
    	foreach( $m_result as $m ){
    		$file = $m->media_file;
		    $file_name = pathinfo($file, PATHINFO_BASENAME);
		    $source = public_path()."/".$file;


		    $dest = base_path()."/storage/app/images/media/".$file_name;

		    $new_DB_file_name = "images/media/".$file_name;
		    //dd($new_DB_file_name);

		    if( $file_name=="" ){
		    	$m->media_file = null;
		    	$m->save();
		    	continue;
		    }
			

		    if( !file_exists($source) ){
		    	echo "n: ".$m->id." ".$m->media_file."<BR>";
		    	$m->media_file = null;
		    	$m->save();

		    	continue;
		    }

		    //if( $m->id==230 ) dump($dest);
		    if( file_exists($dest) ){
		    	echo "c: ".$m->id."<BR>";
		    	$m->media_file = $new_DB_file_name;
		    	$m->save();
		    	continue;
		    }

		    //if( $m->id==230 ) dump("mso");

		    $m->media_file = $new_DB_file_name;
		    $m->save();


		    if (!file_exists(dirname($dest))) {
	            mkdir(dirname($dest), 0777,true);
	        }
		    //dump($dest);

		    copy($source,$dest);
    	}


    }
}

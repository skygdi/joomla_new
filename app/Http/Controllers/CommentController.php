<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Response;
use Auth;

use App\Models\Media;
use App\Models\MediaComment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Input::get("id");

        $media = Media::where("id",$id)->first();
        if( !$media ){
            //return response()->json(['result_string' => 'media not exist']);
            return ['state' =>  'media not exist'];
        }

        if( $media->user_id!=Auth::id() && $media->disable_comment==1 ){
            //return response()->json(['result_string' => 'no permission']);
            return ['state' =>  'no permission'];
        }
        $comment = new MediaComment(['media_id' => $media->id,"user_id" => Auth::id() , "content" => Input::get("content")]);
        $comment->save();
        //$media->Comment()->save($comment);
        
        //return response()->json(['state' => 'success',"id"=>$id,"comment"=>$comment]);
        return ['state' => 'success',"id"=>$id,"comment"=>$comment];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MediaComment::Self(Auth::id())->where("id",$id)->delete();
        //return response()->json(['state' => 'success']);
        return ['state' => 'success'];
    }

    function Permission($id,$value){
        $m = Media::where("id",$id)
                ->Self(Auth::id())
                ->first();
        if( !$m ) die("error");

        if( $value==1 ) $m->disable_comment = 1;
        else $m->disable_comment = 0;

        $m->save();

        return ['state' => 'success'];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ShoppingCartItem;

class OrderController extends Controller
{
    public static function store($item_array,$addresses)
    {

        //Save to shopping cart first
        foreach( $item_array as $item ){
            $si = ShoppingCartItem::Self( Auth::id() )->where("id",$item->id)->first();
            if( !$si ) continue;
            $si->quantity = $item->quantity;
            $si->save();
        }

        $billing_address = Address::Self( Auth::id() )->Billing()->where("id",$addresses["billing"])->first()->toArray();
        $shipping_address = Address::Self( Auth::id() )->Shipping()->where("id",$addresses["shipping"])->first()->toArray();

        $data["BA_name"]        = $billing_address["name"];
        $data["BA_phone"]       = $billing_address["phone"];
        $data["BA_email"]       = $billing_address["email"];
        $data["BA_address1"]    = $billing_address["address1"];
        $data["BA_address2"]    = $billing_address["address2"];
        $data["BA_city"]        = $billing_address["city"];
        $data["BA_state"]       = $billing_address["state"];
        $data["BA_zipcode"]     = $billing_address["zipcode"];
        $data["BA_country"]      = $billing_address["country"];

        $data["SA_name"]        = $shipping_address["name"];
        $data["SA_phone"]       = $shipping_address["phone"];
        $data["SA_email"]       = $shipping_address["email"];
        $data["SA_address1"]    = $shipping_address["address1"];
        $data["SA_address2"]    = $shipping_address["address2"];
        $data["SA_city"]        = $shipping_address["city"];
        $data["SA_state"]       = $shipping_address["state"];
        $data["SA_zipcode"]     = $shipping_address["zipcode"];
        $data["SA_country"]      = $shipping_address["country"];

        $o = Order::create([
            'user_id'   => Auth::id(),
        ]);
        $o->fill($data);
        $o->save();

        foreach( $item_array as $item ){
            $si = ShoppingCartItem::Self( Auth::id() )->where("id",$item->id)->first();
            if( !$si ) continue;

            $oi = OrderItem::create([
                'order_id'      => $o->id,
                'postcard_id'   => $si->postcard_id,
                'quantity'      => $si->quantity,
                'price'         => $si->PostCard()->first()->Price(),
                'name'          => $si->PostCard()->first()->name,
                'content'       => $si->PostCard()->first()->content,
                'file'          => $si->PostCard()->first()->Media()->first()->file,
                
            ]);
            $oi->save();

        }

        //remove shopping cart
        ShoppingCartItem::Self( Auth::id() )->delete();
        //return ['state'=>'success'];
        return true;
    }
}

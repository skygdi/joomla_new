<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\App;
use Session;
use Auth;
//use App\Http\Controllers\Desktop\ShoppingCartController;

class Locale
{
    private $vars;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Language
        $locale = Session::get('locale');
        if( $locale==null ) $locale = App::getLocale();
        App::setLocale($locale);
        

        return $next($request);
    }
}

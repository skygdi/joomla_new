<?php
namespace App\Skygdi\Item;
use DB;

class MediaHelper{
	public function __construct() {
        
    }

	public $result_flag;
	public $result_string;
	public $uid;

	
	public function CommentRemove(){
		\Skygdi\Model\ItemComment::where("id",$_GET["id"])->delete();
		die("success");
	}

	public function ItemRemove(){
		\Skygdi\Model\Item::where("id",$_GET["id"])->delete();
		\Skygdi\Model\ItemComment::where("item_id",$_GET["id"])->delete();
		\Skygdi\Model\ItemProperty::where("item_id",$_GET["id"])->delete();
		die("success");
	}

	public function CommentSubmit2(){
		$new_comment_creator = $_SESSION["user"]->id;
		$content = $_POST["content"];

		$item = \Skygdi\Model\Item::where("id",$_POST["id"])->where("type","media")->first();
		if( !$item ) $this->Done(false,"item not exist");

        if( $new_comment_creator!=$item->created_by && $item->PropertyDisableComment()=="true" ) $this->Done(false,"no permission");

        \Skygdi\Model\ItemComment::create([
            "item_id"       =>  $item->id,
            "created_by"    =>  $new_comment_creator,
            "content"       =>  $content,
        ]);

    	$this->Done(true,"success");
	}

	public function CommentSubmit(){
		//
		\Skygdi\Model\ItemComment::insert(
			array(	'created_by' 	=> $_SESSION["user"]->id,
					'item_id' 	=> $_GET["id"],
					'content' 		=> $_POST["content"],
					'datetime' 	=> date("Y-m-d H:i:s"),
		));

		$this->vars["MLP"] = new \Skygdi\MultipleLanuage(@$_SESSION["language"]);
        $this->vars["MLP"]->Load("pages.php");

        $this->vars["result_flag"] = true;

        $this->vars["result_string"] = $this->vars["MLP"]->Read("media-management","Comment submit success");


        //get comment and show with twig render
        $current_comment = \Skygdi\Model\Item::where("id",$_GET["id"])->first();
        $this->vars["comments"] = \Skygdi\Model\ItemComment::where("item_id",$current_comment->id)
	        ->where("created_by",$current_comment->created_by)
	        ->orderby("datetime","DESC")
	        ->get();

        $this->vars["item_id"] = $current_comment->id;
        $device = "desktop";
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../templates/front/'.strtolower($device).'/');
		$view = new \Twig_Environment($loader, array(
			'cache' => false,
		));
        $this->vars["comments"] = $view->render('partials/comments.html',$this->vars);

        echo json_encode($this->vars);
        exit();
        //echo $this->vars["result_string"];
	}

	public function LikeChange(){
		if( $_GET["v"]=="n" ){
	        \Skygdi\Model\ItemProperty::updateOrCreate(
	            [
	                "created_by"    => $_SESSION["user"]->id,
	                "item_id"       => $_GET["id"],
	                "name"          => "like",
	            ],

	            [   "value"         => 0
	            ]
            );
	    }
	    else{
	        $Equery = \Skygdi\Model\ItemProperty::query();
	        $Equery->where("created_by"   , $_SESSION["user"]->id);
	        $Equery->where("item_id"   , $_GET["id"]);
	        $Equery->where("name"   , "like");
	        $Equery->delete();
	    }

	    die("success");
	}
	public function CommentPermission(){
		\Skygdi\Model\ItemProperty::updateOrCreate(
	        [
	            "created_by"    => $_SESSION["user"]->id,
	            "item_id"       => $_GET["id"],
	            "name"          => "disable_comment",
	        ],

	        [   "value"         => $_GET["v"]
	        ]
	    );

	    die("success");
	}

	public function Listing(){
		$Equery = \Skygdi\Model\Item::where("created_by",$this->uid);
		$Equery->where("type","media");
		//$Equery->orderby("modified","DESC");
		$Equery->orderby("id","DESC");
		

		$page = 0;

		if( !isset($_GET["page"]) || !is_numeric($_GET["page"]) ) $page = 1;
		else $page = $_GET["page"];
		$result = $Equery->paginate(20,['*'],"page",$page);
		
		return $result;
	}

	public function EditMedia(){
		$this->result_flag = false;
		$this->result_string = null;

		if( !isset($_POST["name"]) ){
			return;
		}

		$elements = "";
		if( strlen(@$_FILES["file_image"]["name"])!=0 ){
			$ret = false;
	    	$file = $this->UploadImage($ret,$this->uid);
	    	if( !$ret ){
	    		$this->result_string = "Image store fails.";
	    		return;
	    	}
			$elements = $this->Elements($file);
		}

		$i = \Skygdi\Model\Item::where("id",$_POST["id"])->first();
		if( !$i ){
			$this->result_string = "Item not found";
			return;

		}

		if( !is_numeric($_POST["is_public"]) ){
			if( $_POST["is_public"]=="on" ) $_POST["is_public"] = 1;
			else $_POST["is_public"] = 0;
		}

		$i->name = $_POST["name"];
		$i->searchable = $_POST["is_public"];
		if( $elements!="" ) $i->elements = $elements;
		$i->save();

		//keyword

		if( isset($_POST["tags"]) ){
			//keyword clean
			DB::table("zoo_tag")->where("item_id",$i->id)->delete();
			$this->InsertKeyWords2($_POST["tags"],$i->id);
		}
		else{
    		$this->InsertKeyWords($_POST["keywords"],$i->id);
    	}

    	/*
		//keyword clean
		DB::table("zoo_tag")->where("item_id",$_POST["id"])->delete();
		$this->InsertKeyWords($_POST["keywords"],$i->id);
		*/

    	$this->result_flag = true;
    	$this->result_string = "Success";

	}

	public function Insert(){
		$this->result_flag = false;
		$this->result_string = null;

		if( !isset($_POST["name"]) ){
			//$this->result_string = "Missing post message";
			return;
		}

    	//var_dump($_POST);
    	$ret = false;
    	$file = $this->UploadImage($ret,$this->uid);
    	if( !$ret ){
    		$this->result_string = "Image store fails.";
    		return;
    	}
		$elements = $this->Elements($file);

		$alias = str_replace(" ","-",strtolower($_POST["name"]));
		$alias = $this->GetAlias($alias);

		if( !is_numeric($_POST["is_public"]) ){
			if( $_POST["is_public"]=="on" ) $_POST["is_public"] = 1;
			else $_POST["is_public"] = 0;
		}

		$i = new \Skygdi\Model\Item;
		$i->application_id = 2;
		$i->type = "media";
		$i->name = $_POST["name"];
		$i->alias = $alias;
		$i->modified_by = $this->uid;
		$i->publish_up = date("Y-m-d H:i:s");
		$i->publish_down = date("Y-m-d H:i:s",time()+60*60*24*365*3 );
		$i->priority = 0;
		$i->hits = 0;
		$i->created_by_alias = "";
		$i->access = 1;
		$i->created_by = $this->uid;
		$i->original_created_by = $this->uid;
		$i->original_media_id = 0;
		$i->params = "";
		$i->searchable = $_POST["is_public"];
		$i->elements = $elements;
		$i->state = 1;
		$i->save();

		//keyword
		if( isset($_POST["tags"]) ){
			$this->InsertKeyWords2($_POST["tags"],$i->id);
		}
		else{
    		$this->InsertKeyWords($_POST["keywords"],$i->id);
    	}

    	$this->result_flag = true;
    	$this->result_string = "Success";
	}

	private function InsertKeyWords2($tag_array,$item_id){
		foreach( $tag_array as $key_word ){
			DB::table("zoo_tag")->insert(
				array(	'item_id' 	=> $item_id,
					'name' 	=> $key_word
			));
		}

	}

	private function InsertKeyWords($key_string,$item_id){
		$key_array = json_decode($key_string);
		foreach( $key_array as $key_word ){
			DB::table("zoo_tag")->insert(
				array(	'item_id' 	=> $item_id,
					'name' 	=> $key_word
			));
		}

	}

	private function GetAlias($alias){
		$ret = \Skygdi\Model\Item::where("alias",$alias)->take(1)->count();
		if( $ret>=1 ){
			return $this->GetAlias($alias."-1");
		}
		else return $alias;
	}

	private function Elements($file){
		//var_dump($file);
		//making the json string
		$elements_array = array();
		$elements_array["73042ccf-209b-424e-841b-44116c191fd2"] 
			= array(
				"file"=>$file,
				"title"=>"",
				"link"=>"",
				"target"=>"0",
				"rel"=>"",
			);
		/*
		$elements_array["ff336fbb-b678-4dba-b349-302f4905e9e4"] = array();
		$v = json_decode('{"0":{"file":"","uniqid":""}}');
		foreach( $v as $key => $value ){
			$v->$key->file = $file;
			$v->$key->uniqid = "f3ca6caa1046f5";
			break;
		}

		$elements_array["ff336fbb-b678-4dba-b349-302f4905e9e4"] = $v;
		*/
		$elements_array["ff336fbb-b678-4dba-b349-302f4905e9e4"] 
			= array(
				"0"=>array(
					"file"=>$file,
					"uniqid"=>"f3ca6caa1046f5"
					)
			);
		return json_encode($elements_array,JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);
	}

	private function UploadImage(&$ret,$user_id){

		$ret = false;

		if( strlen(@$_FILES["file_image"]["name"])==0 ){
			//exist and no going to change
			return 'no file name';
		}
		else if( @$_FILES["file_image"]["size"] < 2000 || @$_FILES["file_image"]["size"] > 800000 ){
			return 'file size accept between 2K to 800K';
		}
		else if ( ($_FILES["file_image"]["type"] == "image/gif")
			|| ($_FILES["file_image"]["type"] == "image/jpeg")
			|| ($_FILES["file_image"]["type"] == "image/pjpeg")
			|| ($_FILES["file_image"]["type"] == "image/png")
			)
		{
			$image_dimension = getimagesize($_FILES["file_image"]["tmp_name"]);

			if ($_FILES["file_image"]["error"] > 0)
			{
				return 'file error , please try it again later.';
			}
			else
			{
				$base_dir = "images/media";
				$dir = $_ENV["root_directiory"]."/".$base_dir;
				if( !file_exists($dir) ) mkdir($dir);

				//try remove previous avatar file
				//@unlink( $dir .$previous_avatar); 

				//save or cover
				$ext = pathinfo($_FILES["file_image"]["name"], PATHINFO_EXTENSION);
				$file_name = $user_id."_".time().".".$ext;
				$destination_file = $dir ."/". $file_name;

				//die( $destination_file );
				if( file_exists($destination_file) ) unlink($destination_file);

				move_uploaded_file($_FILES["file_image"]["tmp_name"],$destination_file);
			}

			//$this->AvatarSave($file_name);
			//$this->ProfileSet("avatar",$file_name);
			$ret = true;
			return $base_dir."/".$file_name;
		}
		else
		{
			//echo "Invalid file";
			return 'avatar file format accept as follow : gif , jpg , png ';
			
		}
	}
}
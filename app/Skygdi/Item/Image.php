<?php
namespace App\Skygdi\Item;

class Image{
	
	function __construct(){
		$this->directory_name = "home_thumb";
		$this->cache_file_base = storage_path().'/app/images/cache/'.$this->directory_name;
		$this->cache_file_url_base = url('/').'/'.$this->directory_name;
		$this->cache_life = 60*30;
		$this->debug = false;
	}

	//var $item_id;
	var $filename;
	var $filetype;
	var $directory_name;
	var $newwidth;
	var $newheight;

	var $cache_url;
	var $cache_file;
	var $cache_life;

	var $flag_not_found;
	var $debug;

	function initialize($item_id,$filename,$newwidth,$newheight){

		//var_dump($this->cache_file_base);
		//$this->item_id = $item_id;
		$this->filename = storage_path()."/app/".$filename;
		$this->filetype = pathinfo($this->filename, PATHINFO_EXTENSION);
		$this->newwidth = $newwidth;
		$this->newheight = $newheight;
		$this->cache_file = $this->cache_file_base."/".$this->item_id.".".$this->filetype;
		$this->cache_url = $this->cache_file_url_base."/".$this->item_id.".".$this->filetype;

	}

	function get_url(){

		if( !file_exists($this->cache_file) ){
			$this->generate_cache();
		}
		else{
			if( (time() - filemtime($this->cache_file) > $this->cache_life) || $this->debug ) $this->generate_cache();
		}

		if( !file_exists($this->cache_file) ){
			$this->flag_not_found = true;
		}
		else $this->flag_not_found = false;

		return $this->cache_url;
	}

	function generate_cache(){
		//dump("generate");
		///*
		if( !file_exists($this->filename) ){
			$this->flag_not_found = true;
			return;
		}
		//*/
		
		//dd($this->cache_file);
		if( file_exists($this->cache_file) ) unlink($this->cache_file);

		//Create directories
		if (!file_exists(dirname($this->cache_file))) {
			mkdir(dirname($this->cache_file), 0777,true);
		}

		//dump("generate:".strtolower($this->filetype) );
		if( strtolower($this->filetype)=="jpg" ) $this->generate_cache_jpeg();
		else if( strtolower($this->filetype)=="jpeg" ) $this->generate_cache_jpeg();
		else if( strtolower($this->filetype)=="png" ) $this->generate_cache_png();

		$this->flag_not_found = false;
	}

	function generate_cache_jpeg(){
		//dd($this->cache_file);
		list($width, $height) = getimagesize($this->filename);
		$thumb = imagecreatetruecolor($this->newwidth, $this->newheight);
		$source = imagecreatefromjpeg($this->filename);
		// Resize
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $this->newwidth, $this->newheight, $width, $height);
		//crop
		//$thumb = imagecrop($source, ['x' => 0, 'y' => 0, 'width' => $newwidth, 'height' => $newheight]);
		@imagejpeg( $thumb, $this->cache_file );
	}

	function generate_cache_png(){
		
		list($width, $height) = getimagesize($this->filename);
		$thumb = imagecreatetruecolor($this->newwidth, $this->newheight);
		$source = imagecreatefrompng($this->filename);

		//var_dump($this->cache_file);exit();
		//echo strtolower($_ENV["DOCUMENT_ROOT"]."/".$this->filename);exit();
		// Resize
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $this->newwidth, $this->newheight, $width, $height);
		@imagepng( $thumb, $this->cache_file );
		
		
	}
}
?>
<?php
namespace App\Skygdi\Item;

use DB;
use App\Models\Item;
use App\Models\PostCard;

class PostCardHelper{
	

	public $result_flag;
	public $result_string;
	public $uid;

	
	public function MediaListing(){
		$Equery = Item::where("created_by",$this->uid);
		$Equery->take(100);
		$Equery->where("type","media");
		$Equery->orderby("modified","DESC");
		$result = $Equery->get();
		return $result;
	}

	
	public function EditPostcard(){
		$alias = str_replace(" ","-",strtolower($_POST["name"]));
		$alias = $this->GetAlias($alias,$_POST["postcard_id"]);

		$op = PostCard::where("created_by",$_SESSION["user"]->id)
			->where("id",$_POST["postcard_id"])
			->first();
		$elements = json_decode($op->elements,true);
		$elements["910c3b75-0e61-4aa2-b670-17e2b7e68ba0"]["0"]["value"] = $_POST["content"];
		$elements["536c2dbe-8287-4e2c-abde-34203a619fa4"]["item"]["0"] = $_POST["id"];
		$elements_str = json_encode($elements,JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);

		$op->elements = $elements_str;
		$op->name = $_POST["name"];
		$op->alias = $alias;
		$op->original_media_id = $_POST["id"];
		
		$op->save();

		die("success");
	}

	public function NewPostcard(){
		$alias = str_replace(" ","-",strtolower($_POST["name"]));
		$alias = $this->GetAlias($alias);

		$oi = Item::where("id",$_POST["id"])->first();

		//$elements = 
		$elements["c0f07ff6-1428-4458-83b0-9243e760f23d"] = array("0"=>array("value"=>1.99,"tex_class"=>1));
		$elements["910c3b75-0e61-4aa2-b670-17e2b7e68ba0"] = array("0"=>array("value"=>$_POST["content"]));
		$elements["536c2dbe-8287-4e2c-abde-34203a619fa4"] = array("item"=>array("0"=>$_POST["id"]));
		$elements_str = json_encode($elements,JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);

		$i = new PostCard;
		$i->application_id = 2;
		//$i->type = "postcard";
		$i->name = $_POST["name"];
		$i->alias = $alias;
		$i->modified_by = $_SESSION["user"]->id;
		$i->publish_up = date("Y-m-d H:i:s");

		$i->publish_down = date("Y-m-d H:i:s",time()+60*60*24*365*3 );
		$i->priority = 0;
		$i->hits = 0;
		$i->created_by_alias = "";
		$i->params = "";

		$i->access = 1;
		$i->created_by = $_SESSION["user"]->id;
		$i->searchable = 1;
		$i->elements = $elements_str;
		$i->original_created_by = $oi->created_by;
		$i->original_media_id = $_POST["id"];
		
		$i->state = 1;
		$i->save();

		die("success");
	}

	private function GetAlias($alias,$self_id=null){
		$Equery = Item::where("alias",$alias);
		if( $self_id!=null ) $Equery->where("id","!=",$self_id);

		$ret = $Equery->take(1)->count();
		

		if( $ret>=1 ){
			return $this->GetAlias($alias."-1");
		}
		else return $alias;
	}

	public function __construct() {
        
    }
}
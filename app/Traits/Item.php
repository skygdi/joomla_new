<?php
namespace App\Traits;

//use App\Models\ItemTag;
use App\Models\MediaProperty;
use App\Skygdi\Item\Image;
use Illuminate\Database\Eloquent\SoftDeletes;


trait Item
{
    use SoftDeletes;

    /*
    public function Property(){
        return $this->hasMany("App\Models\MediaProperty","item_id");
    }
    */

    public function Comment(){
        return $this->hasMany("App\Models\MediaComment");
    }

    public function KeyWord(){
        return $this->hasMany('App\Models\MediaKeyword','media_id');
    }

    

    /*
    public function ImageCache(){
        $image_obj = new Image;
        $image_obj->initialize( $this->id,$this->media_file,400,400 );
        if( $image_obj->flag_not_found ) $this->image_cache = "";
        else $this->image_cache = $image_obj->get_url();
    }
    */
}
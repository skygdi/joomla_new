<?php
namespace App\Traits;

//use App\Models\MediaProperty;

trait MediaImageCache
{
    var $image_full_file_path;
    var $image_extension;
    var $image_cache_width = 400;
    var $image_cache_height = 400;

    public static $image_cache_url;
    var $image_full_cache_file_path;
    public static $image_cache_lifetime = 60*30;

    var $image_cache_not_found;
    var $image_debug = false;

    public function ImageCacheGet(){

        //$directory_name = "media_thumb";
        $this->cache_file_base = storage_path().'/app/images/media_thumb';
        $this->cache_file_url_base = url('/').'/media/thumb';
        //$this->image_debug = false;

        $this->image_full_file_path = storage_path()."/app/images/media/".$this->file;
        $this->image_extension = pathinfo($this->image_full_file_path, PATHINFO_EXTENSION);
        $this->image_full_cache_file_path = $this->cache_file_base."/".$this->id.".".$this->image_extension;
        $this->image_cache_url = $this->cache_file_url_base."/".$this->id.".".$this->image_extension;

        //$this->image_cache_url = "";
        $this->ImageCacheGetUrl();
    }
    
    private function ImageCacheGetUrl(){

        if( !file_exists($this->image_full_cache_file_path) ){
            $this->ImageCacheGenerate();
        }
        else{
            if( (time() - filemtime($this->image_full_cache_file_path) > $this->image_cache_lifetime) || $this->image_debug ) $this->ImageCacheGenerate();
        }

        if( !file_exists($this->image_full_cache_file_path) ){
            $this->image_cache_not_found = true;
        }
        else $this->image_cache_not_found = false;

        //return $this->image_cache_url;
    }

    private function ImageCacheGenerate(){
        //dump("generate");
        if( !file_exists($this->image_full_file_path) ){
            $this->image_cache_not_found = true;
            return;
        }
        
        if( file_exists($this->image_full_cache_file_path) ) unlink($this->image_full_cache_file_path);

        //Create directories
        if (!file_exists(dirname($this->image_full_cache_file_path))) {
            mkdir(dirname($this->image_full_cache_file_path), 0777,true);
        }

        //dump("generate:".strtolower($this->image_extension) );
        if( strtolower($this->image_extension)=="jpg" ) $this->ImageCacheGenerateJPG();
        else if( strtolower($this->image_extension)=="jpeg" ) $this->ImageCacheGenerateJPG();
        else if( strtolower($this->image_extension)=="png" ) $this->ImageCacheGeneratePNG();

        $this->image_cache_not_found = false;
    }

    private function ImageCacheGenerateJPG(){
        //dd($this->image_full_cache_file_path);
        list($width, $height) = getimagesize($this->image_full_file_path);
        $thumb = imagecreatetruecolor($this->image_cache_width, $this->image_cache_height);
        $source = imagecreatefromjpeg($this->image_full_file_path);
        // Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $this->image_cache_width, $this->image_cache_height, $width, $height);
        @imagejpeg( $thumb, $this->image_full_cache_file_path );
    }

    private function ImageCacheGeneratePNG(){
        list($width, $height) = getimagesize($this->image_full_file_path);
        $thumb = imagecreatetruecolor($this->image_cache_width, $this->image_cache_height);
        $source = imagecreatefrompng($this->image_full_file_path);
        // Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $this->image_cache_width, $this->image_cache_height, $width, $height);
        @imagepng( $thumb, $this->image_full_cache_file_path );
    }
}
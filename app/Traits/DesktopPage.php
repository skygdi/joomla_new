<?php
namespace App\Traits;

use Illuminate\Support\Facades\App;
use Session;
//use Auth;
use App\Http\Controllers\Desktop\ShoppingCartController;



trait DesktopPage
{
	private $vars;

    /**
     * Initialize rendering template along with language setting
     */
	public function PageRenderInitialize(){
        $this->vars["shoppingcart_json"] = json_encode(ShoppingCartController::ItemList());
        $this->vars["shoppingcart_item_count"] = ShoppingCartController::SummaryCount();
        $this->vars["shoppingcart_item_total"] = ShoppingCartController::SummaryTotal();
        $this->vars["locale"] = App::getLocale();
    }

}
<?php 
namespace App\Models;

//use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
	protected $table = 'users_profile';

    /*
    public $timestamps = false;
	public static function AddColumn(){
		if(Schema::hasColumn('user_profiles','id'))
        {
            Schema::table('user_profiles', function($table)
            {
                $table->increments('id')->first();
            });
        }
    }
    */
    function User(){
        return $this->belongsTo('App\User');
    }

    /*
    function DefaultReturnAddress(){
        return $this->Address()
                ->Billing()
                ->Default();
    }
    

    function Address(){
        return $this->hasMany('App\Models\Address','user_id','user_id');
    }
    */
    
	
	protected $fillable = [
        'user_id', 'avatar', 'paypal', 'description',
    ];
}
?>
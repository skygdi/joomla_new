<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaKeyword extends Model
{
	protected $table = 'media_keyword';

	public $timestamps = false;
	
	
	protected $fillable = [
        'name', 
        'media_id',
        'id',
    ];
    
}
?>
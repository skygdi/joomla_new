<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expenditure extends Model
{
    use SoftDeletes;
    protected $table = 'payment';

    //const CREATED_AT = 'pay_date';

    public function User(){
        return $this->hasOne("App\User","id","user_id");
    }

    protected $fillable = [
        'user_id',
        'money',
        'note',
        'created_at',
    ];

}

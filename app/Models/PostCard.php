<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Traits\Item;


class PostCard extends Model
{
    use \App\Traits\Item;

    protected $table = 'postcard';
    //protected $table = 'zoo_item';
    //const CREATED_AT = 'created';
    //const UPDATED_AT = 'modified';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope( function (Builder $builder) {
            //$builder->whereNotNull('media_file');
            //$builder->where('type', 'postcard');
            
        });

        static::creating(function($model) {
            //$model->publish_up = null;
            //$model->publish_down = null;
            return true;
        });

        
    }

    public function scopeSelf($query,$id)
    {
        return $query->where('user_id', $id);
    }

    function Media(){
        return $this->hasOne('App\Models\Media','id','media_id');
    }

    function UserDefaultReturnAddress(){
        //return $this->hasOne('App\User','id','user_id');
        return $this->hasOne('App\Models\Address','user_id','user_id')
                ->Billing()
                ->Default()
                ->first();
    }

    function User(){
        return $this->hasOne('App\User','id','user_id');
    }

    protected $fillable = [
        'name',
        'content',
        'user_id',
        'media_id',
        'tmp',
    ];

    public static $base_price = 1.49;

    function Price(){
        //dump($this->id);
        if( !$this->Media()->first() ) return self::$base_price;
        //$kk = self::$base_price;
        if( $this->Media()->first()->user_id==$this->user_id ) return self::$base_price - 0.5;
        return self::$base_price;
    }
    /*
    function Price($user){
        if( $this->Media()->first()->created_by==$user ) return self::$base_price - 0.5;
        return self::$base_price;
    }
    */

    	
}
?>
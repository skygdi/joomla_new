<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemProperty extends Model
{
	protected $table = 'zoo_item_property';

	//public $timestamps = false;
	//const CREATED_AT = null;
	const UPDATED_AT = 'datetime';

	public function setCreatedAt($value)
    {
        return NULL;
    }

	
	protected $fillable = [
        'item_id', 'created_by', 'name','value'
    ];
    
}
?>
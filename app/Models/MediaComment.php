<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaComment extends Model
{
    use SoftDeletes;

    public function User(){
        return $this->belongsTo('App\User');
    }

    public function scopeSelf($query,$id)
    {
        return $query->where('user_id', $id);
    }

    protected $table = 'media_comment';

    protected $fillable = [
        'user_id',
        'media_id',
        'content',
        'tmp',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class MediaPopular extends MediaProperty
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(  function (Builder $builder) {
            $builder->where('name', 'click');
            //$builder->OrderBy('value', 'DESC');
        });

    }

    public function media()
    {
        return $this->belongsTo('App\Models\Media','item_id');
    }
}

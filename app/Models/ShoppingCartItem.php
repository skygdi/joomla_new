<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ShoppingCartItem extends Model
{
    protected $table = 'shopping_cart_item';

    public function scopeSelf($query,$id)
    {
        return $query->where('user_id', $id);
    }

    function PostCard(){
        return $this->hasOne('App\Models\PostCard','id','postcard_id');
    }

    function Total(){
        return round($this->PostCard()->first()->Price() * $this->quantity,2);
    }

    protected $fillable = [
        'user_id',
        'postcard_id',
        'quantity',
    ];
}

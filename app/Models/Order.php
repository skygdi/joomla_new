<?php 
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use OrderItem;

class Order extends Model
{
    use SoftDeletes;

    public function Total(){
        return round($this->ShippingFee() + $this->Subtotal(),2);
    }

    public function ShippingFee(){
        if( $this->SA_country!="US" ){
            return $this->OrderItem()->sum("quantity") * Config::get('postcard')['oversea_fee'];
        }
        else return 0;
    }

    public function Subtotal(){
        $subtotal = $this->OrderItem()->get()->sum(function($item){
            return $item->quantity * $item->price;
        });
        return round($subtotal,2);
    }

    public function OrderItem(){
        return $this->hasMany("App\Models\OrderItem");
    }

    public function scopeReceived($query,$value)
    {
        if( $value ) return $query->where('received', 1);
        else return $query->where('received', 0);
    }

    public function scopePaymentValidating($query,$value)
    {
        if( $value ) return $query->where('payment_validating', 1);
        else return $query->where('payment_validating', 0);
    }

    public function scopeDelivered($query,$value)
    {
        if( $value ) return $query->where('delivered', 1);
        else return $query->where('delivered', 0);
    }

    public function scopePaid($query,$value)
    {
        if( $value ) return $query->where('paid', 1);
        else return $query->where('paid', 0);
    }

    public function scopeSelf($query,$id)
    {
        return $query->where('user_id', $id);
    }

    public function User(){
        return $this->hasOne("App\User","id","user_id");
    }

    public function ID_show(){
        return "0011".$this->id;
    }

	protected $table = 'order';


    protected $fillable = [

        'user_id',
        'paid',
        'pay_type',
        'pay_type',
        'transaction_id',
        'delivered',
        'received',
        'cancel',

        'BA_name',
        'BA_phone',
        'BA_email',
        'BA_address1',
        'BA_address2',
        'BA_city',
        'BA_state',
        'BA_zipcode',
        'BA_country',
        'SA_name',
        'SA_phone',
        'SA_email',
        'SA_address1',
        'SA_address2',
        'SA_city',
        'SA_state',
        'SA_zipcode',
        'SA_country',

        'tmp',
        'created_at',
        'id',
        
    ];
}
?>
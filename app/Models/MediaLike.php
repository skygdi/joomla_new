<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaLike extends Model
{
    protected $table = 'media_like';

    public $timestamps = false;

    protected $fillable = [
        'media_id',
        'user_id',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Traits\Item;
use App\Traits\MediaImageCache;
use Auth;


use App\Models\OrderItem;
use App\Models\MediaProperty;

class MediaOld extends Model
{
    use Item;
    use MediaImageCache;
    

    protected $table = 'zoo_item';


	const CREATED_AT = 'created';
	const UPDATED_AT = 'modified';
    



    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope( function (Builder $builder) {
            $builder->whereNotNull('media_file');
            $builder->where('type', 'media');
            
        });

        static::creating(function($model) {
            $model->publish_up = date("Y-m-d H:i:s",time());
            $model->publish_down = date("Y-m-d H:i:s",time()+60*60*24*365*3 );
            return true;
        });

        
    }

    public function scopeSearchable($query)
    {
        return $query->where('searchable', '1');
    }

    function media_file_base(){
        return pathinfo($this->media_file, PATHINFO_BASENAME);
    }


    public function Liked(){
        $c = $this->Property()
            ->where("name","like")
            ->where("created_by", Auth::id() )
            ->count();
        if( $c>=1 ) return true;
        else return false;
    }

    public function Likes(){
        return $this->Property()
            ->where("name","like")
            ->count();
    }

    function Click(){
        $p = $this->Property()->firstOrNew(
            [   'item_id'       => $this->id , 
                'created_by'    => 0, 
                'name'          => 'click',
            ],
            [   'value' => 0    ]
        );
        $p->value++;
        $this->Property()->save($p);
        //$p->save();
    }

    public function Clicks(){
        $p = $this->Property()->where("name","click")->first();
        if( !$p ) return 0;
        else return $p->value;
    }

    public function Sold()
    {
        return OrderItem::Where("item_id",$this->id)
                ->Purchased()
                ->sum('quantity');
    }

    public function Balance()
    {
        return OrderItem::Where("item_id",$this->id)
                ->Purchased()
                ->get()
                ->sum(function ($oi) {
                    return $oi->quantity*$oi->price - \App\Models\OrderItem::$base_price;
                });
    }

    public function DisableComment(){
        $p = $this->Property()->where("name","disable_comment")->first();

        if( !$p ){
            return;

            //Not exist
            $p = new MediaProperty;
            $p->name = "disable_comment";
            $p->value = 0;
            $p->item_id = $this->id;
            $p->created_by = $this->created_by;
            $this->Property()->save($p);
        }
        else{
            $p->value = 0;
            $this->Property()->value = 0;
            $this->Property()->save($p);
        }
    }

    public function EnableComment(){
        $p = $this->Property()->where("name","disable_comment")->first();

        if( !$p ){
            //Not exist
            $p = new MediaProperty;
            $p->name = "disable_comment";
            $p->value = 1;
            $p->item_id = $this->id;
            $p->created_by = $this->created_by;
            $this->Property()->save($p);
        }
        else{
            $p->value = 1;
            //$p->save();
            //$this->Property()->value = 1;
            $this->Property()->save($p);
        }
    }

    public function IsDisableComment(){
        $p = $this->Property()->where("name","disable_comment")->first();
        if( $p ){
            if( $p->value ) return true;
            else return false;
        }
        else return false;
    }


    public function User(){
        return $this->hasOne("App\User","id","created_by");
    }

    function FixOldPicture(){
        $ea = json_decode($this->elements,true);
        //if( $this->id=="112" ) dump($ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"]);
        if( isset($ea["73042ccf-209b-424e-841b-44116c191fd2"]["file"]) 
            && strlen($ea["73042ccf-209b-424e-841b-44116c191fd2"]["file"])>0 ){
            
            if( $this->media_file!="" ){
                $this->media_file = $ea["73042ccf-209b-424e-841b-44116c191fd2"]["file"];
                $this->save();
                return;
            }
        }
        //if( $this->id=="112" ) dump("kk");

        if( isset($ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"]) 
            && strlen($ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"])>0 ){
            $this->media_file = $ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"];
            //if( $this->id=="3" ) dump("jkl");
            $this->save();
        }

        //if( $this->id=="230" ) dump($this->media_file);
    }


    protected $fillable = [
        'name',
        'created_by',
        'original_created_by',
        'searchable',
        'media_file',
    ];

	protected $attributes = [
        'application_id' 	=> 2,
        'type' 				=> "media",
        'priority'			=> 0,
        'hits'				=> 0,
        'created_by_alias'	=> "",
        'original_media_id'	=> 0,
        'params'			=> "",
        'state'				=> 1,
        'media_file'        => "",
    ];
}

<?php 
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;

    protected $table = 'order_item';

    public static $base_price = 0.99;

    public function scopePurchased($query)
    {
        return $query->WhereHas('Order',function($subQuery){
                    $subQuery->where('paid', 1);
                });
    }
    
    function Order(){
        return $this->belongsTo('App\Models\Order');
    }

    /*
	protected $table = 'zoo_zl_zoocart_orderitems';

    public static $refund = 0.5;
    public static $base_price = 0.99;

    public function scopePurchased($query)
    {
        return $query->WhereHas('Order',function($subQuery){
                    //$subQuery->where('state', '2');
                    $subQuery->whereBetween('state', [2, 5]);
                });
    }
    
    function Order(){
        return $this->belongsTo('App\Models\Order');
    }
    */
    
    
    
	protected $fillable = [
        'order_id', 
        'media_id',
        'postcard_id', 
        'price',
        'name',
        'quantity',
        'content',
        'file',

        'id',
    ];


}
?>
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Traits\Item;
use App\Traits\MediaImageCache;
use Auth;


use App\Models\OrderItem;
use App\Models\MediaProperty;

class Media extends Model
{
    use Item;
    use MediaImageCache;

    protected $table = 'media';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope( function (Builder $builder) {
        });

        /*
        static::creating(function($model) {
            return true;
        });
        */
    }

    public function scopeSelf($query,$id)
    {
        return $query->where('user_id', $id);
    }

    public function scopeSearchable($query)
    {
        return $query->where('searchable', '1');
    }


    public function Liked(){
        if( $this->Like()->where("user_id",Auth::id())->count()>=1 ) return true;
        else return false;
    }

    public function Likes(){
        return $this->Like()->count();
    }

    public function Click(){
        $this->shows++;
        $this->save();
    }

    public function Sold()
    {
        return OrderItem::Where("media_id",$this->id)
                ->Purchased()
                ->sum('quantity');
    }

    public function Balance()
    {
        return OrderItem::Where("media_id",$this->id)
                ->Purchased()
                ->get()
                ->sum(function ($oi) {
                    return $oi->quantity*$oi->price - \App\Models\OrderItem::$base_price;
                });
    }

    public function SaveWord($keys){
        //dump($keys);

        $this->KeyWord()->delete();

        if( count($keys)==0 ) return;

        foreach( $keys as $name ){
            $k = new \App\Models\MediaKeyword;
            $k->name = $name;
            $k->media_id = $this->id;

            if( $this->KeyWord()->where("name",$name)->count()>=1 ) continue;
            $this->KeyWord()->save($k);
        }
    }

    public function User(){
        return $this->hasOne("App\User","id","user_id");
    }

    public function Like(){
        return $this->hasOne("App\Models\MediaLike");
    }

    /*
    function FixOldPicture(){
        $ea = json_decode($this->elements,true);
        //if( $this->id=="230" ) dump($ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"]);
        if( isset($ea["73042ccf-209b-424e-841b-44116c191fd2"]["file"]) 
            && strlen($ea["73042ccf-209b-424e-841b-44116c191fd2"]["file"])>0 ){
            
            if( $this->media_file!="" ){
                $this->media_file = $ea["73042ccf-209b-424e-841b-44116c191fd2"]["file"];
                $this->save();
                return;
            }
        }
        //if( $this->id=="230" ) dump("kk");

        if( isset($ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"]) 
            && strlen($ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"])>0 ){
            $this->media_file = $ea["ff336fbb-b678-4dba-b349-302f4905e9e4"][0]["file"];
            //if( $this->id=="3" ) dump("jkl");
            $this->save();
        }

        //if( $this->id=="230" ) dump($this->media_file);
    }
    */


    protected $fillable = [
        'name',
        'user_id',
        'searchable',
        'file',
        'created_at',
        'disable_comment',
        'shows',
    ];


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaProperty extends Model{
    //
    protected $table = 'zoo_item_property';

    const UPDATED_AT = 'datetime';

	public function setCreatedAt($value)
    {
        return NULL;
    }

	
	protected $fillable = [
        'item_id', 'created_by', 'name','value'
    ];
}

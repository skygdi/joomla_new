<?php 
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $table = 'address';

    public function scopeSelf($query,$id)
    {
        return $query->where('user_id', $id);
    }
    
    public function scopeDefault($query)
    {
        return $query->where('is_default', '1');
    }

    public function scopeShipping($query)
    {
        return $query->where('type', 'shipping');
    }

    public function scopeBilling($query)
    {
        return $query->where('type', 'billing');
    }


	protected $fillable = [
        'is_default',
        'display_as_return_address',
        'type',
        'user_id',
        'name',
        'phone',
        'email',
        'address1',
        'address2',
        'city',
        'state',
        'zipcode',
        'country',
    ];
}
?>
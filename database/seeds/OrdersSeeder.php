<?php

use Illuminate\Database\Seeder;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Address;
use App\Models\PostCard;
use App\User;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        //$u = User::all()->random(1)->toArray();
        $user_id = 30;
        $u = User::where("id",$user_id)->first();
        $data = array();
        $data["user_id"] = $u->id;
        $data["paid"] = 1;
        //$data["transaction_id"] = "PAY-2YG96093XV192804ELI35FCY";

        for($i=0;$i<50;$i++){
	        $data["transaction_id"] = strtoupper($faker->lexify($string =  'PAY-????????????????????????'));

	        $data["pay_type"] = "paypal";

	        $billing_address = Address::where("user_id",$u->id)->Billing()->Default()->first()->toArray();
	        $shipping_address = Address::where("user_id",$u->id)->Shipping()->Default()->first()->toArray();

	        $data["BA_name"]        = $billing_address["name"];
	        $data["BA_phone"]       = $billing_address["phone"];
	        $data["BA_email"]       = $billing_address["email"];
	        $data["BA_address1"]    = $billing_address["address1"];
	        $data["BA_address2"]    = $billing_address["address2"];
	        $data["BA_city"]        = $billing_address["city"];
	        $data["BA_state"]       = $billing_address["state"];
	        $data["BA_zipcode"]     = $billing_address["zipcode"];
	        $data["BA_country"]      = $billing_address["country"];

	        $data["SA_name"]        = $shipping_address["name"];
	        $data["SA_phone"]       = $shipping_address["phone"];
	        $data["SA_email"]       = $shipping_address["email"];
	        $data["SA_address1"]    = $shipping_address["address1"];
	        $data["SA_address2"]    = $shipping_address["address2"];
	        $data["SA_city"]        = $shipping_address["city"];
	        $data["SA_state"]       = $shipping_address["state"];
	        $data["SA_zipcode"]     = $shipping_address["zipcode"];
	        $data["SA_country"]      = $shipping_address["country"];

	        $order_id = Order::create($data);

	        //$post_cards = PostCard::Self($u->id)->WhereBetween('id', [90, 100])->random(1);
	        $take = $faker->numberBetween(1,3);
	        $post_cards = PostCard::Self($u->id)->WhereBetween('id', [90, 100])->inRandomOrder()->take($take)->get();
	        

	        foreach( $post_cards as $post_card ){
	        	$o_data = array();
		        $o_data["order_id"] = $order_id->id;
		        $o_data["postcard_id"] = $post_card->id;
		        $o_data["media_id"] = $post_card->Media()->first()->id;
		        $o_data["name"] = $post_card->name;
		        $o_data["content"] = $post_card->content;
		        $o_data["file"] = $post_card->Media()->first()->file;
		        $o_data["price"] = $post_card->Price();
		        $o_data["quantity"] = $faker->numberBetween(1,4);
	        	OrderItem::create($o_data);
	        }

        }

        
        
    }
}

<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Models\Payment;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

        for($i=0;$i<20;$i++){
        	$data = array();
        	$data["note"] = $faker->realText(120, 2);
        	$data["money"] = $faker->randomFloat(2, 5, 60);
        	$data["user_id"] = User::all()->random(1)->first()->id;
            $data["created_at"] = $faker->dateTimeThisYear('now', null);
        	Payment::create($data);

        }
    }
}

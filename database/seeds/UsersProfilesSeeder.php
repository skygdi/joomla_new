<?php

use Illuminate\Database\Seeder;

class UsersProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data_array = array();
        $result = DB::table('user_profiles')->get();
        foreach( $result as $row ){
        	//echo $row->id;
        	$user_id = $row->user_id;
        	$key = $row->profile_key;
        	$data_array[$user_id][$key] = $row->profile_value;
        }

        //var_dump($data_array);
        foreach( $data_array as $uid => $row ){
        	$data = $row;
        	if( !isset($data["paypal"]) || $data["paypal"]==null ) continue;
        	$data["user_id"] = $uid;
        	unset($data["new_temporary_password"]);
        	$data["created_at"] = date("Y-m-d H:i:s");
        	$data["updated_at"] = date("Y-m-d H:i:s");
        	DB::table('users_profile')->insert($data);
        }
    }
}

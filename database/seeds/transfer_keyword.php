<?php

use Illuminate\Database\Seeder;

use App\Models\MediaKeyword;

class transfer_keyword extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result = DB::table('zoo_tag')->get();
        foreach( $result as $row ){
        	//echo $row->id;
        	//$user_id = $row->user_id;
        	//$key = $row->profile_key;
        	//$data_array[$user_id][$key] = $row->profile_value;
        	$k = MediaKeyword::firstOrNew(["media_id"=>$row->item_id]);
        	$k->name = $row->name;
        	$k->save();
        }
    }
}

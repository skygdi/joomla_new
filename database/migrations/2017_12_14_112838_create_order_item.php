<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->mediumInteger('media_id')->default(0);
            $table->mediumInteger('order_id')->default(0);
            $table->mediumInteger('postcard_id')->default(0);
            $table->mediumInteger('quantity')->default(0);
            $table->float('price', 8, 2);
            $table->char('name',64)->nullable();
            $table->char('content',255)->nullable();
            $table->char('file',255)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}

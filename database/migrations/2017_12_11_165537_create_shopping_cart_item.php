<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingCartItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_cart_item', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            //$table->softDeletes();
            $table->mediumInteger('user_id')->default(0);
            $table->mediumInteger('postcard_id')->default(0);
            $table->mediumInteger('quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_cart_item');
    }
}

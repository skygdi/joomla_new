<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('paid')->default(false);
            $table->char('pay_type',32)->nullable();
            $table->char('transaction_id',255)->nullable();
            $table->boolean('payment_validating')->default(false);
            $table->boolean('delivered')->default(false);
            $table->boolean('received')->default(false);
            $table->mediumInteger('user_id')->default(0);
            $table->char('BA_name',64)->nullable();
            $table->char('BA_phone',32)->nullable();
            $table->char('BA_email',128)->nullable();
            $table->char('BA_address1',128)->nullable();
            $table->char('BA_address2',128)->nullable();
            $table->char('BA_city',64)->nullable();
            $table->char('BA_state',32)->nullable();
            $table->char('BA_zipcode',16)->nullable();
            $table->char('BA_country',64)->nullable();
            $table->char('SA_name',64)->nullable();
            $table->char('SA_phone',32)->nullable();
            $table->char('SA_email',128)->nullable();
            $table->char('SA_address1',128)->nullable();
            $table->char('SA_address2',128)->nullable();
            $table->char('SA_city',64)->nullable();
            $table->char('SA_state',32)->nullable();
            $table->char('SA_zipcode',16)->nullable();
            $table->char('SA_country',64)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
